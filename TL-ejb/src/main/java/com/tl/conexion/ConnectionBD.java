package com.tl.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionBD {

    public static final String BD = "tl";
    public static final String LOGIN = "root";
    public static final String PASSWORD = "system";
    public static final String URL = "jdbc:mysql://localhost/" + BD;

    Connection connection = null;

    public ConnectionBD() {
        try {
            //obtenemos el driver de para mysql
            Class.forName("com.mysql.jdbc.Driver");
            //obtenemos la conexion
            connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
            if (connection != null) {
                System.out.println("Conexion a base de datos " + BD + " Ok\n");
            }

        } catch (ClassNotFoundException e) {
            System.out.println("com.ga.conexion.ConnectionBD.<init>()connection error: " + e);
        } catch (SQLException e) {
            System.out.println("com.ga.conexion.ConnectionBD.<init>()sql error: " + e);
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
