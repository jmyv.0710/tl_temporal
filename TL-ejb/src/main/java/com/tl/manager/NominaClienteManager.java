/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.NominaClienteDao;
import com.tl.entidades.Nominacliente;
import java.util.ArrayList;

/**
 *
 * @author Manuel
 */
public class NominaClienteManager {
 
    private NominaClienteDao nominaControlador;
    
    public NominaClienteManager(){
        System.out.println("com.tl.manager.NominaClienteManager.<init>()Inicio");
        nominaControlador = new NominaClienteDao();
    }
    
    public boolean crearNominaCliente(Nominacliente nominaCliente){
        System.out.println("com.tl.manager.NominaClienteManager.crearNominaCliente()Inicio");
        return nominaControlador.crearNominaCliente(nominaCliente);
    }
    
    public ArrayList<Nominacliente> listarNominaClientes(int idCliente) {
        System.out.println("com.tl.manager.NominaClienteManager.listarNominaClientes()Inicio");
        return nominaControlador.listarNominaClientes(idCliente);
    }
    
    public Nominacliente consultarNomClienteDocto(String identifacionsolicitante) {
        System.out.println("com.tl.manager.NominaClienteManager.consultarNomClienteDocto()Inicio");
        return nominaControlador.consultarNomClienteDocto(identifacionsolicitante);
    }
}
