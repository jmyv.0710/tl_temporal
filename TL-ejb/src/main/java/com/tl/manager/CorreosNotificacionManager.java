/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.CorreosNotificacionDao;
import com.tl.entidades.Correosnotificacion;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class CorreosNotificacionManager {
    
    private CorreosNotificacionDao correosNotificacionDao;

    public CorreosNotificacionManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        correosNotificacionDao= new CorreosNotificacionDao();
    }
    
    public boolean crearCorreosNotificacion(Correosnotificacion correosN){
        return correosNotificacionDao.crearCorreosNotificacion(correosN);
    }
    
    public Correosnotificacion consultarCorreosNotificacion(int idCliente){
        return correosNotificacionDao.consultarCorreosNotificacion(idCliente);
    }
    
    public List <Correosnotificacion> listarCorreosNotificacion(){
        return correosNotificacionDao.listarCorreosNotificacion();
    }
    
}
