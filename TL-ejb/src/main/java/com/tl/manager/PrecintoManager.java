/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.PrecintoDao;
import com.tl.entidades.Precinto;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class PrecintoManager {
    
    private PrecintoDao precintoDao;
    
    public PrecintoManager(){
        //Esto es un constructor
        precintoDao = new PrecintoDao();
    }
    
    public String crearPrecinto(Precinto precinto){
        System.out.println("manager.PrecintoManager.crearPrecinto()Start");
        precintoDao.crearPrecinto(precinto);
        return "Exito";
    }
    
    public List<Precinto> listarPrecintos(){
        System.out.println("manager.PrecintoManager.listarPrecintos()Start:");
        List<Precinto> listadoPrecintos = precintoDao.listarPrecintos();
        return listadoPrecintos;
    }
    
}
