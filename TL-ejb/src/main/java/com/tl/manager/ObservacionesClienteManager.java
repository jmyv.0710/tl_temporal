/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.ObservacionesClienteDao;
import com.tl.entidades.Observacionescliente;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class ObservacionesClienteManager {
    
    private ObservacionesClienteDao observaClienteDao;

    public ObservacionesClienteManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        observaClienteDao= new ObservacionesClienteDao();
    }
    
    public boolean crearObservaCliente(Observacionescliente observacionesC){
        return observaClienteDao.crearObservaCliente(observacionesC);
    }
    
    public Observacionescliente consultarObservaCliente(int idSolicitud){
        return observaClienteDao.consultarObservaCliente(idSolicitud);
    }
    
    public List <Observacionescliente> listarObservaClientes(){
        return observaClienteDao.listarObservaClientes();
    }
    
}
