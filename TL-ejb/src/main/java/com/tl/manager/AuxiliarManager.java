/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.AuxiliarDao;
import com.tl.entidades.Auxiliar;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class AuxiliarManager {
    
    private AuxiliarDao auxiliarDao;
    
    public AuxiliarManager(){
        //Esto es un constructor
        auxiliarDao = new AuxiliarDao();
    }
    
    public String crearAuxiliar(Auxiliar auxiliar){
        System.out.println("manager.AuxiliarManager.crearAuxiliar()Start");
        auxiliarDao.crearAuxiliar(auxiliar);
        return "Exito";
    }
    
    public List<Auxiliar> listarAuxiliares(){
        System.out.println("manager.AuxiliarManager.listarAuxiliares()Start:");
        List<Auxiliar> listadoAuxiliar = auxiliarDao.listarAuxiliares();
        return listadoAuxiliar;
    }
    
}
