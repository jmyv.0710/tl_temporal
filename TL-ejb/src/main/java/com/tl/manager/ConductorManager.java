/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.ConductorDao;
import com.tl.entidades.Conductor;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class ConductorManager {
    
    private ConductorDao conductorDao;

    public ConductorManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        conductorDao= new ConductorDao();
    }
    
    public boolean crearConductor(Conductor conductor){
        return conductorDao.crearConductor(conductor);
    }
    
    public Conductor consultarConductorDocto(String nrodocumentod){
        return conductorDao.consultarConductorDocto(nrodocumentod);
    }
    
    public List <Conductor> listarConductores(){
        return conductorDao.listarConductores();
    }
    
}
