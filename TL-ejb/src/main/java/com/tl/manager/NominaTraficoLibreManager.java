/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.NominaTraficoLibreDAO;
import com.tl.entidades.Nominatraficolibre;
import java.util.List;

/**
 *
 * @author Manuel
 */
public class NominaTraficoLibreManager {
 
    private NominaTraficoLibreDAO nominaTraficoLibreDAOImpl;
    
    public NominaTraficoLibreManager(){
        System.out.println("com.tl.manager.NominaTraficoLibreManager.<init>()");
        nominaTraficoLibreDAOImpl = new NominaTraficoLibreDAO();
    }
    
    public boolean adicionarNomina(Nominatraficolibre ntl) {
        System.out.println("com.tl.manager.NominaTraficoLibreManager.adicionarNomina()Inicio");
        return nominaTraficoLibreDAOImpl.adicionarNomina(ntl);
    }
    
    public List<Nominatraficolibre> listaNominaTrafLibre() {
        System.out.println("com.tl.manager.NominaTraficoLibreManager.listaNominaTrafLibre()Inicio");
        return nominaTraficoLibreDAOImpl.listaNominaTrafLibre();
    }
    
    public Nominatraficolibre consultarNominaPorDoc(int numeroDocumento) {
        System.out.println("com.tl.manager.NominaTraficoLibreManager.consultarNominaPorDoc()Inicio");
        return nominaTraficoLibreDAOImpl.consultarNominaPorDoc(numeroDocumento);
    }
    
}
