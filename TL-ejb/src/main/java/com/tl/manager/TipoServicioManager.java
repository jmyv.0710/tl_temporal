/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.TipoServicioDao;
import com.tl.entidades.Tiposervicio;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class TipoServicioManager {
    
    private TipoServicioDao tipoServicioDao;
    
    public TipoServicioManager(){
        //Esto es un constructor
        tipoServicioDao = new TipoServicioDao();
    }
    
    public String crearTipoServicio(Tiposervicio tiposervicio){
        System.out.println("manager.TipoServicioManager.crearTipoServicio()Start");
        tipoServicioDao.crearTipoServicio(tiposervicio);
        return "Exito";
    }
    
    public List<Tiposervicio> listarTipoServicio(){
        System.out.println("manager.TipoServicioManager.listarTipoServicio()Start:");
        List<Tiposervicio> listadoTipoServicio = tipoServicioDao.listarTipoServicio();
        return listadoTipoServicio;
    }

    
}
