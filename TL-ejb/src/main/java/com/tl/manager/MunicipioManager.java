/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.MunicipioDao;
import com.tl.entidades.Municipio;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class MunicipioManager {
    
    private MunicipioDao municipioDao;
    
    public MunicipioManager(){
        //Esto es un constructor
        municipioDao = new MunicipioDao();
    }
    
        
    public List<Municipio> listarMunicipiosPorIdDepto(int idDepto){
        System.out.println("manager.MunicipioManager.listarMunicipiosPorIdDepto()Start:");
        List<Municipio> listadoMunicipio = municipioDao.listarMunicipiosPorIdDepto(idDepto);
        return listadoMunicipio;
    }
    
}
