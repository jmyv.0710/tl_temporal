/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.EstudioSeguridadDao;
import com.tl.entidades.Estudioseguridad;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class EstudioSeguridadManager {
    
    private EstudioSeguridadDao estudioSeguridadDao;
    
    public EstudioSeguridadManager(){
        //Esto es un constructor
        estudioSeguridadDao = new EstudioSeguridadDao();
    }
    
    public String crearEstudioSeg(Estudioseguridad estudioseguridad){
        System.out.println("manager.EstudioSeguridadManager.crearEstudioSeg()Start");
        estudioSeguridadDao.crearEstudioSeg(estudioseguridad);
        return "Exito";
    }
    
    public List<Estudioseguridad> listarEstudioSeg(){
        System.out.println("manager.EstudioSeguridadManager.listarEstudioSeg()Start:");
        List<Estudioseguridad> listadoEstudio = estudioSeguridadDao.listarEstudioSeg();
        return listadoEstudio;
    }
    
}
