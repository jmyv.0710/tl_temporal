/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.DireccionDao;
import com.tl.entidades.Direccion;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class DireccionManager {
    
    private DireccionDao direccionDao;

    public DireccionManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        direccionDao= new DireccionDao();
    }
    
    public boolean crearDireccion(Direccion direccion){
        return direccionDao.crearDireccion(direccion);
    }
    
    public Direccion consultarDireccion(String direccion){
        return direccionDao.consultarDireccion(direccion);
    }
    
    public List <Direccion> listarDirecciones(){
        return direccionDao.listarDirecciones();
    }
    
}
