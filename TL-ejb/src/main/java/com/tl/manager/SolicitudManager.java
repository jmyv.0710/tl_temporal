/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.SolicitudDAOImpl;
import com.tl.entidades.Solicitud;
import java.util.List;

/**
 *
 * @author 
 */
public class SolicitudManager {
    
    private SolicitudDAOImpl solicitudDAOImpl;
    
    public SolicitudManager(){
        //Esto es un constructor
        solicitudDAOImpl = new SolicitudDAOImpl();
    }
    
    public boolean adicionarSolicitud(Solicitud solicitud){
        System.out.println("manager.SolicitudManager.adicionarSolicitud()Start");
        return solicitudDAOImpl.adicionarSolicitud(solicitud);
    }
    
    public List<Solicitud> listarSolicitudes(){
        System.out.println("manager.SolicitudManager.listarSolicitudes()Start:");
        List<Solicitud> listadoSolicitudes = solicitudDAOImpl.listaSolicitudes();
        return listadoSolicitudes;
    }
}
