/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.RutaDao;
import com.tl.entidades.Ruta;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class RutaManager {
    
    private RutaDao rutaDao;
    
    public RutaManager(){
        //Esto es un constructor
        rutaDao = new RutaDao();
    }
    
    public String crearRuta(Ruta ruta){
        System.out.println("manager.RutaManager.crearRuta()Start");
        rutaDao.crearRuta(ruta);
        return "Exito";
    }
    
    public List<Ruta> listarRutas(){
        System.out.println("manager.RutaManager.listarRutas()Start:");
        List<Ruta> listadoRutas = rutaDao.listarRutas();
        return listadoRutas;
    }
    
}
