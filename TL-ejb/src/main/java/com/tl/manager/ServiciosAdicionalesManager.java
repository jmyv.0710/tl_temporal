/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.ServiciosAdicionalesDao;
import com.tl.entidades.ServiciosAdicionales;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class ServiciosAdicionalesManager {
    
    private ServiciosAdicionalesDao serviciosAdicionalesDao;

    public ServiciosAdicionalesManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        serviciosAdicionalesDao= new ServiciosAdicionalesDao();
    }
    
    public boolean crearServiciosAdicionales(ServiciosAdicionales serviciosAd){
        return serviciosAdicionalesDao.crearServiciosAdicionales(serviciosAd);
    }
    
    public ServiciosAdicionales consultarServicioAdicional(int idServicioAd){
        return serviciosAdicionalesDao.consultarServicioAdicional(idServicioAd);
    }
    
    public List <ServiciosAdicionales> listarServiciosAdicionales(){
        return serviciosAdicionalesDao.listarServiciosAdicionales();
    }
    
}
