/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.VehiculoDao;
import com.tl.entidades.Vehiculo;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class VehiculoManager {
    
    private VehiculoDao vehiculoDao;

    public VehiculoManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        vehiculoDao= new VehiculoDao();
    }
    
    public boolean crearVehiculo(Vehiculo vehiculo){
        return vehiculoDao.crearVehiculo(vehiculo);
    }
    
    public boolean inactivarVehiculo(String placa){
        return vehiculoDao.inactivarVehiculo(placa);
    }
    
    public List <Vehiculo> listarVehiculos(){
        return vehiculoDao.listarVehiculos();
    }

}
