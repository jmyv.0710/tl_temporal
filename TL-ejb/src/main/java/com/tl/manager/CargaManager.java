/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.CargaDao;
import com.tl.entidades.Carga;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class CargaManager {
    
    private CargaDao cargaDao;

    public CargaManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        cargaDao= new CargaDao();
    }
    
    public boolean crearCarga(Carga carga){
        return cargaDao.crearCarga(carga);
    }
    
    
    public List <Carga> listarCargas(){
        return cargaDao.listarCargas();
    }
    
}
