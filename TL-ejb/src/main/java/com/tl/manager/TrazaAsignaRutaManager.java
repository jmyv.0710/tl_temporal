/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;


import com.tl.dao.TrazaAsignaRutaDao;
import com.tl.entidades.Trazabilidadasignacionruta;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class TrazaAsignaRutaManager {
    
    private TrazaAsignaRutaDao trazaAsignaRutaDao;
    
    public TrazaAsignaRutaManager(){
        //Esto es un constructor
        trazaAsignaRutaDao = new TrazaAsignaRutaDao();
    }
    
    public String crearTrazaAsignaRuta(Trazabilidadasignacionruta trazaAsignaRuta){
        System.out.println("manager.TrazaAsignaEstadoManager.crearTrazaAsignaRuta()Start");
        trazaAsignaRutaDao.crearTrazaAsignaRuta(trazaAsignaRuta);
        return "Exito";
    }
    
    public List<Trazabilidadasignacionruta> listarTrazaAsignaRuta(){
        System.out.println("manager.TrazaAsignaEstadoManager.listarTrazaAsignaRuta()Start:");
        List<Trazabilidadasignacionruta> listarTrazaAsignaRuta = trazaAsignaRutaDao.listarTrazaAsignaRuta();
        return listarTrazaAsignaRuta;
    }
    
    public Trazabilidadasignacionruta consultarTrazaPorAsignacion(int idAsignacion) {
        System.out.println("manager.TrazaAsignaEstadoManager.consultarTrazaPorAsignacion()Start:");
        Trazabilidadasignacionruta trazaAsignaEstado = trazaAsignaRutaDao.consultarTrazaPorAsignacion(idAsignacion);
        return trazaAsignaEstado;
    }
    
    public Trazabilidadasignacionruta consultarTrazaPorRutaAnt(int idEstadoAnt) {
        System.out.println("manager.TrazaAsignaEstadoManager.consultarTrazaPorRutaAnt()Start:");
        Trazabilidadasignacionruta trazaRutaAnt = trazaAsignaRutaDao.consultarTrazaPorRutaAnt(idEstadoAnt);
        return trazaRutaAnt;
    }

    public Trazabilidadasignacionruta consultarTrazaPorRutaNuev(int idEstadoNuev) {
        System.out.println("manager.TrazaAsignaEstadoManager.consultarTrazaPorRutaNuev()Start:");
        Trazabilidadasignacionruta trazaAsignaRuta = trazaAsignaRutaDao.consultarTrazaPorRutaNuev(idEstadoNuev);
        return trazaAsignaRuta;
    }
}
