/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.ClienteDao;
import com.tl.entidades.Cliente;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class ClienteManager {
    
    private ClienteDao clienteDao;

    public ClienteManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        clienteDao= new ClienteDao();
    }
    
    public boolean crearCliente(Cliente cliente){
        return clienteDao.crearCliente(cliente);
    }
    
    public Cliente consultarClienteDocto(String nrodocumentod){
        return clienteDao.consultarClienteDocto(nrodocumentod);
    }
    
    public List <Cliente> listarClientes(){
        return clienteDao.listarClientes();
    }
    
}
