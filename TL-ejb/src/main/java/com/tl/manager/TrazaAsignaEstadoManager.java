/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.TrazaAsignaEstadoDao;
import com.tl.entidades.Trazabilidadasignacionestado;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class TrazaAsignaEstadoManager {
    
    private TrazaAsignaEstadoDao trazaAsignaEstadoDao;
    
    public TrazaAsignaEstadoManager(){
        //Esto es un constructor
        trazaAsignaEstadoDao = new TrazaAsignaEstadoDao();
    }
    
    public String crearTrazaAsignaEstado(Trazabilidadasignacionestado trazaAsignaEstado){
        System.out.println("manager.TrazaAsignaEstadoManager.crearTrazaAsignaEstado()Start");
        trazaAsignaEstadoDao.crearTrazaAsignaEstado(trazaAsignaEstado);
        return "Exito";
    }
    
    public List<Trazabilidadasignacionestado> listarTrazaAsignaEstado(){
        System.out.println("manager.TrazaAsignaEstadoManager.listarTrazaAsignaEstado()Start:");
        List<Trazabilidadasignacionestado> listarTrazaAsignaEstado = trazaAsignaEstadoDao.listarTrazaAsignaEstado();
        return listarTrazaAsignaEstado;
    }
    
    public Trazabilidadasignacionestado consultarTrazaPorAsignacion(int idAsignacion) {
        System.out.println("manager.TrazaAsignaEstadoManager.consultarTrazaPorAsignacion()Start:");
        Trazabilidadasignacionestado trazaAsignaEstado = trazaAsignaEstadoDao.consultarTrazaPorAsignacion(idAsignacion);
        return trazaAsignaEstado;
    }
    
    public Trazabilidadasignacionestado consultarTrazaPorEstadoAnt(int idEstadoAnt) {
        System.out.println("manager.TrazaAsignaEstadoManager.consultarTrazaPorEstadoAnt()Start:");
        Trazabilidadasignacionestado trazaAsignaEstado = trazaAsignaEstadoDao.consultarTrazaPorEstadoAnt(idEstadoAnt);
        return trazaAsignaEstado;
    }

    public Trazabilidadasignacionestado consultarTrazaPorEstadoNuev(int idEstadoNuev) {
        System.out.println("manager.TrazaAsignaEstadoManager.consultarTrazaPorEstadoNuev()Start:");
        Trazabilidadasignacionestado trazaAsignaEstado = trazaAsignaEstadoDao.consultarTrazaPorEstadoNuev(idEstadoNuev);
        return trazaAsignaEstado;
    }
}
