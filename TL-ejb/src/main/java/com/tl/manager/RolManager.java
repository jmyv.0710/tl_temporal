/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.RolesDao;
import com.tl.entidades.Rol;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class RolManager {
    
    private RolesDao rolesDao;

    public RolManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        rolesDao= new RolesDao();
    }
    
    public boolean crearRol(Rol rol){
        return rolesDao.crearRol(rol);
    }
    
    public boolean inactivarRol(int idRol){
        return rolesDao.inactivarRol(idRol);
    }
    
    public List <Rol> listarRoles(){
        return rolesDao.listarRoles();
    }
    
}
