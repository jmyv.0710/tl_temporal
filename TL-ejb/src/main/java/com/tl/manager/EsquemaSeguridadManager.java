/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.EsquemaSeguridadDao;
import com.tl.entidades.Esquemaseguridad;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class EsquemaSeguridadManager {
    
    private EsquemaSeguridadDao esquemaSeguridadDao;

    public EsquemaSeguridadManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        esquemaSeguridadDao= new EsquemaSeguridadDao();
    }
    
    public boolean crearEsquemaSeguridad(Esquemaseguridad esquemaS){
        return esquemaSeguridadDao.crearEsquemaSeguridad(esquemaS);
    }
    
    public Esquemaseguridad consultarEsquemaSeguridad(String nroIdentificacion){
        return esquemaSeguridadDao.consultarEsquemaSeguridad(nroIdentificacion);
    }
    
    public List <Esquemaseguridad> listarEsquemaSeguridad(){
        return esquemaSeguridadDao.listarEsquemaSeguridad();
    }
    
}
