/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.DepartamentoDao;
import com.tl.entidades.Departamento;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class DepartamentoManager {
    
    private DepartamentoDao departamentoDao;
    
    public DepartamentoManager(){
        //Esto es un constructor
        departamentoDao = new DepartamentoDao();
    }
    
        
    public List<Departamento> listarDepartamentos(){
        System.out.println("manager.DepartamentoManager.listarDepartamentos()Start:");
        List<Departamento> listadoDepartamento = departamentoDao.listarDepartamentos();
        return listadoDepartamento;
    }
    
}
