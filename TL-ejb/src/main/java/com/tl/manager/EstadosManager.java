/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.EstadosDao;
import com.tl.entidades.Estado;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class EstadosManager {
    
    private EstadosDao estadosDao;
    
    public EstadosManager(){
        //Esto es un constructor
        estadosDao = new EstadosDao();
    }
    
    public String crearEstado(Estado estado){
        System.out.println("manager.EstadosManager.crearEstado()Start");
        estadosDao.crearEstado(estado);
        return "Exito";
    }
    
    public List<Estado> listarEstados(){
        System.out.println("manager.EstadosManager.listarEstados()Start:");
        List<Estado> listadoEstados = estadosDao.listarEstados();
        return listadoEstados;
    }
    
}
