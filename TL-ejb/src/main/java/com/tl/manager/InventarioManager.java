/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.InventarioDao;
import com.tl.entidades.Inventario;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class InventarioManager {
    
    private InventarioDao inventarioDao;
    
    public InventarioManager(){
        //Esto es un constructor
        inventarioDao = new InventarioDao();
    }
    
    public String crearInventario(Inventario inventario){
        System.out.println("manager.InventarioManager.crearInventario()Start");
        inventarioDao.crearInventario(inventario);
        return "Exito";
    }
    
    public List<Inventario> listarInventario(){
        System.out.println("manager.InventarioManager.listarInventario()Start:");
        List<Inventario> listadoInventario = inventarioDao.listarInventario();
        return listadoInventario;
    }
    
}
