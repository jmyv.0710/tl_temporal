/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.manager;

import com.tl.dao.DireccionClienteDao;
import com.tl.entidades.DireccionesCliente;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class DireccionClienteManager {
    
    private DireccionClienteDao direccionClienteDao;

    public DireccionClienteManager() {
        
        inicializarVariable ();
    }
    
     
    
    private void inicializarVariable (){
        direccionClienteDao= new DireccionClienteDao();
    }
    
    public boolean crearDireccionCliente(DireccionesCliente direccionC){
        return direccionClienteDao.crearDireccionCliente(direccionC);
    }
    
    public DireccionesCliente consultarDireccionC(String direccion1){
        return direccionClienteDao.consultarDireccionC(direccion1);
    }
    
    public List <DireccionesCliente> listarDireccionesC(int idCliente){
        return direccionClienteDao.listarDireccionesC(idCliente);
    }
    
}
