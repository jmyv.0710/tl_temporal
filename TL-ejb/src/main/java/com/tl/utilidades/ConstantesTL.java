/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.utilidades;

/**
 *
 * @author johan.yepez
 */
public class ConstantesTL {
    
    //FORMATOS DE FECHA Y HORA
    public static final String FORMATO_FECHA_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FORMATO_FECHA_DD_MM_YYYY = "dd-MM-yyyy";
    public static final String FORMATO_FECHA_YYYY_MM_DD_HORA = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMATO_FECHA_DD_MM_YYYY_HORA = "dd-MM-yyyy HH:mm:SS";
    public static final String MINUTOS_00_00_00 = " 00:00:00";
    public static final String MINUTOS_23_59_59 = " 23:59:59";
    
}
