/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Manuel
 * Created: 28/10/2019
 */
DROP TABLE IF EXISTS auxiliar;
DROP TABLE IF EXISTS correosnotificacion;
DROP TABLE IF EXISTS direcciones_cliente;
DROP TABLE IF EXISTS estudioseguridad;
DROP TABLE IF EXISTS inventario;
DROP TABLE IF EXISTS mensajemotivacional;
DROP TABLE IF EXISTS observacionescliente;
DROP TABLE IF EXISTS precinto;
DROP TABLE IF EXISTS serviciosadicionales;
DROP TABLE IF EXISTS trazabilidadasignacionestado;
DROP TABLE IF EXISTS trazabilidadasignacionruta;
DROP TABLE IF EXISTS asignacion;
DROP TABLE IF EXISTS esquemaseguridad;
DROP TABLE IF EXISTS estados;
DROP TABLE IF EXISTS nominatraficolibre;
DROP TABLE IF EXISTS ruta;
DROP TABLE IF EXISTS agendavehiculo;
DROP TABLE IF EXISTS conductor;
DROP TABLE IF EXISTS solicitud;
DROP TABLE IF EXISTS tiposervicio;
DROP TABLE IF EXISTS vehiculo;
DROP TABLE IF EXISTS carga;
DROP TABLE IF EXISTS direccion;
DROP TABLE IF EXISTS nominacliente;
DROP TABLE IF EXISTS rol;
DROP TABLE IF EXISTS cliente;

CREATE TABLE agendavehiculo (
    idagenda          INTEGER NOT NULL AUTO_INCREMENT,
    idvehiculo        INTEGER NOT NULL,
    idconductor       INTEGER NOT NULL,
    fechaasignacion   DATE,
    estado            INTEGER NOT NULL,
    fechacita         DATE,
    idsolicitud       INTEGER NOT NULL,
    usuariolog        VARCHAR(20),
    fechalog          DATE,
	PRIMARY KEY ( idagenda )
);

CREATE TABLE asignacion (
    idasignacion           INTEGER NOT NULL AUTO_INCREMENT,
    idnominatraficolibre   INTEGER NOT NULL,
    fechaenviocorreo       DATE,
    horaenviocorreo        DATE,
    tiemporeaccion         DATE,
    idestadoasignacion     INTEGER NOT NULL,
    idagendavehiculo       INTEGER NOT NULL,
    idsolicitud            INTEGER NOT NULL,
    horaactualizacion      DATE,
    idruta                 INTEGER NOT NULL,
    tiempototaloperacion   DATE,
    horaadicional          DATE,
    tarifafacturar         INTEGER,
    usuariolog             VARCHAR(20),
    fechalog               DATE,
	PRIMARY KEY ( idasignacion )
);

CREATE TABLE auxiliar (
    idauxiliar             INTEGER NOT NULL AUTO_INCREMENT,
    nombreauxiliar         VARCHAR(50),
    numeroidentificacion   INTEGER,
    direccion              VARCHAR(50),
    numerocontacto         INTEGER,
    tipocontrato           VARCHAR(50),
    idinventario           INTEGER NOT NULL,
    idestudioseguridad     INTEGER NOT NULL,
    usuariolog             VARCHAR(20),
    fechalog               DATE,
	PRIMARY KEY ( idauxiliar )
);

CREATE TABLE carga (
    idcarga             INTEGER NOT NULL AUTO_INCREMENT,
    peso                INTEGER,
    volumen             INTEGER,
    altura              INTEGER,
    ancho               INTEGER,
    largo               INTEGER,
    valorcarga          INTEGER,
    tipocarga           VARCHAR(50),
    responsablepoliza   VARCHAR(50),
    usuariolog          VARCHAR(20),
    fechalog            DATE,
	PRIMARY KEY ( idcarga )
);

CREATE TABLE cliente (
    idcliente                 INTEGER NOT NULL AUTO_INCREMENT,
    nombrecliente             VARCHAR(100),
    tipopersona               VARCHAR(50),
    nroidentificacion         VARCHAR(15),
    estado                    INTEGER NOT NULL,
    usuariolog                VARCHAR(20),
    fechalog                  DATE,
    idclientetercero          INTEGER,
    observacionesespeciales   VARCHAR(1024),
    tipocliente               CHAR(1),
	PRIMARY KEY ( idcliente )
);

CREATE TABLE conductor (
    idconductor       INTEGER NOT NULL AUTO_INCREMENT,
    nombreconductor   VARCHAR(50),
    tipodocumento     VARCHAR(20),
    nrodocumento      VARCHAR(15),
    tipoconductor     VARCHAR(30),
    direccion         VARCHAR(50),
    telefono          VARCHAR(20),
    correo            VARCHAR(30),
    estado            INTEGER NOT NULL,
    eps               VARCHAR(30),
    fondopension      VARCHAR(30),
    arl               VARCHAR(30),
    usuariolog        VARCHAR(20),
    fechalog          DATE,
    foto              BLOB,
    idrol             INTEGER NOT NULL,
    contrasenia       VARCHAR(20),
	PRIMARY KEY ( idconductor )
);

CREATE TABLE correosnotificacion (
    idcorreonotificacion   INTEGER NOT NULL AUTO_INCREMENT,
    nombrepersona          VARCHAR(100),
    correo                 VARCHAR(100),
    idcliente              INTEGER NOT NULL,
    usuariolog             VARCHAR(20),
    fechalog               DATE,
	PRIMARY KEY ( idcorreonotificacion )
);

CREATE TABLE direccion (
    iddireccion    INTEGER NOT NULL AUTO_INCREMENT,
    direccion      VARCHAR(50),
    barrio         VARCHAR(50),
    localidad      VARCHAR(50),
    ciudad         VARCHAR(50),
    departamento   VARCHAR(50),
    pais           VARCHAR(50),
    usuariolog     VARCHAR(20),
    fechalog       DATE,
	PRIMARY KEY ( iddireccion )
);

CREATE TABLE direcciones_cliente (
    iddireccionescliente   INTEGER NOT NULL AUTO_INCREMENT,
    tipodireccion          VARCHAR(30),
    direccion              VARCHAR(50),
    idcliente              INTEGER NOT NULL,
    usuariolog             VARCHAR(20),
    fechalog               DATE,
	PRIMARY KEY ( iddireccionescliente )
);

CREATE TABLE esquemaseguridad (
    idesquema           INTEGER NOT NULL AUTO_INCREMENT,
    nombreescolta       VARCHAR(100),
    nroidentificacion   VARCHAR(15),
    tiposeguridad       VARCHAR(50),
    tipoperimetro       VARCHAR(50),
    horarioservicio     DATE,
    tarifa              INTEGER,
    nivelcriticidad     CHAR(1),
    horallegada         DATE,
    horaretiro          DATE,
    estadoescolta       INTEGER NOT NULL,
    cantidadescoltas    INTEGER,
    usuariolog          VARCHAR(20),
    fechalog            DATE,
	PRIMARY KEY ( idesquema )
);

CREATE TABLE estados (
    idestados      INTEGER NOT NULL AUTO_INCREMENT,
    tipoestado     VARCHAR(30),
    nombreestado   VARCHAR(50),
    descripcion    VARCHAR(100),
    usuariolog     VARCHAR(20),
    fechalog       DATE,
	PRIMARY KEY ( idestados )
);

CREATE TABLE estudioseguridad (
    idestudioseguridad     INTEGER NOT NULL AUTO_INCREMENT,
    tipoestudioseguridad   VARCHAR(150),
    observacion            VARCHAR(256),
    fechavencimiento       DATE,
    tipodocumento          VARCHAR(150),
    rutadocumento          VARCHAR(256),
    estado                 INTEGER NOT NULL,
    idcliente              INTEGER NOT NULL,
    idconductor            INTEGER NOT NULL,
    idvehiculo             INTEGER NOT NULL,
    usuariolog             VARCHAR(20),
    fechalog               DATE,
	PRIMARY KEY ( idestudioseguridad )
);

CREATE TABLE inventario (
    idinventario   INTEGER NOT NULL AUTO_INCREMENT,
    nombreequipo   VARCHAR(100),
    tipoequipo     VARCHAR(50),
    cantidad       INTEGER,
    observacion    VARCHAR(200),
    epp            VARCHAR(100),
    usuariolog     VARCHAR(20),
    fechalog       DATE,
	PRIMARY KEY ( idinventario )
);

CREATE TABLE mensajemotivacional (
    idmensaje    INTEGER NOT NULL AUTO_INCREMENT,
    mensaje      VARCHAR(256),
    fechauso     DATE,
    estado       INTEGER,
    fechalog     DATE,
    usuariolog   VARCHAR(20),
	PRIMARY KEY ( idmensaje )
);

CREATE TABLE nominacliente (
    idnominacliente           INTEGER NOT NULL AUTO_INCREMENT,
    nombresolicitante         VARCHAR(50),
    identifacionsolicitante   VARCHAR(15),
    estado                    INTEGER NOT NULL,
    cargo                     VARCHAR(50),
    idcliente                 INTEGER NOT NULL,
    tipodocumento             VARCHAR(20),
    usuariolog                VARCHAR(20),
    fechalog                  DATE,
    contrasenia               VARCHAR(20),
    idrol                     INTEGER NOT NULL,
	PRIMARY KEY ( idnominacliente )
);

CREATE TABLE nominatraficolibre (
    idnomina          INTEGER NOT NULL AUTO_INCREMENT,
    tipodocumento     VARCHAR(20),
    nrodocumento      INTEGER,
    nombre            VARCHAR(50),
    direccion         VARCHAR(50),
    telefono          VARCHAR(20),
    correo            VARCHAR(30),
    fechanacimiento   DATE,
    usuariolog        VARCHAR(20),
    fechalog          DATE,
    contrasenia       VARCHAR(20),
    idrol             INTEGER NOT NULL,
    PRIMARY KEY ( idnomina )
);

CREATE TABLE observacionescliente (
    idobservaciones   INTEGER NOT NULL AUTO_INCREMENT,
    idsolicitud       INTEGER NOT NULL,
    trafico           VARCHAR(50),
    novedades         VARCHAR(200),
    observaciones     VARCHAR(200),
    calificacion      INTEGER,
    usuariolog        VARCHAR(20),
    fechalog          DATE,
    PRIMARY KEY ( idobservaciones )
);

CREATE TABLE precinto (
    idprecinto              INTEGER NOT NULL AUTO_INCREMENT,
    nroprecinto             INTEGER,
    fecharadicado           DATE,
    conductorasociado       INTEGER NOT NULL,
    fechauso                DATE,
    estado                  INTEGER NOT NULL,
    tipoprecinto            VARCHAR(30),
    fechaentregaconductor   DATE,
    observaciones           VARCHAR(100),
    usuariolog              VARCHAR(20),
    fechalog                DATE,
    PRIMARY KEY ( idprecinto )
);

CREATE TABLE rol (
    idrol        INTEGER NOT NULL AUTO_INCREMENT,
    rol          VARCHAR(50),
	descripcion  VARCHAR(50),
	estado       VARCHAR(50),
    usuariolog   VARCHAR(20),
    fechalog     DATE,
	PRIMARY KEY ( idrol )
);

CREATE TABLE ruta (
    idruta               INTEGER NOT NULL AUTO_INCREMENT,
    nombreruta           VARCHAR(50),
    rutainicio           VARCHAR(50),
    rutafin              VARCHAR(50),
    descripcion          VARCHAR(100),
    tiporuta             VARCHAR(50),
    tiempoestimadoruta   DATE,
    usuariolog           VARCHAR(20),
    fechalog             DATE,
	PRIMARY KEY ( idruta )
);

CREATE TABLE serviciosadicionales (
    idservicio              INTEGER NOT NULL AUTO_INCREMENT,
    nombreservicio          VARCHAR(50),
    descripcionservicio     VARCHAR(100),
    montacarga              CHAR(1),
    dispositivo             CHAR(1),
    auxiliar                CHAR(1),
    cantidadauxiliares      INTEGER,
    idauxiliar              INTEGER,
    idesquemaseguridad      INTEGER NOT NULL,
    requiereseguridad       CHAR(1),
    usuariolog              VARCHAR(20),
    fechalog                DATE,
    solicitud_idsolicitud   INTEGER NOT NULL,
	PRIMARY KEY ( idservicio )
);

CREATE TABLE solicitud (
    idsolicitud                    INTEGER NOT NULL AUTO_INCREMENT,
    nroreserva                     INTEGER,
    fechasolicitud                 DATE,
    fechacargue                    DATE,
    fpu                            CHAR(1),
    tarifa                         INTEGER,
    origencarga                    INTEGER NOT NULL,
    destinocarga                   INTEGER NOT NULL,
    idcarga                        INTEGER NOT NULL,
    valormercancia                 INTEGER,
    tiposervicio                   INTEGER NOT NULL,
    observaciones                  VARCHAR(200),
    serviciologisticaadicional     INTEGER NOT NULL,
    impoexpo                       CHAR(1),
    idlistaserrviciosadicionales   INTEGER NOT NULL,
    solicitadopor                  INTEGER NOT NULL,
    contactoorigen                 INTEGER NOT NULL,
    contactodestino                INTEGER NOT NULL,
    tiemporeaccionsolicitud        INTEGER,
    usuariolog                     VARCHAR(20),
    fechalog                       DATE,
	PRIMARY KEY ( idsolicitud )
);

CREATE TABLE tiposervicio (
    idtiposervicio       INTEGER NOT NULL AUTO_INCREMENT,
    nombretiposervicio   VARCHAR(30),
    horaservicio         DATE,
    usuariolog           VARCHAR(20),
    fechalog             DATE,
	PRIMARY KEY ( idtiposervicio )
);

CREATE TABLE trazabilidadasignacionestado (
    idtrazabilidad     INTEGER NOT NULL AUTO_INCREMENT,
    idasignacion       INTEGER NOT NULL,
    estadoanterior     INTEGER NOT NULL,
    nuevoestado        INTEGER NOT NULL,
    fechacambio        DATE,
    idempleadonomina   INTEGER NOT NULL,
    kpi                VARCHAR(50),
    observacion        VARCHAR(100),
    tipokpi            VARCHAR(30),
    fecharegistro      DATE,
    usuariolog         VARCHAR(20),
    fechalog           DATE,
	PRIMARY KEY ( idtrazabilidad )
);

CREATE TABLE trazabilidadasignacionruta (
    idtrazabilidad          INTEGER NOT NULL AUTO_INCREMENT,
    idasignacion            INTEGER NOT NULL,
    idestados               INTEGER,
    ubicacionregistro       VARCHAR(100),
    observacionesregistro   VARCHAR(100),
    fecharegistro           DATE,
    numeroregistro          INTEGER,
    estadoanterior          INTEGER NOT NULL,
    nuevoestado             INTEGER NOT NULL,
    usuariolog              VARCHAR(20),
    fechalog                DATE,
	PRIMARY KEY ( idtrazabilidad )
);

CREATE TABLE vehiculo (
    idvehiculo        INTEGER NOT NULL AUTO_INCREMENT,
    tipovehiculo      VARCHAR(50),
    placa             VARCHAR(8),
    tipopropietario   VARCHAR(50),
    estado            INTEGER NOT NULL,
    capacidad         INTEGER,
    cilindraje        INTEGER,
    marca             VARCHAR(30),
    modelo            VARCHAR(30),
    nrochasis         VARCHAR(30),
    nromotor          VARCHAR(30),
    cantidadruedas    INTEGER,
    foto              BLOB,
    soat              VARCHAR(30),
    rtm               VARCHAR(30),
    fechasoat         DATE,
    fechartm          DATE,
    usuariolog        VARCHAR(20),
    fechalog          DATE,
	PRIMARY KEY ( idvehiculo )
);


CREATE TABLE Departamento (
    iddepartamento                 INTEGER NOT NULL AUTO_INCREMENT,
    abreviatura             VARCHAR(100),
    nombre               VARCHAR(50),
	PRIMARY KEY ( iddepartamento )
);        


DROP TABLE IF EXISTS Municipio;

CREATE TABLE Municipio (
    idmunicipio             INTEGER NOT NULL AUTO_INCREMENT,
    iddepartamento          INTEGER NOT NULL,
    nombre               VARCHAR(100),
	PRIMARY KEY ( idmunicipio )
);

ALTER TABLE agendavehiculo
    ADD CONSTRAINT agendavehiculo_conductor_fk FOREIGN KEY ( idconductor )
        REFERENCES conductor ( idconductor );

ALTER TABLE agendavehiculo
    ADD CONSTRAINT agendavehiculo_solicitud_fk FOREIGN KEY ( idsolicitud )
        REFERENCES solicitud ( idsolicitud );

ALTER TABLE agendavehiculo
    ADD CONSTRAINT agendavehiculo_vehiculo_fk FOREIGN KEY ( idvehiculo )
        REFERENCES vehiculo ( idvehiculo );

ALTER TABLE asignacion
    ADD CONSTRAINT asigna_nominatl_fk FOREIGN KEY ( idnominatraficolibre )
        REFERENCES nominatraficolibre ( idnomina );

ALTER TABLE asignacion
    ADD CONSTRAINT asignacion_agendavehiculo_fk FOREIGN KEY ( idagendavehiculo )
        REFERENCES agendavehiculo ( idagenda );

ALTER TABLE asignacion
    ADD CONSTRAINT asignacion_estados_fk FOREIGN KEY ( idestadoasignacion )
        REFERENCES estados ( idestados );

ALTER TABLE asignacion
    ADD CONSTRAINT asignacion_ruta_fk FOREIGN KEY ( idruta )
        REFERENCES ruta ( idruta );

ALTER TABLE asignacion
    ADD CONSTRAINT asignacion_solicitud_fk FOREIGN KEY ( idsolicitud )
        REFERENCES solicitud ( idsolicitud );

ALTER TABLE auxiliar
    ADD CONSTRAINT auxiliar_estudioseguridad_fk FOREIGN KEY ( idestudioseguridad )
        REFERENCES estudioseguridad ( idestudioseguridad );

ALTER TABLE auxiliar
    ADD CONSTRAINT auxiliar_inventario_fk FOREIGN KEY ( idinventario )
        REFERENCES inventario ( idinventario );

ALTER TABLE cliente
    ADD CONSTRAINT cliente_cliente_fk FOREIGN KEY ( idclientetercero )
        REFERENCES cliente ( idcliente );

ALTER TABLE conductor
    ADD CONSTRAINT conductor_rol_fk FOREIGN KEY ( idrol )
        REFERENCES rol ( idrol );

ALTER TABLE correosnotificacion
    ADD CONSTRAINT correonotifica_nomclient_fk FOREIGN KEY ( idcliente )
        REFERENCES nominacliente ( idnominacliente );

ALTER TABLE direcciones_cliente
    ADD CONSTRAINT direcciones_cliente_cliente_fk FOREIGN KEY ( idcliente )
        REFERENCES cliente ( idcliente );

ALTER TABLE estudioseguridad
    ADD CONSTRAINT estudioseguridad_cliente_fk FOREIGN KEY ( idcliente )
        REFERENCES cliente ( idcliente );

ALTER TABLE estudioseguridad
    ADD CONSTRAINT estudioseguridad_conductor_fk FOREIGN KEY ( idconductor )
        REFERENCES conductor ( idconductor );

ALTER TABLE estudioseguridad
    ADD CONSTRAINT estudioseguridad_vehiculo_fk FOREIGN KEY ( idvehiculo )
        REFERENCES vehiculo ( idvehiculo );

ALTER TABLE nominacliente
    ADD CONSTRAINT nominacliente_cliente_fk FOREIGN KEY ( idcliente )
        REFERENCES cliente ( idcliente );

ALTER TABLE nominacliente
    ADD CONSTRAINT nominacliente_rol_fk FOREIGN KEY ( idrol )
        REFERENCES rol ( idrol );

-- ALTER TABLE nominatraficolibre
--     ADD CONSTRAINT nominatraficolibre_rol_fk FOREIGN KEY ( idrol )
--         REFERENCES rol ( idrol );

ALTER TABLE observacionescliente
    ADD CONSTRAINT observacionclient_solic_fk FOREIGN KEY ( idsolicitud )
        REFERENCES solicitud ( idsolicitud );

ALTER TABLE precinto
    ADD CONSTRAINT precinto_conductor_fk FOREIGN KEY ( conductorasociado )
        REFERENCES conductor ( idconductor );

ALTER TABLE serviciosadicionales
    ADD CONSTRAINT serviciosad_esqseguridad_fk FOREIGN KEY ( idesquemaseguridad )
        REFERENCES esquemaseguridad ( idesquema );

ALTER TABLE serviciosadicionales
    ADD CONSTRAINT serviciosad_solicitud_fk FOREIGN KEY ( solicitud_idsolicitud )
        REFERENCES solicitud ( idsolicitud );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_carga_fk FOREIGN KEY ( idcarga )
        REFERENCES carga ( idcarga );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_direccion_fk FOREIGN KEY ( origencarga )
        REFERENCES direccion ( iddireccion );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_direccion_fkv2 FOREIGN KEY ( destinocarga )
        REFERENCES direccion ( iddireccion );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_nominacliente_fk FOREIGN KEY ( solicitadopor )
        REFERENCES nominacliente ( idnominacliente );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_nominacliente_fkv2 FOREIGN KEY ( contactoorigen )
        REFERENCES nominacliente ( idnominacliente );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_nominacliente_fkv3 FOREIGN KEY ( contactodestino )
        REFERENCES nominacliente ( idnominacliente );

ALTER TABLE solicitud
    ADD CONSTRAINT solicitud_tiposervicio_fk FOREIGN KEY ( tiposervicio )
        REFERENCES tiposervicio ( idtiposervicio );

ALTER TABLE trazabilidadasignacionestado
    ADD CONSTRAINT trazaasigestado_asign_fk FOREIGN KEY ( idasignacion )
        REFERENCES asignacion ( idasignacion );

ALTER TABLE trazabilidadasignacionestado
    ADD CONSTRAINT trazaasigestado_estado_fk FOREIGN KEY ( estadoanterior )
        REFERENCES estados ( idestados );

ALTER TABLE trazabilidadasignacionestado
    ADD CONSTRAINT trazaasigestado_nominatl_fk FOREIGN KEY ( idempleadonomina )
        REFERENCES nominatraficolibre ( idnomina );

ALTER TABLE trazabilidadasignacionruta
    ADD CONSTRAINT trazaasignaruta_estado_fk FOREIGN KEY ( estadoanterior )
        REFERENCES estados ( idestados );

ALTER TABLE trazabilidadasignacionruta
    ADD CONSTRAINT trazaasignaruta_estado_fkv2 FOREIGN KEY ( nuevoestado )
        REFERENCES estados ( idestados );

ALTER TABLE trazabilidadasignacionestado
    ADD CONSTRAINT trazaasignestado_estad_fkv2 FOREIGN KEY ( nuevoestado )
        REFERENCES estados ( idestados );

ALTER TABLE trazabilidadasignacionruta
    ADD CONSTRAINT trazaasigruta_asigna_fk FOREIGN KEY ( idasignacion )
        REFERENCES asignacion ( idasignacion );

ALTER TABLE Municipio
    ADD CONSTRAINT municipio_departamento_fk FOREIGN KEY ( iddepartamento )
        REFERENCES Departamento ( iddepartamento );    