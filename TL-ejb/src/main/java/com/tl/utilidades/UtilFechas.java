/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author johan.yepez
 */
public class UtilFechas {
    
    public static String obtenerFechaSistemaString(){
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaSistemaString()inicio");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD);
        return formato.format(new Date());
    }
    
    public static String obtenerFechaSistemaConHoraString(){
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaSistemaConHoraString()inicio");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD_HORA);
        return formato.format(new Date());
    }
    
    public static String obtenerFechaFormatoString(Date fecha){
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaFormatoString()inicio");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD);
        return formato.format(fecha);
    }
    
    public static String obtenerFechaFormatoConHoraString(Date fecha){
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaFormatoString()inicio");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD_HORA);
        return formato.format(fecha);
    }
    
    public static Date obtenerFechaSistemaDate() throws ParseException {
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaSistemaDate()");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD);
        return formato.parse(obtenerFechaSistemaString());
    }
    
    public static Date obtenerFechaFormatoConHoraDate(String fecha) throws ParseException {
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaFormatoString()inicio");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD_HORA);
        return formato.parse(fecha);
    }
    
    public static Date obtenerFechaDate(String fecha) throws ParseException {
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaFormatoString()inicio");
        SimpleDateFormat formato = new SimpleDateFormat(ConstantesTL.FORMATO_FECHA_YYYY_MM_DD);
        return formato.parse(fecha);
    }
    
    public static Date obtenerFechaMinimaMayorDeEdad(){
        System.out.println("com.ga.utilidades.UtilFechas.obtenerFechaMinimaMayorDeEdad()Inicio");
        Calendar fechaActual = Calendar.getInstance();
        int añoActual = fechaActual.get(Calendar.YEAR);
        int mesActual = fechaActual.get(Calendar.MONTH);
        int diaActual = fechaActual.get(Calendar.DATE);
        int añoNuevo = añoActual - 18;
        fechaActual.set(añoNuevo, mesActual, diaActual);
        return fechaActual.getTime();
    }
    
    public static int calcularEdad(Date fechaNacimiento){
        System.out.println("com.ga.utilidades.UtilFechas.calcularEdad()Inicio");
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        LocalDate fechaNac = LocalDate.parse(sdf.format(fechaNacimiento), fmt);
        LocalDate ahora = LocalDate.now();

        Period periodo = Period.between(fechaNac, ahora);
//        System.out.printf("Tu edad es: %s años, %s meses y %s días",
//                            periodo.getYears(), periodo.getMonths(), periodo.getDays());
        return periodo.getYears();
    }
}
