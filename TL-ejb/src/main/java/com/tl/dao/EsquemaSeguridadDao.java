/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Esquemaseguridad;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class EsquemaSeguridadDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public EsquemaSeguridadDao() {
    System.out.println("com.tl.dao.EsquemaSeguridadDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EsquemaSeguridadDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearEsquemaSeguridad(Esquemaseguridad esquemaS){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.EsquemaSeguridadDao.crearEsquemaSeguridad()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO esquemaseguridad VALUE (null,"
                    + "'" + esquemaS.getIdesquema() + "',"
                    + "'" + esquemaS.getFechalog() + "',"
                    + "'" + esquemaS.getNombreescolta() + "',"
                    + "'" + esquemaS.getNroidentificacion() + "',"
                    + "'" + esquemaS.getTiposeguridad() + "',"
                    + "'" + esquemaS.getTipoperimetro() + "',"
                    + "'" + esquemaS.getHorarioservicio() + "',"
                    + "'" + esquemaS.getTarifa() + "',"
                    + "'" + esquemaS.getNivelcriticidad() + "',"
                    + "'" + esquemaS.getHorallegada() + "',"
                    + "'" + esquemaS.getHoraretiro() + "',"
                    + "'" + esquemaS.getEstadoescolta() + "',"
                    + "'" + esquemaS.getCantidadescoltas() + "',"
                    + "'" + esquemaS.getUsuariolog() + "')";

            System.out.println("com.tl.dao.EsquemaSeguridadDao.crearEsquemaSeguridad()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego esquema de seguridad");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.EsquemaSeguridadDao.crearEsquemaSeguridad()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Esquemaseguridad> listarEsquemaSeguridad() {
        System.out.println("com.tl.dao.EsquemaSeguridadDao.listarEsquemaSeguridad()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Esquemaseguridad> listaEsquemaseguridad = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM esquemaseguridad");

            while (rs.next()) {
                Esquemaseguridad esquemaS = new Esquemaseguridad();
                esquemaS.setIdesquema(rs.getInt("idesquema"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                esquemaS.setNombreescolta(rs.getString("nombreescolta"));
                esquemaS.setNroidentificacion(rs.getString("nroidentificacion"));
                esquemaS.setTiposeguridad(rs.getString("tiposeguridad"));
                esquemaS.setTipoperimetro(rs.getString("tipoperimetro"));
                esquemaS.setHorarioservicio(rs.getDate("horarioservicio"));
                esquemaS.setTarifa(rs.getInt("tarifa"));
                esquemaS.setNivelcriticidad(rs.getString("nivelcriticidad"));
                esquemaS.setHorallegada(rs.getDate("horallegada"));
                esquemaS.setHoraretiro(rs.getDate("horaretiro"));
                esquemaS.setEstadoescolta(rs.getInt("estadoescolta"));
                esquemaS.setCantidadescoltas(rs.getInt("cantidadescoltas"));
                esquemaS.setUsuariolog(rs.getString("usuariolog"));
                esquemaS.setFechalog(rs.getDate("fechalog"));
                listaEsquemaseguridad.add(esquemaS);
            }
            rs.close();
            estatuto.close();
            return listaEsquemaseguridad;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EsquemaSeguridadDao.listarEsquemaSeguridad()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Esquemaseguridad consultarEsquemaSeguridad(String nroIdentificacion) {

        System.out.println("com.tl.dao.EsquemaSeguridadDao.consultarEsquemaSeguridad()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM esquemaseguridad WHERE nroidentificacion = '" + nroIdentificacion + "'";
            System.out.println("com.tl.dao.EsquemaSeguridadDao.consultarEsquemaSeguridad()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Esquemaseguridad esquemaS = new Esquemaseguridad();

            while (rs.next()) {

                esquemaS.setIdesquema(rs.getInt("idesquema"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                esquemaS.setNombreescolta(rs.getString("nombreescolta"));
                esquemaS.setNroidentificacion(rs.getString("nroidentificacion"));
                esquemaS.setTiposeguridad(rs.getString("tiposeguridad"));
                esquemaS.setTipoperimetro(rs.getString("tipoperimetro"));
                esquemaS.setHorarioservicio(rs.getDate("horarioservicio"));
                esquemaS.setTarifa(rs.getInt("tarifa"));
                esquemaS.setNivelcriticidad(rs.getString("nivelcriticidad"));
                esquemaS.setHorallegada(rs.getDate("horallegada"));
                esquemaS.setHoraretiro(rs.getDate("horaretiro"));
                esquemaS.setEstadoescolta(rs.getInt("estadoescolta"));
                esquemaS.setCantidadescoltas(rs.getInt("cantidadescoltas"));
                esquemaS.setUsuariolog(rs.getString("usuariolog"));
                esquemaS.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return esquemaS;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EsquemaSeguridadDao.consultarEsquemaSeguridad()error: " + e);
        }
        return null;

    }
    
}
