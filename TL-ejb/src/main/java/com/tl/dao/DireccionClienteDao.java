/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Direccion;
import com.tl.entidades.DireccionesCliente;
import com.tl.utilidades.UtilFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class DireccionClienteDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public DireccionClienteDao() {
    System.out.println("com.tl.dao.DireccionClienteDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionClienteDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearDireccionCliente(DireccionesCliente direccionC){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.DireccionClienteDao.crearDireccionCliente()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO direcciones_cliente VALUE (null,"
                    + "'" + direccionC.getTipodireccion()+ "',"
                    + "'" + direccionC.getDireccion()+ "',"
                    + "'" + direccionC.getIdCliente() + "',"
                    + "'" + direccionC.getUsuariolog() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoConHoraString(direccionC.getFechalog()) + "')";

            System.out.println("com.tl.dao.DireccionClienteDao.crearDireccionCliente()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego direccion cliente");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionClienteDao.crearDireccionCliente()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<DireccionesCliente> listarDireccionesC(int idCliente) {
        System.out.println("com.tl.dao.DireccionClienteDao.listarDireccionesC()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<DireccionesCliente> listaDireccionesC = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM direcciones_cliente WHERE idcliente = " + idCliente);

            while (rs.next()) {
                DireccionesCliente direccionC = new DireccionesCliente();
                direccionC.setIddireccionescliente(rs.getInt("iddireccionescliente"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                direccionC.setTipodireccion(rs.getString("tipodireccion"));
                direccionC.setDireccion(rs.getString("direccion"));
                direccionC.setIdCliente(rs.getInt("idcliente"));
                direccionC.setUsuariolog(rs.getString("usuariolog"));
                direccionC.setFechalog(rs.getDate("fechalog"));
                listaDireccionesC.add(direccionC);
            }
            rs.close();
            estatuto.close();
            return listaDireccionesC;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionClienteDao.listarDireccionesC()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public DireccionesCliente consultarDireccionC(String direccion1) {

        System.out.println("com.tl.dao.DireccionClienteDao.consultarDireccionC()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM direcciones_cliente WHERE direccion = '" + direccion1 + "'";
            System.out.println("com.tl.dao.DireccionClienteDao.consultarDireccionC()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            DireccionesCliente direccionC = new DireccionesCliente();

            while (rs.next()) {

                direccionC.setIddireccionescliente(rs.getInt("iddireccionescliente"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                direccionC.setTipodireccion(rs.getString("tipodireccion"));
                direccionC.setDireccion(rs.getString("direccion"));
                direccionC.setIdCliente(rs.getInt("idcliente"));
                direccionC.setUsuariolog(rs.getString("usuariolog"));
                direccionC.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return direccionC;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionClienteDao.consultarDireccionC()error: " + e);
        }
        return null;

    }

    
}
