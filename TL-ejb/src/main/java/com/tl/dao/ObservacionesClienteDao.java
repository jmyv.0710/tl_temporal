/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Observacionescliente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class ObservacionesClienteDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public ObservacionesClienteDao() {
    System.out.println("com.tl.dao.ObservacionesClienteDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ObservacionesClienteDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearObservaCliente(Observacionescliente observaCliente){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.ObservacionesClienteDao.crearObservaCliente()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO observacionescliente VALUE (null,"
                    + "'" + observaCliente.getIdobservaciones()+ "',"
                    + "'" + observaCliente.getIdSolicitud()+ "',"
                    + "'" + observaCliente.getTrafico()+ "',"
                    + "'" + observaCliente.getNovedades()+ "',"
                    + "'" + observaCliente.getObservaciones()+ "',"
                    + "'" + observaCliente.getCalificacion()+ "',"
                    + "'" + observaCliente.getFechalog() + "',"
                    + "'" + observaCliente.getUsuariolog() + "')";

            System.out.println("com.tl.dao.ObservacionesClienteDao.crearObservaCliente()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego observaciones");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.ObservacionesClienteDao.crearObservaCliente()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Observacionescliente> listarObservaClientes() {
        System.out.println("com.tl.dao.ObservacionesClienteDao.listarObservaClientes()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Observacionescliente> listaObservaClientes = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM observacionescliente");

            while (rs.next()) {
                Observacionescliente observaCliente = new Observacionescliente();
                observaCliente.setIdobservaciones(rs.getInt("idobservaciones"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                observaCliente.setIdSolicitud(rs.getInt("idsolicitud"));
                observaCliente.setTrafico(rs.getString("trafico"));
                observaCliente.setNovedades(rs.getString("novedades"));
                observaCliente.setObservaciones(rs.getString("observaciones"));
                observaCliente.setCalificacion(rs.getInt("calificacion"));
                observaCliente.setUsuariolog(rs.getString("usuariolog"));
                observaCliente.setFechalog(rs.getDate("fechalog"));
                listaObservaClientes.add(observaCliente);
            }
            rs.close();
            estatuto.close();
            return listaObservaClientes;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ObservacionesClienteDao.listarObservaClientes()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Observacionescliente consultarObservaCliente(int idSolicitud) {

        System.out.println("com.tl.dao.ObservacionesClienteDao.consultarObservaCliente()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM observacionescliente WHERE idsolicitud = '" + idSolicitud + "'";
            System.out.println("com.tl.dao.ObservacionesClienteDao.consultarObservaCliente()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Observacionescliente observaCliente = new Observacionescliente();

            while (rs.next()) {

                observaCliente.setIdobservaciones(rs.getInt("idobservaciones"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                observaCliente.setIdSolicitud(rs.getInt("idsolicitud"));
                observaCliente.setTrafico(rs.getString("trafico"));
                observaCliente.setNovedades(rs.getString("novedades"));
                observaCliente.setObservaciones(rs.getString("observaciones"));
                observaCliente.setCalificacion(rs.getInt("calificacion"));
                observaCliente.setUsuariolog(rs.getString("usuariolog"));
                observaCliente.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return observaCliente;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ObservacionesClienteDao.consultarObservaCliente()error: " + e);
        }
        return null;

    }

    
}
