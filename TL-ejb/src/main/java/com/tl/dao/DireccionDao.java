/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Direccion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class DireccionDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public DireccionDao() {
    System.out.println("com.tl.dao.DireccionDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearDireccion(Direccion direccion){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.DireccionDao.crearDireccion()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO direccion VALUE (null,"
                    + "'" + direccion.getIddireccion() + "',"
                    + "'" + direccion.getFechalog() + "',"
                    + "'" + direccion.getDireccion() + "',"
                    + "'" + direccion.getBarrio() + "',"
                    + "'" + direccion.getLocalidad() + "',"
                    + "'" + direccion.getCiudad() + "',"
                    + "'" + direccion.getDepartamento() + "',"
                    + "'" + direccion.getPais() + "',"
                    + "'" + direccion.getUsuariolog() + "')";

            System.out.println("com.tl.dao.DireccionDao.crearDireccion()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego direccion");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionDao.crearDireccion()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Direccion> listarDirecciones() {
        System.out.println("com.tl.dao.DireccionDao.listarDirecciones()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Direccion> listaDirecciones = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM direccion");

            while (rs.next()) {
                Direccion direccion = new Direccion();
                direccion.setIddireccion(rs.getInt("iddireccion"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                direccion.setDireccion(rs.getString("direccion"));
                direccion.setBarrio(rs.getString("barrio"));
                direccion.setLocalidad(rs.getString("localidad"));
                direccion.setCiudad(rs.getString("ciudad"));
                direccion.setDepartamento(rs.getString("departamento"));
                direccion.setPais(rs.getString("pais"));
                direccion.setUsuariolog(rs.getString("usuariolog"));
                direccion.setFechalog(rs.getDate("fechalog"));
                listaDirecciones.add(direccion);
            }
            rs.close();
            estatuto.close();
            return listaDirecciones;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionDao.listarDirecciones()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Direccion consultarDireccion(String direccion1) {

        System.out.println("com.tl.dao.DireccionDao.consultarDireccion()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM direccion WHERE direccion = '" + direccion1 + "'";
            System.out.println("com.tl.dao.DireccionDao.consultarDireccion()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Direccion direccion = new Direccion();

            while (rs.next()) {

                direccion.setIddireccion(rs.getInt("iddireccion"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                direccion.setDireccion(rs.getString("direccion"));
                direccion.setBarrio(rs.getString("barrio"));
                direccion.setLocalidad(rs.getString("localidad"));
                direccion.setCiudad(rs.getString("ciudad"));
                direccion.setDepartamento(rs.getString("departamento"));
                direccion.setPais(rs.getString("pais"));
                direccion.setUsuariolog(rs.getString("usuariolog"));
                direccion.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return direccion;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DireccionDao.consultarDireccion()error: " + e);
        }
        return null;

    }
    

    
    
}
