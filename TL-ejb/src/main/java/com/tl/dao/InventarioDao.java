/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Inventario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class InventarioDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public InventarioDao() {
    System.out.println("com.tl.dao.InventarioDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.InventarioDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearInventario(Inventario inventario){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.InventarioDao.crearInventario()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO inventario VALUE (null,"
                    + "'" + inventario.getNombreequipo()+ "',"
                    + "'" + inventario.getTipoequipo()+ "',"
                    + "'" + inventario.getCantidad()+ "',"
                    + "'" + inventario.getObservacion()+ "',"
                    + "'" + inventario.getEpp()+ "',"
                    + "'" + inventario.getUsuariolog() + "',"
                    + "'" + inventario.getFechalog() + "')";

            System.out.println("com.tl.dao.InventarioDao.crearInventario()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego inventario");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.InventarioDao.crearInventario()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Inventario> listarInventario() {
        System.out.println("com.tl.dao.InventarioDao.listarInventario()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Inventario> listaInventario = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM inventario");

            while (rs.next()) {
                Inventario inventario = new Inventario();
                inventario.setIdinventario(rs.getInt("idinventario"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                inventario.setNombreequipo(rs.getString("nombreequipo"));
                inventario.setTipoequipo(rs.getString("tipoequipo"));
                inventario.setCantidad(rs.getInt("cantidad"));
                inventario.setObservacion(rs.getString("observacion"));
                inventario.setEpp(rs.getString("epp"));
                inventario.setUsuariolog(rs.getString("usuariolog"));
                inventario.setFechalog(rs.getDate("fechalog"));
                listaInventario.add(inventario);
            }
            rs.close();
            estatuto.close();
            return listaInventario;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.InventarioDao.listarInventario()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
}
