/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Vehiculo;
import com.tl.utilidades.UtilFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Naticha
 */
public class VehiculoDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public VehiculoDao() {
    
        System.out.println("com.tl.dao.VehiculoDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.VehiculoDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    public boolean crearVehiculo(Vehiculo vehiculo){
        
        boolean valida;
        
        System.out.println("com.tl.dao.VehiculoDao.crearVehiculo()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO vehiculo VALUE (null,"
                    + "'" + vehiculo.getTipovehiculo()+ "',"
                    + "'" + vehiculo.getPlaca() + "',"
                    + "'" + vehiculo.getTipopropietario() + "',"
                    + "'" + vehiculo.getEstado()+ "',"
                    + "'" + vehiculo.getCapacidad() + "',"
                    + "'" + vehiculo.getCilindraje() + "',"
                    + "'" + vehiculo.getMarca() + "',"
                    + "'" + vehiculo.getModelo() + "',"
                    + "'" + vehiculo.getNrochasis() + "',"
                    + "'" + vehiculo.getNromotor() + "',"
                    + "'" + vehiculo.getCantidadruedas() + "',"
                    + "'" + vehiculo.getFoto() + "',"
                    + "'" + vehiculo.getSoat() + "',"
                    + "'" + vehiculo.getRtm() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoString(vehiculo.getFechasoat()) + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoString(vehiculo.getFechartm()) + "',"
                    + "'" + "ADMIN" + "',"
//                    + "'" + vehiculo.getUsuariolog() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoString(new Date()) + "')";

            System.out.println("com.tl.dao.VehiculoDao.crearVehiculo()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego vehiculo");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.VehiculoDao.crearVehiculo()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    
    public boolean inactivarVehiculo(String placa){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.VehiculoDao.inactivarVehiculo()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();
            String sql = "UPDATE vehiculo SET estado = 'INACTIVO' WHERE placa = '" + placa + "'";
            System.out.println("com.tl.dao.VehiculoDao.inactivarVehiculo()sql -> " + sql);
            int rs = estatuto.executeUpdate(sql);
            if(rs > 0){
                valida = true;
            }

        } catch (SQLException e) {
            System.out.println("com.tl.dao.VehiculoDao.inactivarVehiculo()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Vehiculo> listarVehiculos() {
        System.out.println("com.tl.dao.VehiculoDao.listarVehiculos()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Vehiculo> listaVehiculos = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM vehiculo");

            while (rs.next()) {
                Vehiculo vehiculo = new Vehiculo();
                vehiculo.setIdvehiculo(rs.getInt("idvehiculo"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                vehiculo.setTipovehiculo(rs.getString("tipovehiculo"));
                vehiculo.setPlaca(rs.getString("placa"));
                vehiculo.setTipopropietario(rs.getString("tipopropietario"));
                vehiculo.setEstado(rs.getInt("estado"));
                vehiculo.setCapacidad(rs.getInt("capacidad"));
                vehiculo.setCilindraje(rs.getInt("cilindraje"));
                vehiculo.setMarca(rs.getString("marca"));
                vehiculo.setModelo(rs.getString("modelo"));
                vehiculo.setNrochasis(rs.getString("nrochasis"));
                vehiculo.setNromotor(rs.getString("nromotor"));
                vehiculo.setCantidadruedas(rs.getInt("cantidadruedas"));
                vehiculo.setSoat(rs.getString("soat"));
                vehiculo.setRtm(rs.getString("rtm"));
                vehiculo.setFechartm(rs.getDate("fechartm"));
                vehiculo.setFechasoat(rs.getDate("fechasoat"));
                vehiculo.setUsuariolog(rs.getString("usuariolog"));
                vehiculo.setFechalog(rs.getDate("fechalog"));
                listaVehiculos.add(vehiculo);
            }
            rs.close();
            estatuto.close();
            return listaVehiculos;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.VehiculoDao.listarVehiculos()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }    

   public Vehiculo consultarVehiculoPlaca(String placa) {

        System.out.println("com.tl.dao.VehiculoDao.consultarVehiculoPlaca()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM vehiculo WHERE placa = '" + placa + "'";
            System.out.println("com.tl.dao.VehiculoDao.consultarVehiculoPlaca()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Vehiculo vehiculo = new Vehiculo();

            while (rs.next()) {

                vehiculo.setIdvehiculo(rs.getInt("idvehiculo"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                vehiculo.setTipovehiculo(rs.getString("tipovehiculo"));
                vehiculo.setPlaca(rs.getString("placa"));
                vehiculo.setTipopropietario(rs.getString("tipopropietario"));
                vehiculo.setEstado(rs.getInt("estado"));
                vehiculo.setCapacidad(rs.getInt("capacidad"));
                vehiculo.setCilindraje(rs.getInt("estado"));
                vehiculo.setMarca(rs.getString("marca"));
                vehiculo.setModelo(rs.getString("modelo"));
                vehiculo.setNrochasis(rs.getString("nrochasis"));
                vehiculo.setNromotor(rs.getString("nromotor"));
                vehiculo.setCantidadruedas(rs.getInt("cantidadruedas"));
                vehiculo.setSoat(rs.getString("soat"));
                vehiculo.setRtm(rs.getString("rtm"));
                vehiculo.setFechartm(rs.getDate("fechartm"));
                vehiculo.setFechasoat(rs.getDate("fechasoat"));
                vehiculo.setUsuariolog(rs.getString("usuariolog"));
                vehiculo.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return vehiculo;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.VehiculoDao.consultarVehiculoPlaca()error: " + e);
        }
        return null;

    } 
    
}
