/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Solicitud;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class SolicitudDAOImpl {

    private ConnectionBD conn;
    private Statement estatuto;

    public SolicitudDAOImpl() {

        System.out.println("com.ga.daos.SolicitudDAOImpl.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.ga.daos.SolicitudDAOImpl.<init>()Error: " + e);
        }
    }
    //--------------METODOS-------------------    

    public boolean adicionarSolicitud(Solicitud s) {
        System.out.println("com.ga.daos.SolicitudDAOImpl.adicionarSolicitud()Inicio");
        boolean respuesta;
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO Solicitud VALUE (null,"
                    + "'" + s.getFechacargue() + "',"
                    + "'" + s.getFechalog() + "',"
                    + "'" + s.getFechasolicitud() + "',"
                    + "'" + s.getFpu() + "',"
                    + "'" + s.getIdlistaserrviciosadicionales() + "',"
                    + "'" + s.getImpoexpo() + "',"
                    + "'" + s.getNroreserva() + "',"
                    + "'" + s.getObservaciones() + "',"
                    + "'" + s.getServiciologisticaadicional() + "',"
                    + "'" + s.getTarifa() + "',"
                    + "'" + s.getTiemporeaccionsolicitud() + "',"
                    + "" + s.getUsuariolog() + ","
                    + "'" + s.getValormercancia() + "',"
                    + "'" + s.getIdCarga() + "',"
                    + "'" + s.getOrigenCarga() + "',"
                    + "'" + s.getDestinoCarga() + "',"
                    + "'" + s.getSolicitadopor() + "',"
                    + "'" + s.getContactoorigen() + "',"
                    + "'" + s.getContactodestino() + "',"
                    + "'" + s.getTipoServicio() + "')";

            System.out.println("com.ga.daos.SolicitudDAOImpl.adicionarSolicitud()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego facturas");
            estatuto.close();
            respuesta = true;
        } catch (SQLException e) {
            System.out.println("com.ga.daos.SolicitudDAOImpl.adicionarSolicitud()error: " + e);
            respuesta = false;
        }
        return respuesta;
    }

    public List<Solicitud> listaSolicitudes() {

        System.out.println("com.ga.daos.SolicitudDAOImpl.listaSolicitudes()Inicio");
        try {
            ArrayList<Solicitud> listaSolicitud = new ArrayList<>();

            estatuto = conn.getConnection().createStatement();
            String sql = "SELECT * FROM Solicitud";
            System.out.println("com.ga.daos.SolicitudDAOImpl.listaSolicitudes()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            while (rs.next()) {
                Solicitud solicitud = new Solicitud();
                solicitud.setIdsolicitud(rs.getInt("idsolicitud"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                solicitud.setFechalog(rs.getDate("fechalog"));
                solicitud.setFechasolicitud(rs.getDate("fechasolicitud"));
                solicitud.setFechacargue(rs.getDate("fechacargue"));
                solicitud.setFpu(rs.getString("fpu"));
                solicitud.setIdlistaserrviciosadicionales(rs.getInt("idlistaserrviciosadicionales"));
                solicitud.setImpoexpo(rs.getString("impoexpo"));
                solicitud.setNroreserva(rs.getInt("nroreserva"));
                solicitud.setObservaciones(rs.getString("observaciones"));
                solicitud.setServiciologisticaadicional(rs.getInt("serviciologisticaadicional"));
                solicitud.setTarifa(rs.getInt("tarifa"));
                solicitud.setTiemporeaccionsolicitud(rs.getInt("tiemporeaccionsolicitud"));
                solicitud.setUsuariolog(rs.getString("usuariolog"));
                solicitud.setValormercancia(rs.getInt("valormercancia"));
                solicitud.setIdCarga(rs.getInt("idcarga"));
                solicitud.setContactoorigen(rs.getInt("contactoorigen"));
                solicitud.setContactodestino(rs.getInt("contactodestino"));
                solicitud.setSolicitadopor(rs.getInt("solicitadopor"));
                solicitud.setOrigenCarga(rs.getInt("origencarga"));
                solicitud.setDestinoCarga(rs.getInt("destinocarga"));
                solicitud.setTipoServicio(rs.getInt("tiposervicio"));

                listaSolicitud.add(solicitud);
            }
            rs.close();
            estatuto.close();
            return listaSolicitud;
        } catch (SQLException e) {
            System.out.println("com.ga.daos.SolicitudDAOImpl.listarFactura()error: " + e);
        }
        return null;
    }

    public Solicitud consultarSolicitudPorId(int id) {

        System.out.println("com.ga.daos.SolicitudDAOImpl.consultarSolicitudPorId()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM solicitud WHERE idsolicitud = '" + id + "'";
            System.out.println("com.ga.daos.PropietariosDAO.buscarPropietario()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Solicitud solicitud = new Solicitud();

            while (rs.next()) {

                solicitud.setIdsolicitud(rs.getInt("idsolicitud"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                solicitud.setFechalog(rs.getDate("fechalog"));
                solicitud.setFechasolicitud(rs.getDate("fechasolicitud"));
                solicitud.setFechacargue(rs.getDate("fechacargue"));
                solicitud.setFpu(rs.getString("fpu"));
                solicitud.setIdlistaserrviciosadicionales(rs.getInt("idlistaserrviciosadicionales"));
                solicitud.setImpoexpo(rs.getString("impoexpo"));
                solicitud.setNroreserva(rs.getInt("nroreserva"));
                solicitud.setObservaciones(rs.getString("observaciones"));
                solicitud.setServiciologisticaadicional(rs.getInt("serviciologisticaadicional"));
                solicitud.setTarifa(rs.getInt("tarifa"));
                solicitud.setTiemporeaccionsolicitud(rs.getInt("tiemporeaccionsolicitud"));
                solicitud.setUsuariolog(rs.getString("usuariolog"));
                solicitud.setValormercancia(rs.getInt("valormercancia"));
                solicitud.setIdCarga(rs.getInt("idcarga"));
                solicitud.setContactoorigen(rs.getInt("contactoorigen"));
                solicitud.setContactodestino(rs.getInt("contactodestino"));
                solicitud.setSolicitadopor(rs.getInt("solicitadopor"));
                solicitud.setOrigenCarga(rs.getInt("origencarga"));
                solicitud.setDestinoCarga(rs.getInt("destinocarga"));
                solicitud.setTipoServicio(rs.getInt("tiposervicio"));
            }
            rs.close();
            estatuto.close();
            return solicitud;
        } catch (SQLException e) {
            System.out.println("com.ga.daos.SolicitudDAOImpl.consultarSolicitudPorId()error: " + e);
        }
        return null;

    }

    public void inactivarSolicitud(int id) {

    }

}
