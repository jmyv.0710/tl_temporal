/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Estudioseguridad;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class EstudioSeguridadDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public EstudioSeguridadDao() {
    System.out.println("com.tl.dao.EstudioSeguridadDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EstudioSeguridadDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearEstudioSeg(Estudioseguridad estudioSeg){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.EstudioSeguridadDao.crearEstudioSeg()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO estudioseguridad VALUE (null,"
                    + "'" + estudioSeg.getTipoestudioseguridad()+ "',"
                    + "'" + estudioSeg.getObservacion()+ "',"
                    + "'" + estudioSeg.getFechavencimiento()+ "',"
                    + "'" + estudioSeg.getTipodocumento()+ "',"
                    + "'" + estudioSeg.getRutadocumento()+ "',"
                    + "'" + estudioSeg.getEstado()+ "',"
                    + "'" + estudioSeg.getIdcliente()+ "',"
                    + "'" + estudioSeg.getIdconductor()+ "',"
                    + "'" + estudioSeg.getIdvehiculo()+ "',"
                    + "'" + estudioSeg.getUsuariolog() + "',"
                    + "'" + estudioSeg.getFechalog() + "')";

            System.out.println("com.tl.dao.EstudioSeguridadDao.crearEstudioSeg()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego estudio seguridad");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.EstudioSeguridadDao.crearEstudioSeg()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Estudioseguridad> listarEstudioSeg() {
        System.out.println("com.tl.dao.EstudioSeguridadDao.listarEstudioSeg()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Estudioseguridad> listaEstudioSeg = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM estudioseguridad");

            while (rs.next()) {
                Estudioseguridad estudioSeg = new Estudioseguridad();
                estudioSeg.setIdestudioseguridad(rs.getInt("idestudioseguridad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                estudioSeg.setTipoestudioseguridad(rs.getString("tipoestudioseguridad"));
                estudioSeg.setObservacion(rs.getString("observacion"));
                estudioSeg.setFechavencimiento(rs.getDate("fechavencimiento"));
                estudioSeg.setTipodocumento(rs.getString("tipodocumento"));
                estudioSeg.setRutadocumento(rs.getString("rutadocumento"));
                estudioSeg.setEstado(rs.getInt("estado"));
                estudioSeg.setIdcliente(rs.getInt("idcliente"));
                estudioSeg.setIdconductor(rs.getInt("idconductor"));
                estudioSeg.setIdvehiculo(rs.getInt("idvehiculo"));
                estudioSeg.setUsuariolog(rs.getString("usuariolog"));
                estudioSeg.setFechalog(rs.getDate("fechalog"));
                listaEstudioSeg.add(estudioSeg);
            }
            rs.close();
            estatuto.close();
            return listaEstudioSeg;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EstudioSeguridadDao.listarEstudioSeg()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
}
