/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Correosnotificacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class CorreosNotificacionDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public CorreosNotificacionDao() {
    System.out.println("com.tl.dao.CorreosNotificacionDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.CorreosNotificacionDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearCorreosNotificacion(Correosnotificacion correos){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.CorreosNotificacionDao.crearCorreosNotificacion()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO correosnotificacion VALUE (null,"
                    + "'" + correos.getIdcorreonotificacion() + "',"
                    + "'" + correos.getFechalog() + "',"
                    + "'" + correos.getNombrepersona() + "',"
                    + "'" + correos.getCorreo() + "',"
                    + "'" + correos.getIdCliente() + "',"
                    + "'" + correos.getUsuariolog() + "')";

            System.out.println("com.tl.dao.CorreosNotificacionDao.crearCorreosNotificacion()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego correos de notificacion");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.CorreosNotificacionDao.crearCorreosNotificacion()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Correosnotificacion> listarCorreosNotificacion() {
        System.out.println("com.tl.dao.CorreosNotificacionDao.listarCorreosNotificacion()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Correosnotificacion> listaCorreosN = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM correosnotificacion");

            while (rs.next()) {
                Correosnotificacion correosN = new Correosnotificacion();
                correosN.setIdcorreonotificacion(rs.getInt("idcorreonotificacion"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                correosN.setNombrepersona(rs.getString("nombrepersona"));
                correosN.setCorreo(rs.getString("correo"));
                correosN.setIdCliente(rs.getInt("idcliente"));
                correosN.setUsuariolog(rs.getString("usuariolog"));
                correosN.setFechalog(rs.getDate("fechalog"));
                listaCorreosN.add(correosN);
            }
            rs.close();
            estatuto.close();
            return listaCorreosN;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.CorreosNotificacionDao.listarCorreosNotificacion()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Correosnotificacion consultarCorreosNotificacion(int idCliente) {

        System.out.println("com.tl.dao.CorreosNotificacionDao.consultarCorreosNotificacion()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM correosnotificacion WHERE idcliente = '" + idCliente + "'";
            System.out.println("com.tl.dao.CorreosNotificacionDao.consultarCorreosNotificacion()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Correosnotificacion correosN = new Correosnotificacion();

            while (rs.next()) {

                correosN.setIdcorreonotificacion(rs.getInt("idcorreonotificacion"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                correosN.setNombrepersona(rs.getString("nombrepersona"));
                correosN.setCorreo(rs.getString("correo"));
                correosN.setIdCliente(rs.getInt("idcliente"));
                correosN.setUsuariolog(rs.getString("usuariolog"));
                correosN.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return correosN;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.CorreosNotificacionDao.consultarCorreosNotificacion()error: " + e);
        }
        return null;

    }
    
}
