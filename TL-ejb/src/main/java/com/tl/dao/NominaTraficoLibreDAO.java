/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Nominatraficolibre;
import com.tl.utilidades.ConstantesTL;
import com.tl.utilidades.UtilFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class NominaTraficoLibreDAO {

    private ConnectionBD conn;
    private Statement estatuto;

    public NominaTraficoLibreDAO() {

        System.out.println("com.ga.daos.NominaTraficoLibreDAO.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.ga.daos.NominaTraficoLibreDAO.<init>()Error: " + e);
        }
    }
    //--------------METODOS-------------------    

    public boolean adicionarNomina(Nominatraficolibre ntl) {
        System.out.println("com.ga.daos.NominaTraficoLibreDAO.adicionarNomina()Inicio");
        boolean respuesta;
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO nominatraficolibre VALUE (null,"
                    + "'" + ntl.getTipodocumento() + "',"
                    + "'" + ntl.getNrodocumento() + "',"
                    + "'" + ntl.getNombre() + "',"
                    + "'" + ntl.getDireccion() + "',"
                    + "'" + ntl.getTelefono() + "',"
                    + "'" + ntl.getCorreo() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoConHoraString(ntl.getFechanacimiento()) + "',"
                    + "'" + ntl.getUsuariolog() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoConHoraString(ntl.getFechalog()) + "',"
                    + "'" + ntl.getContrasenia() + "',"
                    + "'" + ntl.getIdRol() + "')";

            System.out.println("com.ga.daos.NominaTraficoLibreDAO.adicionarNomina()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego usuario");
            estatuto.close();
            respuesta = true;
        } catch (SQLException e) {
            System.out.println("com.ga.daos.NominaTraficoLibreDAO.adicionarNomina()error: " + e);
            respuesta = false;
        }
        return respuesta;
    }

    public List<Nominatraficolibre> listaNominaTrafLibre() {

        System.out.println("com.ga.daos.NominaTraficoLibreDAO.listaNominaTrafLibre()Inicio");
        try {
            ArrayList<Nominatraficolibre> listaNomina = new ArrayList<>();

            estatuto = conn.getConnection().createStatement();
            String sql = "SELECT * FROM nominatraficolibre";
            System.out.println("com.ga.daos.NominaTraficoLibreDAO.listaSolicitudes()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            while (rs.next()) {
                Nominatraficolibre nomtraflibre = new Nominatraficolibre();
                nomtraflibre.setIdnomina(rs.getInt("idnomina"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                nomtraflibre.setTipodocumento(rs.getString("tipodocumento"));
                nomtraflibre.setNrodocumento(rs.getInt("nrodocumento"));
                nomtraflibre.setNombre(rs.getString("nombre"));
                nomtraflibre.setDireccion(rs.getString("direccion"));
                nomtraflibre.setTelefono(rs.getString("telefono"));
                nomtraflibre.setCorreo(rs.getString("correo"));
                nomtraflibre.setFechanacimiento(rs.getDate("fechanacimiento"));
                nomtraflibre.setUsuariolog(rs.getString("usuariolog"));
                nomtraflibre.setFechalog(rs.getDate("fechalog"));
                nomtraflibre.setContrasenia(rs.getString("contrasenia"));
                nomtraflibre.setIdRol(rs.getInt("idrol"));

                listaNomina.add(nomtraflibre);
            }
            rs.close();
            estatuto.close();
            return listaNomina;
        } catch (SQLException e) {
            System.out.println("com.ga.daos.NominaTraficoLibreDAO.listaNominaTrafLibre()error: " + e);
        }
        return null;
    }

    public Nominatraficolibre consultarNominaPorDoc(int numeroDocumento) {

        System.out.println("com.ga.daos.NominaTraficoLibreDAO.consultarNominaPorDoc()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM nominatraficolibre WHERE nrodocumento = '" + numeroDocumento + "'";
            System.out.println("com.ga.daos.NominaTraficoLibreDAO.consultarNominaPorDoc()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Nominatraficolibre nomtraflibre = new Nominatraficolibre();

            while (rs.next()) {

                nomtraflibre.setIdnomina(rs.getInt("idnomina"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                nomtraflibre.setTipodocumento(rs.getString("tipodocumento"));
                nomtraflibre.setNrodocumento(rs.getInt("nrodocumento"));
                nomtraflibre.setNombre(rs.getString("nombre"));
                nomtraflibre.setDireccion(rs.getString("direccion"));
                nomtraflibre.setTelefono(rs.getString("telefono"));
                nomtraflibre.setCorreo(rs.getString("correo"));
                nomtraflibre.setFechanacimiento(rs.getDate("fechanacimiento"));
                nomtraflibre.setUsuariolog(rs.getString("usuariolog"));
                nomtraflibre.setFechalog(rs.getDate("fechalog"));
                nomtraflibre.setContrasenia(rs.getString("contrasenia"));
                nomtraflibre.setIdRol(rs.getInt("idrol"));
                
            }
            rs.close();
            estatuto.close();
            return nomtraflibre;
        } catch (SQLException e) {
            System.out.println("com.ga.daos.NominaTraficoLibreDAO.consultarNominaPorDoc()error: " + e);
        }
        return null;

    }

}
