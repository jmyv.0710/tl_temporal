/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Tiposervicio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class TipoServicioDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public TipoServicioDao() {
    System.out.println("com.tl.dao.TipoServicioDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TipoServicioDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearTipoServicio(Tiposervicio tipoServicio){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.TipoServicioDao.crearTipoServicio()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO tiposervicio VALUE (null,"
                    + "'" + tipoServicio.getIdtiposervicio()+ "',"
                    + "'" + tipoServicio.getNombretiposervicio()+ "',"
                    + "'" + tipoServicio.getHoraservicio()+ "',"
                    + "'" + tipoServicio.getFechalog() + "',"
                    + "'" + tipoServicio.getUsuariolog() + "')";

            System.out.println("com.tl.dao.TipoServicioDao.crearTipoServicio()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego tipo servicio");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.TipoServicioDao.crearTipoServicio()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Tiposervicio> listarTipoServicio() {
        System.out.println("com.tl.dao.TipoServicioDao.listarTipoServicio()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Tiposervicio> listaTipoServicio = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM tiposervicio");

            while (rs.next()) {
                Tiposervicio tipoServicio = new Tiposervicio();
                tipoServicio.setIdtiposervicio(rs.getInt("idtiposervicio"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                tipoServicio.setNombretiposervicio(rs.getString("nombretiposervicio"));
                tipoServicio.setHoraservicio(rs.getDate("horaservicio"));
                tipoServicio.setUsuariolog(rs.getString("usuariolog"));
                tipoServicio.setFechalog(rs.getDate("fechalog"));
                listaTipoServicio.add(tipoServicio);
            }
            rs.close();
            estatuto.close();
            return listaTipoServicio;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TipoServicioDao.listarTipoServicio()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
       
}
