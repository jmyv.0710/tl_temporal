/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Cliente;
import com.tl.utilidades.UtilFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class ClienteDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public ClienteDao() {
    System.out.println("com.tl.dao.ClienteDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ClienteDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearCliente(Cliente cliente){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.ClienteDao.crearCliente()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO cliente VALUE (null,"
                    + "'" + cliente.getNombrecliente() + "',"
                    + "'" + cliente.getTipopersona()+ "',"
                    + "'" + cliente.getNroidentificacion() + "',"
                    + "'" + cliente.getEstado() + "',"
                    + "'" + cliente.getUsuariolog() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoConHoraString(cliente.getFechalog()) + "',"    
                    + "'" + cliente.getIdClienteTercero() + "',"
                    + "'" + cliente.getObservacionesespeciales() + "',"
                    + "'" + cliente.getTipocliente() + "')";

            System.out.println("com.tl.dao.ClienteDao.crearCliente()sql -> " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego cliente");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.ClienteDao.crearCliente()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Cliente> listarClientes() {
        System.out.println("com.tl.dao.ClienteDao.listarClientes()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Cliente> listaClientes = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM cliente");

            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setIdcliente(rs.getInt("idcliente"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                cliente.setNombrecliente(rs.getString("nombrecliente"));
                cliente.setTipopersona(rs.getString("tipopersona"));
                cliente.setNroidentificacion(rs.getString("nroidentificacion"));
                cliente.setObservacionesespeciales(rs.getString("observacionesespeciales"));
                cliente.setTipocliente(rs.getString("tipocliente"));
                cliente.setIdClienteTercero(rs.getInt("idclientetercero"));
                cliente.setEstado(rs.getInt("estado"));
                cliente.setUsuariolog(rs.getString("usuariolog"));
                cliente.setFechalog(rs.getDate("fechalog"));
                listaClientes.add(cliente);
            }
            rs.close();
            estatuto.close();
            return listaClientes;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ClienteDao.listarClientes()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Cliente consultarClienteDocto(String nrodocumentod) {

        System.out.println("com.tl.dao.ClienteDao.consultarClienteDocto()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM cliente WHERE nroidentificacion = '" + nrodocumentod + "'";
            System.out.println("com.tl.dao.ClienteDao.consultarClienteDocto()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Cliente cliente = new Cliente();

            while (rs.next()) {

                cliente.setIdcliente(rs.getInt("idcliente"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                cliente.setNombrecliente(rs.getString("nombrecliente"));
                cliente.setTipopersona(rs.getString("tipopersona"));
                cliente.setNroidentificacion(rs.getString("nroidentificacion"));
                cliente.setObservacionesespeciales(rs.getString("observacionesespeciales"));
                cliente.setTipocliente(rs.getString("tipocliente"));
                cliente.setIdClienteTercero(rs.getInt("idclientetercero"));
                cliente.setEstado(rs.getInt("estado"));
                cliente.setUsuariolog(rs.getString("usuariolog"));
                cliente.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return cliente;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ClienteDao.consultarClienteDocto()error: " + e);
        }
        return null;

    }
    

    
}
