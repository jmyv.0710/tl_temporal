/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Carga;
import com.tl.entidades.Cliente;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class CargaDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public CargaDao() {
    System.out.println("com.tl.dao.CargaDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.CargaDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearCarga(Carga carga){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.CargaDao.crearCarga()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO carga VALUE (null,"
                    + "'" + carga.getIdcarga() + "',"
                    + "'" + carga.getFechalog() + "',"
                    + "'" + carga.getPeso() + "',"
                    + "'" + carga.getVolumen() + "',"
                    + "'" + carga.getAltura() + "',"
                    + "'" + carga.getAncho() + "',"
                    + "'" + carga.getLargo() + "',"
                    + "'" + carga.getValorcarga() + "',"
                    + "'" + carga.getTipocarga() + "',"
                    + "'" + carga.getResponsablepoliza() + "',"
                    + "'" + carga.getUsuariolog() + "')";

            System.out.println("com.tl.dao.CargaDao.crearCarga()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego carga");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.CargaDao.crearCarga()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Carga> listarCargas() {
        System.out.println("com.tl.dao.CargaDao.listarCargas()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Carga> listaCargas = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM carga");

            while (rs.next()) {
                Carga carga = new Carga();
                carga.setIdcarga(rs.getInt("idcarga"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                carga.setPeso(rs.getInt("peso"));
                carga.setVolumen(rs.getInt("volumen"));
                carga.setAltura(rs.getInt("altura"));
                carga.setAncho(rs.getInt("ancho"));
                carga.setLargo(rs.getInt("largo"));
                carga.setValorcarga(rs.getInt("valorcarga"));
                carga.setTipocarga(rs.getString("tipocarga"));
                carga.setResponsablepoliza(rs.getString("responsablepoliza"));
                carga.setUsuariolog(rs.getString("usuariolog"));
                carga.setFechalog(rs.getDate("fechalog"));
                listaCargas.add(carga);
            }
            rs.close();
            estatuto.close();
            return listaCargas;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.CargaDao.listarCargas()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
     
    
}
