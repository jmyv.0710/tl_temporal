/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Departamento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class DepartamentoDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public DepartamentoDao() {
    System.out.println("com.tl.dao.DepartamentoDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DepartamentoDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public ArrayList<Departamento> listarDepartamentos() {
        System.out.println("com.tl.dao.DepartamentoDao.listarDepartamentos()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Departamento> listaDepartamento = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM Departamento");

            while (rs.next()) {
                Departamento departamento = new Departamento();
                departamento.setIddepartamento(rs.getInt("iddepartamento"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                departamento.setAbreviatura(rs.getString("abreviatura"));
                departamento.setNombre(rs.getString("nombre"));
                listaDepartamento.add(departamento);
            }
            rs.close();
            estatuto.close();
            return listaDepartamento;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.DepartamentoDao.listarDepartamentos()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }

    
}
