/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Ruta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class RutaDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public RutaDao() {
    System.out.println("com.tl.dao.RutaDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.RutaDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearRuta(Ruta ruta){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.RutaDao.crearRuta()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO ruta VALUE (null,"
                    + "'" + ruta.getNombreruta()+ "',"
                    + "'" + ruta.getRutainicio()+ "',"
                    + "'" + ruta.getRutafin()+ "',"
                    + "'" + ruta.getDescripcion()+ "',"
                    + "'" + ruta.getTiporuta()+ "',"
                    + "'" + ruta.getTiempoestimadoruta()+ "',"
                    + "'" + ruta.getUsuariolog() + "',"
                    + "'" + ruta.getFechalog() + "')";

            System.out.println("com.tl.dao.RutaDao.crearRuta()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego ruta");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.RutaDao.crearRuta()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Ruta> listarRutas() {
        System.out.println("com.tl.dao.RutaDao.listarRutas()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Ruta> listaEstado = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM ruta");

            while (rs.next()) {
                Ruta ruta = new Ruta();
                ruta.setIdruta(rs.getInt("idruta"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                ruta.setNombreruta(rs.getString("nombreruta"));
                ruta.setRutainicio(rs.getString("rutainicio"));
                ruta.setRutafin(rs.getString("rutafin"));
                ruta.setDescripcion(rs.getString("descripcion"));
                ruta.setTiporuta(rs.getString("tiporuta"));
                ruta.setTiempoestimadoruta(rs.getDate("tiempoestimadoruta"));
                ruta.setUsuariolog(rs.getString("usuariolog"));
                ruta.setFechalog(rs.getDate("fechalog"));
                listaEstado.add(ruta);
            }
            rs.close();
            estatuto.close();
            return listaEstado;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.RutaDao.listarRutas()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
}
