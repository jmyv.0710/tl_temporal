/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Precinto;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class PrecintoDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public PrecintoDao() {
    System.out.println("com.tl.dao.PrecintoDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.PrecintoDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearPrecinto(Precinto precinto){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.PrecintoDao.crearPrecinto()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO precinto VALUE (null,"
                    + "'" + precinto.getNroprecinto()+ "',"
                    + "'" + precinto.getFecharadicado()+ "',"
                    + "'" + precinto.getConductorAsociado()+ "',"
                    + "'" + precinto.getFechauso()+ "',"
                    + "'" + precinto.getEstado()+ "',"
                    + "'" + precinto.getTipoprecinto()+ "',"
                    + "'" + precinto.getFechaentregaconductor()+ "',"
                    + "'" + precinto.getObservaciones()+ "',"
                    + "'" + precinto.getUsuariolog() + "',"
                    + "'" + precinto.getFechalog() + "')";

            System.out.println("com.tl.dao.PrecintoDao.crearPrecinto()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego precinto");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.PrecintoDao.crearPrecinto()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Precinto> listarPrecintos() {
        System.out.println("com.tl.dao.PrecintoDao.listarPrecintos()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Precinto> listaPrecinto = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM precinto");

            while (rs.next()) {
                Precinto precinto = new Precinto();
                precinto.setIdprecinto(rs.getInt("idprecinto"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                precinto.setNroprecinto(rs.getInt("nroprecinto"));
                precinto.setFecharadicado(rs.getDate("fecharadicado"));
                precinto.setConductorAsociado(rs.getInt("conductorasociado"));
                precinto.setFechauso(rs.getDate("fechauso"));
                precinto.setEstado(rs.getInt("estado"));
                precinto.setTipoprecinto(rs.getString("tipoprecinto"));
                precinto.setFechaentregaconductor(rs.getDate("fechaentregaconductor"));
                precinto.setObservaciones(rs.getString("observaciones"));
                precinto.setUsuariolog(rs.getString("usuariolog"));
                precinto.setFechalog(rs.getDate("fechalog"));
                listaPrecinto.add(precinto);
            }
            rs.close();
            estatuto.close();
            return listaPrecinto;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.PrecintoDao.listarPrecintos()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
}
