/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.ServiciosAdicionales;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class ServiciosAdicionalesDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public ServiciosAdicionalesDao() {
    System.out.println("com.tl.dao.ServiciosAdicionalesDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ServiciosAdicionalesDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearServiciosAdicionales(ServiciosAdicionales serviciosAd){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.ServiciosAdicionalesDao.crearServiciosAdicionales()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO serviciosadicionales VALUE (null,"
                    + "'" + serviciosAd.getIdservicio() + "',"
                    + "'" + serviciosAd.getFechalog() + "',"
                    + "'" + serviciosAd.getNombreservicio() + "',"
                    + "'" + serviciosAd.getDescripcionservicio() + "',"
                    + "'" + serviciosAd.getMontacarga() + "',"
                    + "'" + serviciosAd.getDispositivo() + "',"
                    + "'" + serviciosAd.getAuxiliar() + "',"
                    + "'" + serviciosAd.getCantidadauxiliares() + "',"
                    + "'" + serviciosAd.getIdauxiliar() + "',"
                    + "'" + serviciosAd.getIdEsquemaSeguridad() + "',"
                    + "'" + serviciosAd.getRequiereseguridad() + "',"
                    + "'" + serviciosAd.getUsuariolog() + "')";

            System.out.println("com.tl.dao.ServiciosAdicionalesDao.crearServiciosAdicionales()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego servicios adicionales");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.ServiciosAdicionalesDao.crearServiciosAdicionales()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<ServiciosAdicionales> listarServiciosAdicionales() {
        System.out.println("com.tl.dao.ServiciosAdicionalesDao.listarServiciosAdicionales()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<ServiciosAdicionales> listaServiciosAd = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM serviciosadicionales");

            while (rs.next()) {
                ServiciosAdicionales serviciosAd = new ServiciosAdicionales();
                serviciosAd.setIdservicio(rs.getInt("idservicio"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                serviciosAd.setNombreservicio(rs.getString("nombreservicio"));
                serviciosAd.setDescripcionservicio(rs.getString("descripcionservicio"));
                serviciosAd.setMontacarga(rs.getString("montacarga"));
                serviciosAd.setDispositivo(rs.getString("dispositivo"));
                serviciosAd.setAuxiliar(rs.getString("auxiliar"));
                serviciosAd.setCantidadauxiliares(rs.getInt("cantidadauxiliares"));
                serviciosAd.setIdauxiliar(rs.getInt("idauxiliar"));
                serviciosAd.setIdEsquemaSeguridad(rs.getInt("idesquemaseguridad"));
                serviciosAd.setRequiereseguridad(rs.getString("requiereseguridad"));
                serviciosAd.setUsuariolog(rs.getString("usuariolog"));
                serviciosAd.setFechalog(rs.getDate("fechalog"));
                listaServiciosAd.add(serviciosAd);
            }
            rs.close();
            estatuto.close();
            return listaServiciosAd;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ServiciosAdicionalesDao.listarServiciosAdicionales()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public ServiciosAdicionales consultarServicioAdicional(int idServicioAd) {

        System.out.println("com.tl.dao.ServiciosAdicionalesDao.consultarServicioAdicional()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM serviciosadicionales WHERE idservicio = '" + idServicioAd + "'";
            System.out.println("com.tl.dao.ServiciosAdicionalesDao.consultarServicioAdicional()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            ServiciosAdicionales serviciosAd = new ServiciosAdicionales();

            while (rs.next()) {

                serviciosAd.setIdservicio(rs.getInt("idservicio"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                serviciosAd.setNombreservicio(rs.getString("nombreservicio"));
                serviciosAd.setDescripcionservicio(rs.getString("descripcionservicio"));
                serviciosAd.setMontacarga(rs.getString("montacarga"));
                serviciosAd.setDispositivo(rs.getString("dispositivo"));
                serviciosAd.setAuxiliar(rs.getString("auxiliar"));
                serviciosAd.setCantidadauxiliares(rs.getInt("cantidadauxiliares"));
                serviciosAd.setIdauxiliar(rs.getInt("idauxiliar"));
                serviciosAd.setIdEsquemaSeguridad(rs.getInt("idesquemaseguridad"));
                serviciosAd.setRequiereseguridad(rs.getString("requiereseguridad"));
                serviciosAd.setUsuariolog(rs.getString("usuariolog"));
                serviciosAd.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return serviciosAd;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ServiciosAdicionalesDao.consultarServicioAdicional()error: " + e);
        }
        return null;

    }
    
}
