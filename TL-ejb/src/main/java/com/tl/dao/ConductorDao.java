/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Conductor;
import com.tl.entidades.Vehiculo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class ConductorDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public ConductorDao() {
    System.out.println("com.ga.daos.ConductorDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.ga.daos.ConductorDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearConductor(Conductor conductor){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.ConductorDao.crearConductor()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:ss:mm");

            String sql = "INSERT INTO conductor VALUE (null,"
                    + "'" + conductor.getNombreconductor() + "',"
                    + "'" + conductor.getTipodocumento() + "',"
                    + "'" + conductor.getNrodocumento() + "',"
                    + "'" + conductor.getTipoconductor() + "',"
                    + "'" + conductor.getDireccion() + "',"
                    + "'" + conductor.getTelefono() + "',"
                    + "'" + conductor.getCorreo() + "',"
                    + "'" + conductor.getEstado() + "',"
                    + "'" + conductor.getEps() + "',"
                    + "'" + conductor.getFondopension() + "',"
                    + "'" + conductor.getArl() + "',"
                    + "'" + conductor.getUsuariolog() + "',"        
                    + "'" + sdf.format(conductor.getFechalog()) + "'," 
                    + "null,"
                    + "'" + conductor.getIdrol() + "',"
                    + "'" + conductor.getContrasenia() + "')";

            System.out.println("com.tl.dao.ConductorDao.crearConductor()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego conductor");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.ConductorDao.crearConductor()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Conductor> listarConductores() {
        System.out.println("com.tl.dao.ConductorDao.listarConductores()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Conductor> listaConductores = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM conductor");

            while (rs.next()) {
                Conductor conductor = new Conductor();
                conductor.setIdconductor(rs.getInt("idconductor"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                conductor.setNombreconductor(rs.getString("nombreconductor"));
                conductor.setTipodocumento(rs.getString("tipodocumento"));
                conductor.setTipoconductor(rs.getString("tipoconductor"));
                conductor.setNrodocumento(rs.getString("nrodocumento"));
                conductor.setDireccion(rs.getString("direccion"));
                conductor.setTelefono(rs.getString("telefono"));
                conductor.setCorreo(rs.getString("correo"));
                conductor.setEstado(rs.getInt("estado"));
                conductor.setEps(rs.getString("eps"));
                conductor.setFondopension(rs.getString("fondopension"));
                conductor.setArl(rs.getString("arl"));
                conductor.setIdrol(rs.getInt("idrol"));
                conductor.setContrasenia(rs.getString("contrasenia"));
                conductor.setUsuariolog(rs.getString("usuariolog"));
                conductor.setFechalog(rs.getDate("fechalog"));
                listaConductores.add(conductor);
            }
            rs.close();
            estatuto.close();
            return listaConductores;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ConductorDao.listarConductores()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Conductor consultarConductorDocto(String nrodocumentod) {

        System.out.println("com.tl.dao.ConductorDao.consultarConductorDocto()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM conductor WHERE nrodocumento = '" + nrodocumentod + "'";
            System.out.println("com.tl.dao.ConductorDao.consultarConductorDocto()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Conductor conductor = null;

            if (rs.next()) {
                
                conductor = new Conductor();

                conductor.setIdconductor(rs.getInt("idconductor"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                conductor.setNombreconductor(rs.getString("nombreconductor"));
                conductor.setTipodocumento(rs.getString("tipodocumento"));
                conductor.setTipoconductor(rs.getString("tipoconductor"));
                conductor.setNrodocumento(rs.getString("nrodocumento"));
                conductor.setDireccion(rs.getString("direccion"));
                conductor.setTelefono(rs.getString("telefono"));
                conductor.setCorreo(rs.getString("correo"));
                conductor.setEstado(rs.getInt("estado"));
                conductor.setEps(rs.getString("eps"));
                conductor.setFondopension(rs.getString("fondopension"));
                conductor.setArl(rs.getString("arl"));
                conductor.setIdrol(rs.getInt("idrol"));
                conductor.setContrasenia(rs.getString("contrasenia"));
                conductor.setUsuariolog(rs.getString("usuariolog"));
                conductor.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return conductor;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.ConductorDao.consultarConductorDocto()error: " + e);
        }
        return null;

    }
    
    
}
