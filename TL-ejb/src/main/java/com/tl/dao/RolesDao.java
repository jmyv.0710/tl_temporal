/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Rol;
import com.tl.utilidades.UtilFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Naticha
 */
public class RolesDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public RolesDao() {
    System.out.println("com.tl.dao.RolesDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.RolesDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearRol(Rol rol){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.RolesDao.crearRol()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();
            
            

            String sql = "INSERT INTO Rol VALUE (null,"
                    + "'" + rol.getRol() + "',"
                    + "'" + rol.getDescripcion() + "',"
                    + "'" + rol.getEstado() + "',"
                    + "'" + rol.getUsuariolog() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoConHoraString(rol.getFechalog()) + "')";

            System.out.println("com.tl.dao.RolesDao.crearRol()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego roles");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.RolesDao.crearRol()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    
    public boolean inactivarRol(int idRol){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.RolesDao.inactivarRol()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();
            String sql = "UPDATE rol SET estado = 'INACTIVO' WHERE idrol = '" + idRol + "'";
            System.out.println("com.tl.dao.RolesDao.inactivarRol()sql -> " + sql);
            int rs = estatuto.executeUpdate(sql);
            if(rs > 0){
                valida = true;
            }

        } catch (SQLException e) {
            System.out.println("com.tl.dao.RolesDao.inactivarRol()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Rol> listarRoles() {
        System.out.println("com.tl.dao.RolesDao.listarRoles()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Rol> listaRoles = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM Rol");

            while (rs.next()) {
                Rol rol = new Rol();
                rol.setIdrol(rs.getInt("idrol"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                rol.setRol(rs.getString("rol"));
                rol.setDescripcion(rs.getString("descripcion"));
                rol.setEstado(rs.getString("estado"));
                rol.setUsuariolog(rs.getString("usuariolog"));
                rol.setFechalog(rs.getDate("fechalog"));
                listaRoles.add(rol);
            }
            rs.close();
            estatuto.close();
            return listaRoles;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.RolesDao.listarRoles()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }    
    
    
}
