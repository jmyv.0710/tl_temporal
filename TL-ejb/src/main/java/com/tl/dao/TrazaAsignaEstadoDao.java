/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Trazabilidadasignacionestado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class TrazaAsignaEstadoDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public TrazaAsignaEstadoDao() {
    System.out.println("com.tl.dao.TrazaAsignaEstadoDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearTrazaAsignaEstado(Trazabilidadasignacionestado trazaAsignaEstado){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.TrazaAsignaEstadoDao.crearTrazaAsignaEstado()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO trazabilidadasignacionestado VALUE (null,"
                    + "'" + trazaAsignaEstado.getIdtrazabilidad()+ "',"
                    + "'" + trazaAsignaEstado.getIdAsignacion()+ "',"
                    + "'" + trazaAsignaEstado.getEstadoAnterior()+ "',"
                    + "'" + trazaAsignaEstado.getNuevoEstado()+ "',"
                    + "'" + trazaAsignaEstado.getFechacambio()+ "',"
                    + "'" + trazaAsignaEstado.getIdEmpleadoNomina()+ "',"
                    + "'" + trazaAsignaEstado.getKpi()+ "',"
                    + "'" + trazaAsignaEstado.getObservacion()+ "',"
                    + "'" + trazaAsignaEstado.getTipokpi()+ "',"
                    + "'" + trazaAsignaEstado.getFecharegistro()+ "',"
                    + "'" + trazaAsignaEstado.getUsuariolog() + "',"
                    + "'" + trazaAsignaEstado.getFechalog()+ "')";

            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.crearTrazaAsignaEstado()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego trazabilidad Asignacion");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.crearTrazaAsignaEstado()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Trazabilidadasignacionestado> listarTrazaAsignaEstado() {
        System.out.println("com.tl.dao.TrazaAsignaEstadoDao.listarTrazaAsignaEstado()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Trazabilidadasignacionestado> listaTrazaAsignaEstado = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM trazabilidadasignacionestado");

            while (rs.next()) {
                Trazabilidadasignacionestado trazaAsignaEstado = new Trazabilidadasignacionestado();
                trazaAsignaEstado.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaEstado.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaEstado.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaEstado.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaEstado.setFechacambio(rs.getDate("fechacambio"));
                trazaAsignaEstado.setIdEmpleadoNomina(rs.getInt("idempleadonomina"));
                trazaAsignaEstado.setKpi(rs.getString("kpi"));
                trazaAsignaEstado.setObservacion(rs.getString("observacion"));
                trazaAsignaEstado.setTipokpi(rs.getString("tipokpi"));
                trazaAsignaEstado.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaEstado.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaEstado.setFechalog(rs.getDate("fechalog"));
                listaTrazaAsignaEstado.add(trazaAsignaEstado);
            }
            rs.close();
            estatuto.close();
            return listaTrazaAsignaEstado;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.listarTrazaAsignaEstado()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Trazabilidadasignacionestado consultarTrazaPorAsignacion(int idAsignacion) {

        System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorAsignacion()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM trazabilidadasignacionestado WHERE idasignacion = '" + idAsignacion + "'";
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorAsignacion()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Trazabilidadasignacionestado trazaAsignaEstado = new Trazabilidadasignacionestado();

            while (rs.next()) {

                trazaAsignaEstado.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaEstado.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaEstado.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaEstado.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaEstado.setFechacambio(rs.getDate("fechacambio"));
                trazaAsignaEstado.setIdEmpleadoNomina(rs.getInt("idempleadonomina"));
                trazaAsignaEstado.setKpi(rs.getString("kpi"));
                trazaAsignaEstado.setObservacion(rs.getString("observacion"));
                trazaAsignaEstado.setTipokpi(rs.getString("tipokpi"));
                trazaAsignaEstado.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaEstado.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaEstado.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return trazaAsignaEstado;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorAsignacion()error: " + e);
        }
        return null;

    }
    
    public Trazabilidadasignacionestado consultarTrazaPorEstadoAnt(int idEstadoAnt) {

        System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorEstadoAnt()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM trazabilidadasignacionestado WHERE estadoanterior = '" + idEstadoAnt + "'";
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorEstadoAnt()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Trazabilidadasignacionestado trazaAsignaEstado = new Trazabilidadasignacionestado();

            while (rs.next()) {

                trazaAsignaEstado.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaEstado.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaEstado.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaEstado.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaEstado.setFechacambio(rs.getDate("fechacambio"));
                trazaAsignaEstado.setIdEmpleadoNomina(rs.getInt("idempleadonomina"));
                trazaAsignaEstado.setKpi(rs.getString("kpi"));
                trazaAsignaEstado.setObservacion(rs.getString("observacion"));
                trazaAsignaEstado.setTipokpi(rs.getString("tipokpi"));
                trazaAsignaEstado.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaEstado.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaEstado.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return trazaAsignaEstado;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorEstadoAnt()error: " + e);
        }
        return null;

    }
    
    public Trazabilidadasignacionestado consultarTrazaPorEstadoNuev(int idEstadoNuev) {

        System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorEstadoNuev()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM trazabilidadasignacionestado WHERE nuevoestado = '" + idEstadoNuev + "'";
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorEstadoNuev()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Trazabilidadasignacionestado trazaAsignaEstado = new Trazabilidadasignacionestado();

            while (rs.next()) {

                trazaAsignaEstado.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaEstado.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaEstado.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaEstado.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaEstado.setFechacambio(rs.getDate("fechacambio"));
                trazaAsignaEstado.setIdEmpleadoNomina(rs.getInt("idempleadonomina"));
                trazaAsignaEstado.setKpi(rs.getString("kpi"));
                trazaAsignaEstado.setObservacion(rs.getString("observacion"));
                trazaAsignaEstado.setTipokpi(rs.getString("tipokpi"));
                trazaAsignaEstado.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaEstado.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaEstado.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return trazaAsignaEstado;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaEstadoDao.consultarTrazaPorEstadoNuev()error: " + e);
        }
        return null;

    }
    
}
