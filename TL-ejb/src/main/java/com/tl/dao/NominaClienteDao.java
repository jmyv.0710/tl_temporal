/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Cliente;
import com.tl.entidades.Nominacliente;
import com.tl.utilidades.UtilFechas;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class NominaClienteDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public NominaClienteDao() {
    System.out.println("com.tl.dao.NominaClienteDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.NominaClienteDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearNominaCliente(Nominacliente nominaCliente){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.NominaClienteDao.crearNominaCliente()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO nominacliente VALUE (null,"
                    + "'" + nominaCliente.getNombresolicitante() + "',"
                    + "'" + nominaCliente.getIdentifacionsolicitante() + "',"
                    + "'" + nominaCliente.getEstado() + "',"
                    + "'" + nominaCliente.getCargo() + "',"
                    + "'" + nominaCliente.getIdcliente() + "',"
                    + "'" + nominaCliente.getTipodocumento() + "',"
                    + "'" + nominaCliente.getUsuariolog() + "',"
                    + "'" + UtilFechas.obtenerFechaFormatoConHoraString(nominaCliente.getFechalog()) + "',"
                    + "'" + nominaCliente.getContrasenia() + "',"
                    + "'" + nominaCliente.getIdRol() + "')";

            System.out.println("com.tl.dao.NominaClienteDao.crearNominaCliente()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego nomina cliente");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.NominaClienteDao.crearNominaCliente()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Nominacliente> listarNominaClientes(int idCliente) {
        System.out.println("com.tl.dao.NominaClienteDao.listarNominaClientes()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Nominacliente> listaNomClientes = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM nominacliente WHERE idcliente = " + idCliente);

            while (rs.next()) {
                Nominacliente nominacliente = new Nominacliente();
                nominacliente.setIdcliente(rs.getInt("idcliente"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                nominacliente.setNombresolicitante(rs.getString("nombresolicitante"));
                nominacliente.setIdentifacionsolicitante(rs.getString("identifacionsolicitante"));
                nominacliente.setCargo(rs.getString("cargo"));
                nominacliente.setContrasenia(rs.getString("contrasenia"));
                nominacliente.setTipodocumento(rs.getString("tipodocumento"));
                nominacliente.setIdnominacliente(rs.getInt("idnominacliente"));
                nominacliente.setEstado(rs.getInt("estado"));
                nominacliente.setUsuariolog(rs.getString("usuariolog"));
                nominacliente.setFechalog(rs.getDate("fechalog"));
                listaNomClientes.add(nominacliente);
            }
            rs.close();
            estatuto.close();
            return listaNomClientes;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.NominaClienteDao.listarNominaClientes()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Nominacliente consultarNomClienteDocto(String identifacionsolicitante) {

        System.out.println("com.tl.dao.NominaClienteDao.consultarNomClienteDocto()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM nominacliente WHERE identifacionsolicitante = '" + identifacionsolicitante + "'";
            System.out.println("com.tl.dao.NominaClienteDao.consultarNomClienteDocto()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Nominacliente nominacliente = new Nominacliente();

            while (rs.next()) {

                nominacliente.setIdcliente(rs.getInt("idcliente"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                nominacliente.setNombresolicitante(rs.getString("nombresolicitante"));
                nominacliente.setIdentifacionsolicitante(rs.getString("identifacionsolicitante"));
                nominacliente.setCargo(rs.getString("cargo"));
                nominacliente.setContrasenia(rs.getString("contrasenia"));
                nominacliente.setTipodocumento(rs.getString("tipodocumento"));
                nominacliente.setIdnominacliente(rs.getInt("idnominacliente"));
                nominacliente.setEstado(rs.getInt("estado"));
                nominacliente.setUsuariolog(rs.getString("usuariolog"));
                nominacliente.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return nominacliente;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.NominaClienteDao.consultarNomClienteDocto()error: " + e);
        }
        return null;

    }

    
}
