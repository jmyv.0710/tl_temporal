/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Trazabilidadasignacionestado;
import com.tl.entidades.Trazabilidadasignacionruta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class TrazaAsignaRutaDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public TrazaAsignaRutaDao() {
    System.out.println("com.tl.dao.TrazaAsignaRutaDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearTrazaAsignaRuta(Trazabilidadasignacionruta trazaAsignaRuta){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.TrazaAsignaRutaDao.crearTrazaAsignaRuta()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO trazabilidadasignacionruta VALUE (null,"
                    + "'" + trazaAsignaRuta.getIdtrazabilidad()+ "',"
                    + "'" + trazaAsignaRuta.getIdAsignacion()+ "',"
                    + "'" + trazaAsignaRuta.getUbicacionregistro()+ "',"
                    + "'" + trazaAsignaRuta.getObservacionesregistro()+ "',"
                    + "'" + trazaAsignaRuta.getFecharegistro()+ "',"
                    + "'" + trazaAsignaRuta.getNumeroregistro()+ "',"
                    + "'" + trazaAsignaRuta.getEstadoAnterior()+ "',"
                    + "'" + trazaAsignaRuta.getNuevoEstado()+ "',"
                    + "'" + trazaAsignaRuta.getUsuariolog() + "',"
                    + "'" + trazaAsignaRuta.getFechalog()+ "')";

            System.out.println("com.tl.dao.TrazaAsignaRutaDao.crearTrazaAsignaRuta()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego trazabilidad Ruta");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.crearTrazaAsignaRuta()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Trazabilidadasignacionruta> listarTrazaAsignaRuta() {
        System.out.println("com.tl.dao.TrazaAsignaRutaDao.listarTrazaAsignaRuta()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Trazabilidadasignacionruta> listaTrazaAsignaRuta = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM trazabilidadasignacionruta");

            while (rs.next()) {
                Trazabilidadasignacionruta trazaAsignaRuta = new Trazabilidadasignacionruta();
                trazaAsignaRuta.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaRuta.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaRuta.setUbicacionregistro(rs.getString("ubicacionregistro"));
                trazaAsignaRuta.setObservacionesregistro(rs.getString("observacionesregistro"));
                trazaAsignaRuta.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaRuta.setNumeroregistro(rs.getInt("numeroregistro"));
                trazaAsignaRuta.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaRuta.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaRuta.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaRuta.setFechalog(rs.getDate("fechalog"));
                listaTrazaAsignaRuta.add(trazaAsignaRuta);
            }
            rs.close();
            estatuto.close();
            return listaTrazaAsignaRuta;
        } catch (SQLException e) {
            System.out.println("com.tl.dao..TrazaAsignaRutaDao.listarTrazaAsignaRuta()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
    public Trazabilidadasignacionruta consultarTrazaPorAsignacion(int idAsignacion) {

        System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorAsignacion()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM trazabilidadasignacionruta WHERE idasignacion = '" + idAsignacion + "'";
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorAsignacion()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Trazabilidadasignacionruta trazaAsignaRuta = new Trazabilidadasignacionruta();

            while (rs.next()) {

                trazaAsignaRuta.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaRuta.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaRuta.setUbicacionregistro(rs.getString("ubicacionregistro"));
                trazaAsignaRuta.setObservacionesregistro(rs.getString("observacionesregistro"));
                trazaAsignaRuta.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaRuta.setNumeroregistro(rs.getInt("numeroregistro"));
                trazaAsignaRuta.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaRuta.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaRuta.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaRuta.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return trazaAsignaRuta;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorAsignacion()error: " + e);
        }
        return null;

    }
    
    public Trazabilidadasignacionruta consultarTrazaPorRutaAnt(int idEstadoAnt) {

        System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorEstadoAnt()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM trazabilidadasignacionruta WHERE estadoanterior = '" + idEstadoAnt + "'";
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorEstadoAnt()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Trazabilidadasignacionruta trazaAsignaRuta = new Trazabilidadasignacionruta();

            while (rs.next()) {

                trazaAsignaRuta.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaRuta.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaRuta.setUbicacionregistro(rs.getString("ubicacionregistro"));
                trazaAsignaRuta.setObservacionesregistro(rs.getString("observacionesregistro"));
                trazaAsignaRuta.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaRuta.setNumeroregistro(rs.getInt("numeroregistro"));
                trazaAsignaRuta.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaRuta.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaRuta.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaRuta.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return trazaAsignaRuta;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorEstadoAnt()error: " + e);
        }
        return null;

    }
    
    public Trazabilidadasignacionruta consultarTrazaPorRutaNuev(int idEstadoNuev) {

        System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorRutaNuev()inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "SELECT * FROM trazabilidadasignacionruta WHERE nuevoestado = '" + idEstadoNuev + "'";
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorRutaNuev()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            Trazabilidadasignacionruta trazaAsignaRuta = new Trazabilidadasignacionruta();

            while (rs.next()) {

                trazaAsignaRuta.setIdtrazabilidad(rs.getInt("idtrazabilidad"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                trazaAsignaRuta.setIdAsignacion(rs.getInt("idasignacion"));
                trazaAsignaRuta.setUbicacionregistro(rs.getString("ubicacionregistro"));
                trazaAsignaRuta.setObservacionesregistro(rs.getString("observacionesregistro"));
                trazaAsignaRuta.setFecharegistro(rs.getDate("fecharegistro"));
                trazaAsignaRuta.setNumeroregistro(rs.getInt("numeroregistro"));
                trazaAsignaRuta.setEstadoAnterior(rs.getInt("estadoanterior"));
                trazaAsignaRuta.setNuevoEstado(rs.getInt("nuevoestado"));
                trazaAsignaRuta.setUsuariolog(rs.getString("usuariolog"));
                trazaAsignaRuta.setFechalog(rs.getDate("fechalog"));
            }
            rs.close();
            estatuto.close();
            return trazaAsignaRuta;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.TrazaAsignaRutaDao.consultarTrazaPorRutaNuev()error: " + e);
        }
        return null;

    }
    
}
