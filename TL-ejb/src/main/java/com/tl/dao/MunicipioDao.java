/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Municipio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class MunicipioDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public MunicipioDao() {
    System.out.println("com.tl.dao.MunicipioDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.MunicipioDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public ArrayList<Municipio> listarMunicipiosPorIdDepto(int idDepto) {
        System.out.println("com.tl.dao.MunicipioDao.listarMunicipiosPorIdDepto()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Municipio> listaMunicipio = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
                        
            String sql = "SELECT * FROM Municipio WHERE iddepartamento = '" + idDepto + "'";
            System.out.println("com.tl.dao.MunicipioDao.listarMunicipiosPorIdDepto()sql -> " + sql);
            ResultSet rs = estatuto.executeQuery(sql);

            while (rs.next()) {
                Municipio municipio = new Municipio();
                municipio.setIdmunicipio(rs.getInt("idmunicipio"));
                municipio.setIddepartamento(rs.getInt("iddepartamento"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                municipio.setNombre(rs.getString("nombre"));
                listaMunicipio.add(municipio);
            }
            rs.close();
            estatuto.close();
            return listaMunicipio;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.MunicipioDao.listarMunicipiosPorIdDepto()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
}
