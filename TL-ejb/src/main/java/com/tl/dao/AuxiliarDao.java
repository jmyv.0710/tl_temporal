/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Auxiliar;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class AuxiliarDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public AuxiliarDao() {
    System.out.println("com.tl.dao.AuxiliarDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.AuxiliarDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearAuxiliar(Auxiliar auxiliar){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.AuxiliarDao.crearAuxiliar()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO auxiliar VALUE (null,"
                    + "'" + auxiliar.getNombreauxiliar()+ "',"
                    + "'" + auxiliar.getNumeroidentificacion()+ "',"
                    + "'" + auxiliar.getDireccion()+ "',"
                    + "'" + auxiliar.getNumerocontacto()+ "',"
                    + "'" + auxiliar.getTipocontrato()+ "',"
                    + "'" + auxiliar.getIdInventario()+ "',"
                    + "'" + auxiliar.getIdestudioseguridad()+ "',"
                    + "'" + auxiliar.getUsuariolog() + "',"
                    + "'" + auxiliar.getFechalog() + "')";

            System.out.println("com.tl.dao.AuxiliarDao.crearAuxiliar()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego auxiliar");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.AuxiliarDao.crearAuxiliar()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Auxiliar> listarAuxiliares() {
        System.out.println("com.tl.dao.AuxiliarDao.listarAuxiliares()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Auxiliar> listaAuxiliares = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM auxiliar");

            while (rs.next()) {
                Auxiliar auxiliar = new Auxiliar();
                auxiliar.setIdauxiliar(rs.getInt("idauxiliar"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                auxiliar.setNombreauxiliar(rs.getString("nombreauxiliar"));
                auxiliar.setNumeroidentificacion(rs.getInt("numeroidentificacion"));
                auxiliar.setDireccion(rs.getString("direccion"));
                auxiliar.setNumerocontacto(rs.getInt("numerocontacto"));
                auxiliar.setTipocontrato(rs.getString("tipocontrato"));
                auxiliar.setIdInventario(rs.getInt("idinventario"));
                auxiliar.setIdestudioseguridad(rs.getInt("idestudioseguridad"));
                auxiliar.setUsuariolog(rs.getString("usuariolog"));
                auxiliar.setFechalog(rs.getDate("fechalog"));
                listaAuxiliares.add(auxiliar);
            }
            rs.close();
            estatuto.close();
            return listaAuxiliares;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.AuxiliarDao.listarAuxiliares()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    
}
