/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tl.dao;

import com.tl.conexion.ConnectionBD;
import com.tl.entidades.Estado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Naticha
 */
public class EstadosDao {
    
    private ConnectionBD conn;
    private Statement estatuto;

    public EstadosDao() {
    System.out.println("com.tl.dao.EstadosDao.<init>()Inicio");
        try {
            conn = new ConnectionBD();
            estatuto = conn.getConnection().createStatement();
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EstadosDao.<init>()Error: " + e);
        }
    }
    //--------------METODOS------------------- 
    
    public boolean crearEstado(Estado estado){
        
        boolean valida = true;
        
        System.out.println("com.tl.dao.EstadosDao.crearEstado()Inicio");
        try {
            estatuto = conn.getConnection().createStatement();

            String sql = "INSERT INTO estados VALUE (null,"
                    + "'" + estado.getTipoestado()+ "',"
                    + "'" + estado.getNombreestado()+ "',"
                    + "'" + estado.getDescripcion()+ "',"
                    + "'" + estado.getUsuariolog() + "',"
                    + "'" + estado.getFechalog() + "')";

            System.out.println("com.tl.dao.EstadosDao.crearEstado()Sql: " + sql);
            estatuto.execute(sql);
            System.out.println("Se agrego estado");
            valida = true;
            estatuto.close();

        } catch (SQLException e) {
            System.out.println("com.tl.dao.EstadosDao.crearEstado()error: " + e);
            valida = false;
        }
        
        return valida;

    }
    
    public ArrayList<Estado> listarEstados() {
        System.out.println("com.tl.dao.EstadosDao.listarEstados()Inicio");//se usa para marcar un error si se da
        try {
            ConnectionBD conn = new ConnectionBD();
            ArrayList<Estado> listaEstado = new ArrayList<>();

            Statement estatuto = conn.getConnection().createStatement();
            ResultSet rs = estatuto.executeQuery("SELECT * FROM estados");

            while (rs.next()) {
                Estado estado = new Estado();
                estado.setIdestados(rs.getInt("idestados"));//SE PONE EXACTAMENTE COMO APARECE EN LA BASE DE DATOS
                estado.setTipoestado(rs.getString("tipoestado"));
                estado.setNombreestado(rs.getString("nombreestado"));
                estado.setDescripcion(rs.getString("descripcion"));
                estado.setUsuariolog(rs.getString("usuariolog"));
                estado.setFechalog(rs.getDate("fechalog"));
                listaEstado.add(estado);
            }
            rs.close();
            estatuto.close();
            return listaEstado;
        } catch (SQLException e) {
            System.out.println("com.tl.dao.EstadosDao.listarEstados()Error:" + e);//se usa para marcar un error si se da
        }
        return null;
    }
    

    
}
