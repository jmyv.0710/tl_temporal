package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the direccion database table.
 * 
 */
@Entity
@NamedQuery(name="Direccion.findAll", query="SELECT d FROM Direccion d")
public class Direccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int iddireccion;

	private String barrio;

	private String ciudad;

	private String departamento;

	private String direccion;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String localidad;

	private String pais;

	private String usuariolog;

	//bi-directional many-to-one association to Solicitud
	@OneToMany(mappedBy="direccion1")
	private List<Solicitud> solicituds1;

	//bi-directional many-to-one association to Solicitud
	@OneToMany(mappedBy="direccion2")
	private List<Solicitud> solicituds2;

	public Direccion() {
	}

	public int getIddireccion() {
		return this.iddireccion;
	}

	public void setIddireccion(int iddireccion) {
		this.iddireccion = iddireccion;
	}

	public String getBarrio() {
		return this.barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getLocalidad() {
		return this.localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Solicitud> getSolicituds1() {
		return this.solicituds1;
	}

	public void setSolicituds1(List<Solicitud> solicituds1) {
		this.solicituds1 = solicituds1;
	}

	public List<Solicitud> getSolicituds2() {
		return this.solicituds2;
	}

	public void setSolicituds2(List<Solicitud> solicituds2) {
		this.solicituds2 = solicituds2;
	}
	

}