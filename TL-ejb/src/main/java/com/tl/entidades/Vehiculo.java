package com.tl.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the vehiculo database table.
 * 
 */
public class Vehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	private int idvehiculo;
	private int cantidadruedas;
	private int capacidad;
	private int cilindraje;
	private int estado;
	private Date fechalog;
	private Date fechartm;
	private Date fechasoat;
	private Object foto;
	private String marca;
	private String modelo;
	private String nrochasis;
	private String nromotor;
	private String placa;
	private String rtm;
	private String soat;
	private String tipopropietario;
	private String tipovehiculo;
	private String usuariolog;
	private List<Agendavehiculo> agendavehiculos;
	private List<Estudioseguridad> estudioseguridads;

        public Vehiculo() {
	}

	public int getIdvehiculo() {
		return this.idvehiculo;
	}

	public void setIdvehiculo(int idvehiculo) {
		this.idvehiculo = idvehiculo;
	}

	public int getCantidadruedas() {
		return this.cantidadruedas;
	}

	public void setCantidadruedas(int cantidadruedas) {
		this.cantidadruedas = cantidadruedas;
	}

	public int getCapacidad() {
		return this.capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public int getCilindraje() {
		return this.cilindraje;
	}

	public void setCilindraje(int cilindraje) {
		this.cilindraje = cilindraje;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public Date getFechartm() {
		return this.fechartm;
	}

	public void setFechartm(Date fechartm) {
		this.fechartm = fechartm;
	}

	public Date getFechasoat() {
		return this.fechasoat;
	}

	public void setFechasoat(Date fechasoat) {
		this.fechasoat = fechasoat;
	}

	public Object getFoto() {
		return this.foto;
	}

	public void setFoto(Object foto) {
		this.foto = foto;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNrochasis() {
		return this.nrochasis;
	}

	public void setNrochasis(String nrochasis) {
		this.nrochasis = nrochasis;
	}

	public String getNromotor() {
		return this.nromotor;
	}

	public void setNromotor(String nromotor) {
		this.nromotor = nromotor;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRtm() {
		return this.rtm;
	}

	public void setRtm(String rtm) {
		this.rtm = rtm;
	}

	public String getSoat() {
		return this.soat;
	}

	public void setSoat(String soat) {
		this.soat = soat;
	}

	public String getTipopropietario() {
		return this.tipopropietario;
	}

	public void setTipopropietario(String tipopropietario) {
		this.tipopropietario = tipopropietario;
	}

	public String getTipovehiculo() {
		return this.tipovehiculo;
	}

	public void setTipovehiculo(String tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Agendavehiculo> getAgendavehiculos() {
		return this.agendavehiculos;
	}

	public void setAgendavehiculos(List<Agendavehiculo> agendavehiculos) {
		this.agendavehiculos = agendavehiculos;
	}

	public Agendavehiculo addAgendavehiculo(Agendavehiculo agendavehiculo) {
		getAgendavehiculos().add(agendavehiculo);
		agendavehiculo.setVehiculo(this);

		return agendavehiculo;
	}

	public Agendavehiculo removeAgendavehiculo(Agendavehiculo agendavehiculo) {
		getAgendavehiculos().remove(agendavehiculo);
		agendavehiculo.setVehiculo(null);

		return agendavehiculo;
	}

	public List<Estudioseguridad> getEstudioseguridads() {
		return this.estudioseguridads;
	}

	public void setEstudioseguridads(List<Estudioseguridad> estudioseguridads) {
		this.estudioseguridads = estudioseguridads;
	}
}