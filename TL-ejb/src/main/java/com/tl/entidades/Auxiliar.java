package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the auxiliar database table.
 * 
 */
@Entity
@NamedQuery(name="Auxiliar.findAll", query="SELECT a FROM Auxiliar a")
public class Auxiliar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idauxiliar;

	private String direccion;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String nombreauxiliar;

	private int numerocontacto;

	private int numeroidentificacion;

	private String tipocontrato;

	private String usuariolog;

	private int idestudioseguridad;

	private int idInventario;

	public Auxiliar() {
	}

	public int getIdauxiliar() {
		return this.idauxiliar;
	}

	public void setIdauxiliar(int idauxiliar) {
		this.idauxiliar = idauxiliar;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getNombreauxiliar() {
		return this.nombreauxiliar;
	}

	public void setNombreauxiliar(String nombreauxiliar) {
		this.nombreauxiliar = nombreauxiliar;
	}

	public int getNumerocontacto() {
		return this.numerocontacto;
	}

	public void setNumerocontacto(int numerocontacto) {
		this.numerocontacto = numerocontacto;
	}

	public int getNumeroidentificacion() {
		return this.numeroidentificacion;
	}

	public void setNumeroidentificacion(int numeroidentificacion) {
		this.numeroidentificacion = numeroidentificacion;
	}

	public String getTipocontrato() {
		return this.tipocontrato;
	}

	public void setTipocontrato(String tipocontrato) {
		this.tipocontrato = tipocontrato;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

    public int getIdestudioseguridad() {
        return idestudioseguridad;
    }

    public void setIdestudioseguridad(int idestudioseguridad) {
        this.idestudioseguridad = idestudioseguridad;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

	

}