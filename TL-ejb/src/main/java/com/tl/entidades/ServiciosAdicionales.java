package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the serviciosadicionales database table.
 *
 */
@Entity
@Table(name = "serviciosadicionales")
@NamedQuery(name = "Serviciosadicionale.findAll", query = "SELECT s FROM Serviciosadicionale s")
public class ServiciosAdicionales implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idservicio;

    private String auxiliar;

    private int cantidadauxiliares;

    private String descripcionservicio;

    private String dispositivo;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    private int idauxiliar;

    private String montacarga;

    private String nombreservicio;

    private String requiereseguridad;

    private String usuariolog;

    private int idEsquemaSeguridad;

    //bi-directional many-to-one association to Solicitud
    @ManyToOne
    private Solicitud solicitud;

    public ServiciosAdicionales() {
    }

    public int getIdservicio() {
        return this.idservicio;
    }

    public void setIdservicio(int idservicio) {
        this.idservicio = idservicio;
    }

    public String getAuxiliar() {
        return this.auxiliar;
    }

    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    public int getCantidadauxiliares() {
        return this.cantidadauxiliares;
    }

    public void setCantidadauxiliares(int cantidadauxiliares) {
        this.cantidadauxiliares = cantidadauxiliares;
    }

    public String getDescripcionservicio() {
        return this.descripcionservicio;
    }

    public void setDescripcionservicio(String descripcionservicio) {
        this.descripcionservicio = descripcionservicio;
    }

    public String getDispositivo() {
        return this.dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public int getIdauxiliar() {
        return this.idauxiliar;
    }

    public void setIdauxiliar(int idauxiliar) {
        this.idauxiliar = idauxiliar;
    }

    public String getMontacarga() {
        return this.montacarga;
    }

    public void setMontacarga(String montacarga) {
        this.montacarga = montacarga;
    }

    public String getNombreservicio() {
        return this.nombreservicio;
    }

    public void setNombreservicio(String nombreservicio) {
        this.nombreservicio = nombreservicio;
    }

    public String getRequiereseguridad() {
        return this.requiereseguridad;
    }

    public void setRequiereseguridad(String requiereseguridad) {
        this.requiereseguridad = requiereseguridad;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdEsquemaSeguridad() {
        return idEsquemaSeguridad;
    }

    public void setIdEsquemaSeguridad(int idEsquemaSeguridad) {
        this.idEsquemaSeguridad = idEsquemaSeguridad;
    }

    public Solicitud getSolicitud() {
        return this.solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

}
