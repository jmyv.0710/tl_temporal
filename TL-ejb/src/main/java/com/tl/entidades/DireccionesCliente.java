package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the direcciones_cliente database table.
 *
 */
@Entity
@Table(name = "direcciones_cliente")
@NamedQuery(name = "DireccionesCliente.findAll", query = "SELECT d FROM DireccionesCliente d")
public class DireccionesCliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int iddireccionescliente;

    private String direccion;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    private String tipodireccion;

    private String usuariolog;

    private int idCliente;

    public DireccionesCliente() {
    }

    public int getIddireccionescliente() {
        return this.iddireccionescliente;
    }

    public void setIddireccionescliente(int iddireccionescliente) {
        this.iddireccionescliente = iddireccionescliente;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public String getTipodireccion() {
        return this.tipodireccion;
    }

    public void setTipodireccion(String tipodireccion) {
        this.tipodireccion = tipodireccion;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public String toString() {
        return direccion;
    }
    
}
