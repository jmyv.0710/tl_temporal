package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the observacionescliente database table.
 * 
 */
@Entity
@NamedQuery(name="Observacionescliente.findAll", query="SELECT o FROM Observacionescliente o")
public class Observacionescliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idobservaciones;

	private int calificacion;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String novedades;

	private String observaciones;

	private String trafico;

	private String usuariolog;

	private int idSolicitud;

	public Observacionescliente() {
	}

	public int getIdobservaciones() {
		return this.idobservaciones;
	}

	public void setIdobservaciones(int idobservaciones) {
		this.idobservaciones = idobservaciones;
	}

	public int getCalificacion() {
		return this.calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getNovedades() {
		return this.novedades;
	}

	public void setNovedades(String novedades) {
		this.novedades = novedades;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTrafico() {
		return this.trafico;
	}

	public void setTrafico(String trafico) {
		this.trafico = trafico;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

	

}