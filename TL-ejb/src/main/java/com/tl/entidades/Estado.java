package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the estados database table.
 * 
 */
@Entity
@Table(name="estados")
@NamedQuery(name="Estado.findAll", query="SELECT e FROM Estado e")
public class Estado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idestados;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String nombreestado;

	private String tipoestado;

	private String usuariolog;

	//bi-directional many-to-one association to Asignacion
	@OneToMany(mappedBy="estado")
	private List<Asignacion> asignacions;

	//bi-directional many-to-one association to Trazabilidadasignacionestado
	@OneToMany(mappedBy="estado1")
	private List<Trazabilidadasignacionestado> trazabilidadasignacionestados1;

	//bi-directional many-to-one association to Trazabilidadasignacionestado
	@OneToMany(mappedBy="estado2")
	private List<Trazabilidadasignacionestado> trazabilidadasignacionestados2;

	//bi-directional many-to-one association to Trazabilidadasignacionruta
	@OneToMany(mappedBy="estado1")
	private List<Trazabilidadasignacionruta> trazabilidadasignacionrutas1;

	//bi-directional many-to-one association to Trazabilidadasignacionruta
	@OneToMany(mappedBy="estado2")
	private List<Trazabilidadasignacionruta> trazabilidadasignacionrutas2;

	public Estado() {
	}

	public int getIdestados() {
		return this.idestados;
	}

	public void setIdestados(int idestados) {
		this.idestados = idestados;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getNombreestado() {
		return this.nombreestado;
	}

	public void setNombreestado(String nombreestado) {
		this.nombreestado = nombreestado;
	}

	public String getTipoestado() {
		return this.tipoestado;
	}

	public void setTipoestado(String tipoestado) {
		this.tipoestado = tipoestado;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Asignacion> getAsignacions() {
		return this.asignacions;
	}

	public void setAsignacions(List<Asignacion> asignacions) {
		this.asignacions = asignacions;
	}

	public Asignacion addAsignacion(Asignacion asignacion) {
		getAsignacions().add(asignacion);
		asignacion.setEstado(this);

		return asignacion;
	}

	public Asignacion removeAsignacion(Asignacion asignacion) {
		getAsignacions().remove(asignacion);
		asignacion.setEstado(null);

		return asignacion;
	}

	public List<Trazabilidadasignacionestado> getTrazabilidadasignacionestados1() {
		return this.trazabilidadasignacionestados1;
	}

	public void setTrazabilidadasignacionestados1(List<Trazabilidadasignacionestado> trazabilidadasignacionestados1) {
		this.trazabilidadasignacionestados1 = trazabilidadasignacionestados1;
	}

}