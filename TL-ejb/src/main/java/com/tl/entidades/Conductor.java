package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the conductor database table.
 * 
 */
@Entity
@NamedQuery(name="Conductor.findAll", query="SELECT c FROM Conductor c")
public class Conductor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idconductor;

	private String arl;

	private String contrasenia;

	private String correo;

	private String direccion;

	private String eps;

	private int estado;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String fondopension;

	private Object foto;

	private String nombreconductor;

	private String nrodocumento;

	private String telefono;

	private String tipoconductor;

	private String tipodocumento;

	private String usuariolog;
        
	private int idrol;

	public Conductor() {
	}

	public int getIdconductor() {
		return this.idconductor;
	}

	public void setIdconductor(int idconductor) {
		this.idconductor = idconductor;
	}

	public String getArl() {
		return this.arl;
	}

	public void setArl(String arl) {
		this.arl = arl;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEps() {
		return this.eps;
	}

	public void setEps(String eps) {
		this.eps = eps;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getFondopension() {
		return this.fondopension;
	}

	public void setFondopension(String fondopension) {
		this.fondopension = fondopension;
	}

	public Object getFoto() {
		return this.foto;
	}

	public void setFoto(Object foto) {
		this.foto = foto;
	}

	public String getNombreconductor() {
		return this.nombreconductor;
	}

	public void setNombreconductor(String nombreconductor) {
		this.nombreconductor = nombreconductor;
	}

	public String getNrodocumento() {
		return this.nrodocumento;
	}

	public void setNrodocumento(String nrodocumento) {
		this.nrodocumento = nrodocumento;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipoconductor() {
		return this.tipoconductor;
	}

	public void setTipoconductor(String tipoconductor) {
		this.tipoconductor = tipoconductor;
	}

	public String getTipodocumento() {
		return this.tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

        public int getIdrol() {
         return idrol;
        }

        public void setIdrol(int idrol) {
            this.idrol = idrol;
        }

	

}