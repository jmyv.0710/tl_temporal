package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the nominacliente database table.
 *
 */
@Entity
@NamedQuery(name = "Nominacliente.findAll", query = "SELECT n FROM Nominacliente n")
public class Nominacliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idnominacliente;

    private String cargo;

    private String contrasenia;

    private int estado;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    private String identifacionsolicitante;

    private String nombresolicitante;

    private String tipodocumento;

    private String usuariolog;

    private int idcliente;

    private int idRol;

    public Nominacliente() {
    }

    public int getIdnominacliente() {
        return this.idnominacliente;
    }

    public void setIdnominacliente(int idnominacliente) {
        this.idnominacliente = idnominacliente;
    }

    public String getCargo() {
        return this.cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getContrasenia() {
        return this.contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public String getIdentifacionsolicitante() {
        return this.identifacionsolicitante;
    }

    public void setIdentifacionsolicitante(String identifacionsolicitante) {
        this.identifacionsolicitante = identifacionsolicitante;
    }

    public String getNombresolicitante() {
        return this.nombresolicitante;
    }

    public void setNombresolicitante(String nombresolicitante) {
        this.nombresolicitante = nombresolicitante;
    }

    public String getTipodocumento() {
        return this.tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    @Override
    public String toString() {
        return nombresolicitante;
    }
    
}
