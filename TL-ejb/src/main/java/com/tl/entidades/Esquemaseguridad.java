package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the esquemaseguridad database table.
 * 
 */
@Entity
@NamedQuery(name="Esquemaseguridad.findAll", query="SELECT e FROM Esquemaseguridad e")
public class Esquemaseguridad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idesquema;

	private int cantidadescoltas;

	private int estadoescolta;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	@Temporal(TemporalType.DATE)
	private Date horallegada;

	@Temporal(TemporalType.DATE)
	private Date horaretiro;

	@Temporal(TemporalType.DATE)
	private Date horarioservicio;

	private String nivelcriticidad;

	private String nombreescolta;

	private String nroidentificacion;

	private int tarifa;

	private String tipoperimetro;

	private String tiposeguridad;

	private String usuariolog;

	//bi-directional many-to-one association to ServiciosAdicionales
	@OneToMany(mappedBy="esquemaseguridad")
	private List<ServiciosAdicionales> serviciosadicionales;

	public Esquemaseguridad() {
	}

	public int getIdesquema() {
		return this.idesquema;
	}

	public void setIdesquema(int idesquema) {
		this.idesquema = idesquema;
	}

	public int getCantidadescoltas() {
		return this.cantidadescoltas;
	}

	public void setCantidadescoltas(int cantidadescoltas) {
		this.cantidadescoltas = cantidadescoltas;
	}

	public int getEstadoescolta() {
		return this.estadoescolta;
	}

	public void setEstadoescolta(int estadoescolta) {
		this.estadoescolta = estadoescolta;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public Date getHorallegada() {
		return this.horallegada;
	}

	public void setHorallegada(Date horallegada) {
		this.horallegada = horallegada;
	}

	public Date getHoraretiro() {
		return this.horaretiro;
	}

	public void setHoraretiro(Date horaretiro) {
		this.horaretiro = horaretiro;
	}

	public Date getHorarioservicio() {
		return this.horarioservicio;
	}

	public void setHorarioservicio(Date horarioservicio) {
		this.horarioservicio = horarioservicio;
	}

	public String getNivelcriticidad() {
		return this.nivelcriticidad;
	}

	public void setNivelcriticidad(String nivelcriticidad) {
		this.nivelcriticidad = nivelcriticidad;
	}

	public String getNombreescolta() {
		return this.nombreescolta;
	}

	public void setNombreescolta(String nombreescolta) {
		this.nombreescolta = nombreescolta;
	}

	public String getNroidentificacion() {
		return this.nroidentificacion;
	}

	public void setNroidentificacion(String nroidentificacion) {
		this.nroidentificacion = nroidentificacion;
	}

	public int getTarifa() {
		return this.tarifa;
	}

	public void setTarifa(int tarifa) {
		this.tarifa = tarifa;
	}

	public String getTipoperimetro() {
		return this.tipoperimetro;
	}

	public void setTipoperimetro(String tipoperimetro) {
		this.tipoperimetro = tipoperimetro;
	}

	public String getTiposeguridad() {
		return this.tiposeguridad;
	}

	public void setTiposeguridad(String tiposeguridad) {
		this.tiposeguridad = tiposeguridad;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<ServiciosAdicionales> getServiciosadicionales() {
		return this.serviciosadicionales;
	}

	public void setServiciosadicionales(List<ServiciosAdicionales> serviciosadicionales) {
		this.serviciosadicionales = serviciosadicionales;
	}

}