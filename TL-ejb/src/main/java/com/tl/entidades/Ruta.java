package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ruta database table.
 * 
 */
@Entity
@NamedQuery(name="Ruta.findAll", query="SELECT r FROM Ruta r")
public class Ruta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idruta;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String nombreruta;

	private String rutafin;

	private String rutainicio;

	@Temporal(TemporalType.DATE)
	private Date tiempoestimadoruta;

	private String tiporuta;

	private String usuariolog;

	//bi-directional many-to-one association to Asignacion
	@OneToMany(mappedBy="ruta")
	private List<Asignacion> asignacions;

	public Ruta() {
	}

	public int getIdruta() {
		return this.idruta;
	}

	public void setIdruta(int idruta) {
		this.idruta = idruta;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getNombreruta() {
		return this.nombreruta;
	}

	public void setNombreruta(String nombreruta) {
		this.nombreruta = nombreruta;
	}

	public String getRutafin() {
		return this.rutafin;
	}

	public void setRutafin(String rutafin) {
		this.rutafin = rutafin;
	}

	public String getRutainicio() {
		return this.rutainicio;
	}

	public void setRutainicio(String rutainicio) {
		this.rutainicio = rutainicio;
	}

	public Date getTiempoestimadoruta() {
		return this.tiempoestimadoruta;
	}

	public void setTiempoestimadoruta(Date tiempoestimadoruta) {
		this.tiempoestimadoruta = tiempoestimadoruta;
	}

	public String getTiporuta() {
		return this.tiporuta;
	}

	public void setTiporuta(String tiporuta) {
		this.tiporuta = tiporuta;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Asignacion> getAsignacions() {
		return this.asignacions;
	}

	public void setAsignacions(List<Asignacion> asignacions) {
		this.asignacions = asignacions;
	}

	public Asignacion addAsignacion(Asignacion asignacion) {
		getAsignacions().add(asignacion);
		asignacion.setRuta(this);

		return asignacion;
	}

	public Asignacion removeAsignacion(Asignacion asignacion) {
		getAsignacions().remove(asignacion);
		asignacion.setRuta(null);

		return asignacion;
	}

}