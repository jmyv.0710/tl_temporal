package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the asignacion database table.
 * 
 */
@Entity
@NamedQuery(name="Asignacion.findAll", query="SELECT a FROM Asignacion a")
public class Asignacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idasignacion;

	@Temporal(TemporalType.DATE)
	private Date fechaenviocorreo;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	@Temporal(TemporalType.DATE)
	private Date horaactualizacion;

	@Temporal(TemporalType.DATE)
	private Date horaadicional;

	@Temporal(TemporalType.DATE)
	private Date horaenviocorreo;

	private int tarifafacturar;

	@Temporal(TemporalType.DATE)
	private Date tiemporeaccion;

	@Temporal(TemporalType.DATE)
	private Date tiempototaloperacion;

	private String usuariolog;

	//bi-directional many-to-one association to Agendavehiculo
	@ManyToOne
	@JoinColumn(name="idagendavehiculo")
	private Agendavehiculo agendavehiculo;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="idestadoasignacion")
	private Estado estado;

	//bi-directional many-to-one association to Nominatraficolibre
	@ManyToOne
	@JoinColumn(name="idnominatraficolibre")
	private Nominatraficolibre nominatraficolibre;

	//bi-directional many-to-one association to Ruta
	@ManyToOne
	@JoinColumn(name="idruta")
	private Ruta ruta;

	//bi-directional many-to-one association to Solicitud
	@ManyToOne
	@JoinColumn(name="idsolicitud")
	private Solicitud solicitud;

	//bi-directional many-to-one association to Trazabilidadasignacionestado
	@OneToMany(mappedBy="asignacion")
	private List<Trazabilidadasignacionestado> trazabilidadasignacionestados;

	//bi-directional many-to-one association to Trazabilidadasignacionruta
	@OneToMany(mappedBy="asignacion")
	private List<Trazabilidadasignacionruta> trazabilidadasignacionrutas;

	public Asignacion() {
	}

	public int getIdasignacion() {
		return this.idasignacion;
	}

	public void setIdasignacion(int idasignacion) {
		this.idasignacion = idasignacion;
	}

	public Date getFechaenviocorreo() {
		return this.fechaenviocorreo;
	}

	public void setFechaenviocorreo(Date fechaenviocorreo) {
		this.fechaenviocorreo = fechaenviocorreo;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public Date getHoraactualizacion() {
		return this.horaactualizacion;
	}

	public void setHoraactualizacion(Date horaactualizacion) {
		this.horaactualizacion = horaactualizacion;
	}

	public Date getHoraadicional() {
		return this.horaadicional;
	}

	public void setHoraadicional(Date horaadicional) {
		this.horaadicional = horaadicional;
	}

	public Date getHoraenviocorreo() {
		return this.horaenviocorreo;
	}

	public void setHoraenviocorreo(Date horaenviocorreo) {
		this.horaenviocorreo = horaenviocorreo;
	}

	public int getTarifafacturar() {
		return this.tarifafacturar;
	}

	public void setTarifafacturar(int tarifafacturar) {
		this.tarifafacturar = tarifafacturar;
	}

	public Date getTiemporeaccion() {
		return this.tiemporeaccion;
	}

	public void setTiemporeaccion(Date tiemporeaccion) {
		this.tiemporeaccion = tiemporeaccion;
	}

	public Date getTiempototaloperacion() {
		return this.tiempototaloperacion;
	}

	public void setTiempototaloperacion(Date tiempototaloperacion) {
		this.tiempototaloperacion = tiempototaloperacion;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public Agendavehiculo getAgendavehiculo() {
		return this.agendavehiculo;
	}

	public void setAgendavehiculo(Agendavehiculo agendavehiculo) {
		this.agendavehiculo = agendavehiculo;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Nominatraficolibre getNominatraficolibre() {
		return this.nominatraficolibre;
	}

	public void setNominatraficolibre(Nominatraficolibre nominatraficolibre) {
		this.nominatraficolibre = nominatraficolibre;
	}

	public Ruta getRuta() {
		return this.ruta;
	}

	public void setRuta(Ruta ruta) {
		this.ruta = ruta;
	}

	public Solicitud getSolicitud() {
		return this.solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public List<Trazabilidadasignacionestado> getTrazabilidadasignacionestados() {
		return this.trazabilidadasignacionestados;
	}

	public void setTrazabilidadasignacionestados(List<Trazabilidadasignacionestado> trazabilidadasignacionestados) {
		this.trazabilidadasignacionestados = trazabilidadasignacionestados;
	}


}