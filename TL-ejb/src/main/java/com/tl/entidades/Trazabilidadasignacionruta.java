package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the trazabilidadasignacionruta database table.
 *
 */
@Entity
@NamedQuery(name = "Trazabilidadasignacionruta.findAll", query = "SELECT t FROM Trazabilidadasignacionruta t")
public class Trazabilidadasignacionruta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idtrazabilidad;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    @Temporal(TemporalType.DATE)
    private Date fecharegistro;

    private int numeroregistro;

    private String observacionesregistro;

    private String ubicacionregistro;

    private String usuariolog;

    private int idAsignacion;

    private int estadoAnterior;

    private int nuevoEstado;

    public Trazabilidadasignacionruta() {
    }

    public int getIdtrazabilidad() {
        return this.idtrazabilidad;
    }

    public void setIdtrazabilidad(int idtrazabilidad) {
        this.idtrazabilidad = idtrazabilidad;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public Date getFecharegistro() {
        return this.fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public int getNumeroregistro() {
        return this.numeroregistro;
    }

    public void setNumeroregistro(int numeroregistro) {
        this.numeroregistro = numeroregistro;
    }

    public String getObservacionesregistro() {
        return this.observacionesregistro;
    }

    public void setObservacionesregistro(String observacionesregistro) {
        this.observacionesregistro = observacionesregistro;
    }

    public String getUbicacionregistro() {
        return this.ubicacionregistro;
    }

    public void setUbicacionregistro(String ubicacionregistro) {
        this.ubicacionregistro = ubicacionregistro;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdAsignacion() {
        return idAsignacion;
    }

    public void setIdAsignacion(int idAsignacion) {
        this.idAsignacion = idAsignacion;
    }

    public int getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(int estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public int getNuevoEstado() {
        return nuevoEstado;
    }

    public void setNuevoEstado(int nuevoEstado) {
        this.nuevoEstado = nuevoEstado;
    }

}
