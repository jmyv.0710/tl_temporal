package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the cliente database table.
 *
 */
@Entity
@NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idcliente;

    private int estado;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    private String nombrecliente;

    private String nroidentificacion;

    private String observacionesespeciales;

    private String tipocliente;

    private String tipopersona;

    private String usuariolog;

    private int idClienteTercero;

    public Cliente() {
    }

    public int getIdcliente() {
        return this.idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public String getNombrecliente() {
        return this.nombrecliente;
    }

    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }

    public String getNroidentificacion() {
        return this.nroidentificacion;
    }

    public void setNroidentificacion(String nroidentificacion) {
        this.nroidentificacion = nroidentificacion;
    }

    public String getObservacionesespeciales() {
        return this.observacionesespeciales;
    }

    public void setObservacionesespeciales(String observacionesespeciales) {
        this.observacionesespeciales = observacionesespeciales;
    }

    public String getTipocliente() {
        return this.tipocliente;
    }

    public void setTipocliente(String tipocliente) {
        this.tipocliente = tipocliente;
    }

    public String getTipopersona() {
        return this.tipopersona;
    }

    public void setTipopersona(String tipopersona) {
        this.tipopersona = tipopersona;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdClienteTercero() {
        return idClienteTercero;
    }

    public void setIdClienteTercero(int idClienteTercero) {
        this.idClienteTercero = idClienteTercero;
    }

    @Override
    public String toString() {
        return nombrecliente;
    }
    
    

}
