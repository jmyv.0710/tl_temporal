package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the correosnotificacion database table.
 *
 */
@Entity
@NamedQuery(name = "Correosnotificacion.findAll", query = "SELECT c FROM Correosnotificacion c")
public class Correosnotificacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idcorreonotificacion;

    private String correo;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    private String nombrepersona;

    private String usuariolog;

    private int idCliente;

    public Correosnotificacion() {
    }

    public int getIdcorreonotificacion() {
        return this.idcorreonotificacion;
    }

    public void setIdcorreonotificacion(int idcorreonotificacion) {
        this.idcorreonotificacion = idcorreonotificacion;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public String getNombrepersona() {
        return this.nombrepersona;
    }

    public void setNombrepersona(String nombrepersona) {
        this.nombrepersona = nombrepersona;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

}
