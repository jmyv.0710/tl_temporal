package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the precinto database table.
 * 
 */
@Entity
@NamedQuery(name="Precinto.findAll", query="SELECT p FROM Precinto p")
public class Precinto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idprecinto;

	private int estado;

	@Temporal(TemporalType.DATE)
	private Date fechaentregaconductor;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	@Temporal(TemporalType.DATE)
	private Date fecharadicado;

	@Temporal(TemporalType.DATE)
	private Date fechauso;

	private int nroprecinto;

	private String observaciones;

	private String tipoprecinto;

	private String usuariolog;

	private int conductorAsociado;

	public Precinto() {
	}

	public int getIdprecinto() {
		return this.idprecinto;
	}

	public void setIdprecinto(int idprecinto) {
		this.idprecinto = idprecinto;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechaentregaconductor() {
		return this.fechaentregaconductor;
	}

	public void setFechaentregaconductor(Date fechaentregaconductor) {
		this.fechaentregaconductor = fechaentregaconductor;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public Date getFecharadicado() {
		return this.fecharadicado;
	}

	public void setFecharadicado(Date fecharadicado) {
		this.fecharadicado = fecharadicado;
	}

	public Date getFechauso() {
		return this.fechauso;
	}

	public void setFechauso(Date fechauso) {
		this.fechauso = fechauso;
	}

	public int getNroprecinto() {
		return this.nroprecinto;
	}

	public void setNroprecinto(int nroprecinto) {
		this.nroprecinto = nroprecinto;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTipoprecinto() {
		return this.tipoprecinto;
	}

	public void setTipoprecinto(String tipoprecinto) {
		this.tipoprecinto = tipoprecinto;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

    public int getConductorAsociado() {
        return conductorAsociado;
    }

    public void setConductorAsociado(int conductorAsociado) {
        this.conductorAsociado = conductorAsociado;
    }

	

}