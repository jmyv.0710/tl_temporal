package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the nominatraficolibre database table.
 *
 */
@Entity
@NamedQuery(name = "Nominatraficolibre.findAll", query = "SELECT n FROM Nominatraficolibre n")
public class Nominatraficolibre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idnomina;

    private String contrasenia;

    private String correo;

    private String direccion;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;

    private String nombre;

    private int nrodocumento;

    private String telefono;

    private String tipodocumento;

    private String usuariolog;

    private int idRol;

    public Nominatraficolibre() {
    }

    public int getIdnomina() {
        return this.idnomina;
    }

    public void setIdnomina(int idnomina) {
        this.idnomina = idnomina;
    }

    public String getContrasenia() {
        return this.contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public Date getFechanacimiento() {
        return this.fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNrodocumento() {
        return this.nrodocumento;
    }

    public void setNrodocumento(int nrodocumento) {
        this.nrodocumento = nrodocumento;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipodocumento() {
        return this.tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

}
