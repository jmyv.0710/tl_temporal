package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the inventario database table.
 * 
 */
@Entity
@NamedQuery(name="Inventario.findAll", query="SELECT i FROM Inventario i")
public class Inventario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idinventario;

	private int cantidad;

	private String epp;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String nombreequipo;

	private String observacion;

	private String tipoequipo;

	private String usuariolog;

	//bi-directional many-to-one association to Auxiliar
	@OneToMany(mappedBy="inventario")
	private List<Auxiliar> auxiliars;

	public Inventario() {
	}

	public int getIdinventario() {
		return this.idinventario;
	}

	public void setIdinventario(int idinventario) {
		this.idinventario = idinventario;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getEpp() {
		return this.epp;
	}

	public void setEpp(String epp) {
		this.epp = epp;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getNombreequipo() {
		return this.nombreequipo;
	}

	public void setNombreequipo(String nombreequipo) {
		this.nombreequipo = nombreequipo;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getTipoequipo() {
		return this.tipoequipo;
	}

	public void setTipoequipo(String tipoequipo) {
		this.tipoequipo = tipoequipo;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Auxiliar> getAuxiliars() {
		return this.auxiliars;
	}

	public void setAuxiliars(List<Auxiliar> auxiliars) {
		this.auxiliars = auxiliars;
	}

}