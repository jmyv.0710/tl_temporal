package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the agendavehiculo database table.
 * 
 */
@Entity
@NamedQuery(name="Agendavehiculo.findAll", query="SELECT a FROM Agendavehiculo a")
public class Agendavehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idagenda;

	private int estado;

	@Temporal(TemporalType.DATE)
	private Date fechaasignacion;

	@Temporal(TemporalType.DATE)
	private Date fechacita;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private String usuariolog;

	//bi-directional many-to-one association to Conductor
	@ManyToOne
	@JoinColumn(name="idconductor")
	private Conductor conductor;

	//bi-directional many-to-one association to Solicitud
	@ManyToOne
	@JoinColumn(name="idsolicitud")
	private Solicitud solicitud;

	//bi-directional many-to-one association to Vehiculo
	@ManyToOne
	@JoinColumn(name="idvsolicitudehiculo")
	private Vehiculo vehiculo;

	//bi-directional many-to-one association to Asignacion
	@OneToMany(mappedBy="agendavehiculo")
	private List<Asignacion> asignacions;

	public Agendavehiculo() {
	}

	public int getIdagenda() {
		return this.idagenda;
	}

	public void setIdagenda(int idagenda) {
		this.idagenda = idagenda;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechaasignacion() {
		return this.fechaasignacion;
	}

	public void setFechaasignacion(Date fechaasignacion) {
		this.fechaasignacion = fechaasignacion;
	}

	public Date getFechacita() {
		return this.fechacita;
	}

	public void setFechacita(Date fechacita) {
		this.fechacita = fechacita;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public Conductor getConductor() {
		return this.conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}

	public Solicitud getSolicitud() {
		return this.solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public List<Asignacion> getAsignacions() {
		return this.asignacions;
	}

	public void setAsignacions(List<Asignacion> asignacions) {
		this.asignacions = asignacions;
	}

	public Asignacion addAsignacion(Asignacion asignacion) {
		getAsignacions().add(asignacion);
		asignacion.setAgendavehiculo(this);

		return asignacion;
	}

	public Asignacion removeAsignacion(Asignacion asignacion) {
		getAsignacions().remove(asignacion);
		asignacion.setAgendavehiculo(null);

		return asignacion;
	}

}