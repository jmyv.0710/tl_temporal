package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the solicitud database table.
 *
 */
@Entity
@NamedQuery(name = "Solicitud.findAll", query = "SELECT s FROM Solicitud s")
public class Solicitud implements Serializable {

    private static final long serialVersionUID = 1L;

    public Solicitud() {

    }

    public Solicitud(int idsolicitud, Date fechacargue, Date fechalog,
            Date fechasolicitud, String fpu, int idlistaserrviciosadicionales,
            String impoexpo, int nroreserva, String observaciones, int serviciologisticaadicional,
            int tarifa, int tiemporeaccionsolicitud, String usuariolog, int valormercancia, List<Agendavehiculo> agendavehiculos,
            List<Asignacion> asignacions, List<Observacionescliente> observacionesclientes, List<ServiciosAdicionales> serviciosadicionales,
            Carga carga, Direccion direccion1, Direccion direccion2, Nominacliente nominacliente1, Nominacliente nominacliente2,
            Nominacliente nominacliente3, Tiposervicio tiposervicioBean) {

    }

    @Id
    private int idsolicitud;

    @Temporal(TemporalType.DATE)
    private Date fechacargue;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    @Temporal(TemporalType.DATE)
    private Date fechasolicitud;

    private String fpu;

    private int origenCarga;

    private int destinoCarga;

    private int idCarga;

    private int tipoServicio;

    private int solicitadopor;

    private int contactoorigen;

    private int contactodestino;

    private int idlistaserrviciosadicionales;

    private String impoexpo;

    private int nroreserva;

    private String observaciones;

    private int serviciologisticaadicional;

    private int tarifa;

    private int tiemporeaccionsolicitud;

    private String usuariolog;

    private int valormercancia;

    //METODOS
    public int getIdsolicitud() {
        return this.idsolicitud;
    }

    public void setIdsolicitud(int idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public Date getFechacargue() {
        return this.fechacargue;
    }

    public void setFechacargue(Date fechacargue) {
        this.fechacargue = fechacargue;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public Date getFechasolicitud() {
        return this.fechasolicitud;
    }

    public void setFechasolicitud(Date fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public String getFpu() {
        return this.fpu;
    }

    public void setFpu(String fpu) {
        this.fpu = fpu;
    }

    public int getIdlistaserrviciosadicionales() {
        return this.idlistaserrviciosadicionales;
    }

    public void setIdlistaserrviciosadicionales(int idlistaserrviciosadicionales) {
        this.idlistaserrviciosadicionales = idlistaserrviciosadicionales;
    }

    public String getImpoexpo() {
        return this.impoexpo;
    }

    public void setImpoexpo(String impoexpo) {
        this.impoexpo = impoexpo;
    }

    public int getNroreserva() {
        return this.nroreserva;
    }

    public void setNroreserva(int nroreserva) {
        this.nroreserva = nroreserva;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getServiciologisticaadicional() {
        return this.serviciologisticaadicional;
    }

    public void setServiciologisticaadicional(int serviciologisticaadicional) {
        this.serviciologisticaadicional = serviciologisticaadicional;
    }

    public int getTarifa() {
        return this.tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public int getTiemporeaccionsolicitud() {
        return this.tiemporeaccionsolicitud;
    }

    public void setTiemporeaccionsolicitud(int tiemporeaccionsolicitud) {
        this.tiemporeaccionsolicitud = tiemporeaccionsolicitud;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getValormercancia() {
        return this.valormercancia;
    }

    public void setValormercancia(int valormercancia) {
        this.valormercancia = valormercancia;
    }

    public int getOrigenCarga() {
        return origenCarga;
    }

    public void setOrigenCarga(int origenCarga) {
        this.origenCarga = origenCarga;
    }

    public int getDestinoCarga() {
        return destinoCarga;
    }

    public void setDestinoCarga(int destinoCarga) {
        this.destinoCarga = destinoCarga;
    }

    public int getIdCarga() {
        return idCarga;
    }

    public void setIdCarga(int idCarga) {
        this.idCarga = idCarga;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public int getSolicitadopor() {
        return solicitadopor;
    }

    public void setSolicitadopor(int solicitadopor) {
        this.solicitadopor = solicitadopor;
    }

    public int getContactoorigen() {
        return contactoorigen;
    }

    public void setContactoorigen(int contactoorigen) {
        this.contactoorigen = contactoorigen;
    }

    public int getContactodestino() {
        return contactodestino;
    }

    public void setContactodestino(int contactodestino) {
        this.contactodestino = contactodestino;
    }

}
