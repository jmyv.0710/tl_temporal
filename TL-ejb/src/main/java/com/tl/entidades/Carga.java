package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the carga database table.
 * 
 */
@Entity
@NamedQuery(name="Carga.findAll", query="SELECT c FROM Carga c")
public class Carga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idcarga;

	private int altura;

	private int ancho;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	private int largo;

	private int peso;

	private String responsablepoliza;

	private String tipocarga;

	private String usuariolog;

	private int valorcarga;

	private int volumen;

	//bi-directional many-to-one association to Solicitud
	@OneToMany(mappedBy="carga")
	private List<Solicitud> solicituds;

	public Carga() {
	}

	public int getIdcarga() {
		return this.idcarga;
	}

	public void setIdcarga(int idcarga) {
		this.idcarga = idcarga;
	}

	public int getAltura() {
		return this.altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getAncho() {
		return this.ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public int getLargo() {
		return this.largo;
	}

	public void setLargo(int largo) {
		this.largo = largo;
	}

	public int getPeso() {
		return this.peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public String getResponsablepoliza() {
		return this.responsablepoliza;
	}

	public void setResponsablepoliza(String responsablepoliza) {
		this.responsablepoliza = responsablepoliza;
	}

	public String getTipocarga() {
		return this.tipocarga;
	}

	public void setTipocarga(String tipocarga) {
		this.tipocarga = tipocarga;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public int getValorcarga() {
		return this.valorcarga;
	}

	public void setValorcarga(int valorcarga) {
		this.valorcarga = valorcarga;
	}

	public int getVolumen() {
		return this.volumen;
	}

	public void setVolumen(int volumen) {
		this.volumen = volumen;
	}

	public List<Solicitud> getSolicituds() {
		return this.solicituds;
	}

	public void setSolicituds(List<Solicitud> solicituds) {
		this.solicituds = solicituds;
	}


}