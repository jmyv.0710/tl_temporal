package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tiposervicio database table.
 * 
 */
@Entity
@NamedQuery(name="Tiposervicio.findAll", query="SELECT t FROM Tiposervicio t")
public class Tiposervicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idtiposervicio;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	@Temporal(TemporalType.DATE)
	private Date horaservicio;

	private String nombretiposervicio;

	private String usuariolog;

	//bi-directional many-to-one association to Solicitud
	@OneToMany(mappedBy="tiposervicioBean")
	private List<Solicitud> solicituds;

	public Tiposervicio() {
	}

	public int getIdtiposervicio() {
		return this.idtiposervicio;
	}

	public void setIdtiposervicio(int idtiposervicio) {
		this.idtiposervicio = idtiposervicio;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public Date getHoraservicio() {
		return this.horaservicio;
	}

	public void setHoraservicio(Date horaservicio) {
		this.horaservicio = horaservicio;
	}

	public String getNombretiposervicio() {
		return this.nombretiposervicio;
	}

	public void setNombretiposervicio(String nombretiposervicio) {
		this.nombretiposervicio = nombretiposervicio;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Solicitud> getSolicituds() {
		return this.solicituds;
	}

	public void setSolicituds(List<Solicitud> solicituds) {
		this.solicituds = solicituds;
	}

	
}