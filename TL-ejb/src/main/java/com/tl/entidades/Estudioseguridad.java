package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the estudioseguridad database table.
 * 
 */
@Entity
@NamedQuery(name="Estudioseguridad.findAll", query="SELECT e FROM Estudioseguridad e")
public class Estudioseguridad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idestudioseguridad;

	private int estado;

	@Temporal(TemporalType.DATE)
	private Date fechalog;

	@Temporal(TemporalType.DATE)
	private Date fechavencimiento;

	private String observacion;

	private String rutadocumento;

	private String tipodocumento;

	private String tipoestudioseguridad;

	private String usuariolog;

	//bi-directional many-to-one association to Auxiliar
	@OneToMany(mappedBy="estudioseguridad")
	private List<Auxiliar> auxiliars;

	private int idcliente;

	private int idconductor;

	private int idvehiculo;

	public Estudioseguridad() {
	}

	public int getIdestudioseguridad() {
		return this.idestudioseguridad;
	}

	public void setIdestudioseguridad(int idestudioseguridad) {
		this.idestudioseguridad = idestudioseguridad;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechalog() {
		return this.fechalog;
	}

	public void setFechalog(Date fechalog) {
		this.fechalog = fechalog;
	}

	public Date getFechavencimiento() {
		return this.fechavencimiento;
	}

	public void setFechavencimiento(Date fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getRutadocumento() {
		return this.rutadocumento;
	}

	public void setRutadocumento(String rutadocumento) {
		this.rutadocumento = rutadocumento;
	}

	public String getTipodocumento() {
		return this.tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public String getTipoestudioseguridad() {
		return this.tipoestudioseguridad;
	}

	public void setTipoestudioseguridad(String tipoestudioseguridad) {
		this.tipoestudioseguridad = tipoestudioseguridad;
	}

	public String getUsuariolog() {
		return this.usuariolog;
	}

	public void setUsuariolog(String usuariolog) {
		this.usuariolog = usuariolog;
	}

	public List<Auxiliar> getAuxiliars() {
		return this.auxiliars;
	}

	public void setAuxiliars(List<Auxiliar> auxiliars) {
		this.auxiliars = auxiliars;
	}

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public int getIdconductor() {
        return idconductor;
    }

    public void setIdconductor(int idconductor) {
        this.idconductor = idconductor;
    }

    public int getIdvehiculo() {
        return idvehiculo;
    }

    public void setIdvehiculo(int idvehiculo) {
        this.idvehiculo = idvehiculo;
    }

	

}