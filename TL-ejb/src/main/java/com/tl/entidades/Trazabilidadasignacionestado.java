package com.tl.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the trazabilidadasignacionestado database table.
 *
 */
@Entity
@NamedQuery(name = "Trazabilidadasignacionestado.findAll", query = "SELECT t FROM Trazabilidadasignacionestado t")
public class Trazabilidadasignacionestado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int idtrazabilidad;

    @Temporal(TemporalType.DATE)
    private Date fechacambio;

    @Temporal(TemporalType.DATE)
    private Date fechalog;

    @Temporal(TemporalType.DATE)
    private Date fecharegistro;

    private String kpi;

    private String observacion;

    private String tipokpi;

    private String usuariolog;

    private int idAsignacion;

    private int estadoAnterior;

    private int nuevoEstado;

    private int idEmpleadoNomina;

    public Trazabilidadasignacionestado() {
    }

    public int getIdtrazabilidad() {
        return this.idtrazabilidad;
    }

    public void setIdtrazabilidad(int idtrazabilidad) {
        this.idtrazabilidad = idtrazabilidad;
    }

    public Date getFechacambio() {
        return this.fechacambio;
    }

    public void setFechacambio(Date fechacambio) {
        this.fechacambio = fechacambio;
    }

    public Date getFechalog() {
        return this.fechalog;
    }

    public void setFechalog(Date fechalog) {
        this.fechalog = fechalog;
    }

    public Date getFecharegistro() {
        return this.fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public String getKpi() {
        return this.kpi;
    }

    public void setKpi(String kpi) {
        this.kpi = kpi;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipokpi() {
        return this.tipokpi;
    }

    public void setTipokpi(String tipokpi) {
        this.tipokpi = tipokpi;
    }

    public String getUsuariolog() {
        return this.usuariolog;
    }

    public void setUsuariolog(String usuariolog) {
        this.usuariolog = usuariolog;
    }

    public int getIdAsignacion() {
        return idAsignacion;
    }

    public void setIdAsignacion(int idAsignacion) {
        this.idAsignacion = idAsignacion;
    }

    public int getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(int estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public int getNuevoEstado() {
        return nuevoEstado;
    }

    public void setNuevoEstado(int nuevoEstado) {
        this.nuevoEstado = nuevoEstado;
    }

    public int getIdEmpleadoNomina() {
        return idEmpleadoNomina;
    }

    public void setIdEmpleadoNomina(int idEmpleadoNomina) {
        this.idEmpleadoNomina = idEmpleadoNomina;
    }

}
