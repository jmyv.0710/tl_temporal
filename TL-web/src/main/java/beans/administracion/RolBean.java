/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.administracion;

import com.tl.entidades.Rol;
import com.tl.manager.RolManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "rolBean")
public class RolBean {
    
    private RolManager rolControlador;
    
    private Rol nuevoRol;
    private List<Rol> listadoRoles;
    
    private List<String> listaEstados;
    
    @PostConstruct
    public void init(){
        System.out.println("beans.VehiculoBean.init()Inicio");
        rolControlador = new RolManager();
        
        nuevoRol = new Rol();
        
        listarRoles();
        listarEstados();
    }
    
    public void registrarRol(){
        System.out.println("beans.RolBean.registrarRol()Inicio");
        nuevoRol.setFechalog(new Date());
        nuevoRol.setUsuariolog("1");
        boolean respuesta = rolControlador.crearRol(nuevoRol);
        if(respuesta)
            addMessage(FacesMessage.SEVERITY_INFO, "Èxito", "Se Registró el rol satisfactoriamente");
        else
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rol no pudo ser registrado");
        
        limpiarCampos();
        listarRoles();
    }
    
    public void listarRoles(){
        System.out.println("beans.RolBean.listarRoles()");
        listadoRoles = rolControlador.listarRoles();
        if(listadoRoles == null || listadoRoles.isEmpty() || listadoRoles.size() == 0){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "No hay roles registrados");
        }
    }
    
    public void inactivarRol(){
        System.out.println("beans.RolBean.inactivarRol()Inicio");
        boolean respuesta = rolControlador.inactivarRol(0);
        if(respuesta)
            addMessage(FacesMessage.SEVERITY_INFO, "Èxito", "Se inactivo el rol satisfactoriamente");
        else
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rol no pudo ser inactivado");
    }
    
    private void listarEstados(){
        System.out.println("beans.administracion.RolBean.listarEstado()");
        listaEstados = new ArrayList<>();
        listaEstados.add("ACTIVO");
        listaEstados.add("INACTIVO");
    }
    
    private void limpiarCampos(){
        nuevoRol = new Rol();
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    public Rol getNuevoRol() {
        return nuevoRol;
    }

    public void setNuevoRol(Rol nuevoRol) {
        this.nuevoRol = nuevoRol;
    }

    public List<Rol> getListadoRoles() {
        return listadoRoles;
    }

    public void setListadoRoles(List<Rol> listadoRoles) {
        this.listadoRoles = listadoRoles;
    }

    public List<String> getListaEstados() {
        return listaEstados;
    }

    public void setListaEstados(List<String> listaEstados) {
        this.listaEstados = listaEstados;
    }
    
}
