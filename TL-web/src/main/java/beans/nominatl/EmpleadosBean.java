/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.nominatl;

import com.tl.entidades.Nominatraficolibre;
import com.tl.entidades.Rol;
import com.tl.manager.NominaTraficoLibreManager;
import com.tl.manager.RolManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "empleadosBean")
public class EmpleadosBean {

    private NominaTraficoLibreManager empleadosControlador;
    private RolManager rolControlador;
    
    private Nominatraficolibre nuevoEmpleado;
    private List<Nominatraficolibre> listaEmpleados;
    private String rolSeleccionado;
    
    private List<String> listadoTiposDocumento;
    private List<Rol> listaRoles;
    
    @PostConstruct
    public void init(){
        System.out.println("beans.nominatl.EmpleadosBean.init()inicio");
        empleadosControlador = new NominaTraficoLibreManager();
        rolControlador = new RolManager();
        
        nuevoEmpleado = new Nominatraficolibre();
        listarEmpleados();
        llenarListadoTiposDocumentos();
        llenarListaRoles();
    }
    
    public void registrarEmpleado(){
        System.out.println("beans.nominatl.EmpleadosBean.registrarEmpleado()");
        nuevoEmpleado.setFechalog(new Date());
        nuevoEmpleado.setUsuariolog("USER ADMIN");
        
        int idRol = buscarRol(rolSeleccionado);
        nuevoEmpleado.setIdRol(idRol);
        boolean respuesta = empleadosControlador.adicionarNomina(nuevoEmpleado);
        if(respuesta){
            addMessage(FacesMessage.SEVERITY_INFO, "Exito!", "Registro de empleado exitoso");
            limpiarCampos();
        } else {
            addMessage(FacesMessage.SEVERITY_FATAL, "Error!", "no se pudo registrar el empelado");
        }
        listarEmpleados();
    }
    
    private int buscarRol(String rol){
        System.out.println("beans.nominatl.EmpleadosBean.BuscarRol()");
        for (Rol r : listaRoles) {
            if (r.getRol().equals(rol)){
                return r.getIdrol();
            }
        }
        return 0;
    }
    
    private void listarEmpleados(){
        System.out.println("beans.nominatl.EmpleadosBean.listarEmpleados()");
        listaEmpleados = empleadosControlador.listaNominaTrafLibre();
    }
    
    private void llenarListadoTiposDocumentos(){
        System.out.println("beans.nominatl.EmpleadosBean.llenarListadoTiposDocumentos()");
        listadoTiposDocumento = new ArrayList<>();
        listadoTiposDocumento.add("CEDULA CIUDADANÍA");
        listadoTiposDocumento.add("CEDULA EXTRANJERÍA");
    }
    
    private void llenarListaRoles(){
        System.out.println("beans.nominatl.EmpleadosBean.llenarListaRoles()");
        listaRoles = rolControlador.listarRoles();
    }
    
    private void limpiarCampos(){
        nuevoEmpleado = new Nominatraficolibre();
        rolSeleccionado = "";
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }
    
    public Nominatraficolibre getNuevoEmpleado() {
        return nuevoEmpleado;
    }

    public void setNuevoEmpleado(Nominatraficolibre nuevoEmpleado) {
        this.nuevoEmpleado = nuevoEmpleado;
    }

    public List<Nominatraficolibre> getListaEmpleados() {
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Nominatraficolibre> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    public List<String> getListadoTiposDocumento() {
        return listadoTiposDocumento;
    }

    public void setListadoTiposDocumento(List<String> listadoTiposDocumento) {
        this.listadoTiposDocumento = listadoTiposDocumento;
    }

    public List<Rol> getListaRoles() {
        return listaRoles;
    }

    public void setListaRoles(List<Rol> listaRoles) {
        this.listaRoles = listaRoles;
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }
    
}
