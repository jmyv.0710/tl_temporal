/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.nominatl;

import com.tl.entidades.Vehiculo;
import com.tl.manager.VehiculoManager;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "vehiculoBean")
public class VehiculoBean {
    
    private VehiculoManager vehiculoControlador;
    private Vehiculo vehiculo;
    private List<Vehiculo> listadoVehiculos;
    private List<String> listadoTipoVehiculos;
    private List<String> listadoTipoPropietarios;
    
    private String placa;
    
    
    @PostConstruct
    public void init(){
        System.out.println("beans.VehiculoBean.init()Inicio");
        vehiculoControlador = new VehiculoManager();
        
        vehiculo = new Vehiculo();
        
        llenarListaTipoVehiculos();
        listarVehiculos();
        llenarListaTipoPropíetarios();
    }
    
    public void registrarVehiculo(){
        System.out.println("beans.VehiculoBean.registrarVehiculo()Inicio");
        boolean respuesta = vehiculoControlador.crearVehiculo(vehiculo);
        if(respuesta){
            addMessage(FacesMessage.SEVERITY_INFO, "Èxito", "Se Registró el vehiculo satisfactoriamente");
            limpiarCampos();
        }else{
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El vehiculo no pudo ser registrado");
        }
        listarVehiculos();
    }
    
    public void buscarVehiculo(){
        System.out.println("beans.VehiculoBean.buscarVehiculo()Inicio");
//        camion = vehiculoControlador.0
    }
    
    public void actualizarVehiculo(){
        System.out.println("beans.VehiculoBean.actualizarVehiculo()Inicio");
    }
    
    public void listarVehiculos(){
        System.out.println("beans.VehiculoBean.listarVehiculos()Inicio");
        listadoVehiculos = vehiculoControlador.listarVehiculos();
        if(listadoVehiculos == null || listadoVehiculos.isEmpty() || listadoVehiculos.isEmpty()){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "No hay vehiculos registrados");
        }
    }
    
    public void inactivarVehiculo(){
        System.out.println("beans.VehiculoBean.inactivarVehiculo()Inicio");
        boolean respuesta = vehiculoControlador.inactivarVehiculo(placa);
        if(respuesta)
            addMessage(FacesMessage.SEVERITY_INFO, "Èxito", "Se inactivo el vehiculo satisfactoriamente");
        else
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El vehiculo no pudo ser inactivado");
    }
    
    private void llenarListaTipoVehiculos(){
        System.out.println("beans.nominatl.VehiculoBean.llenarListalistadoTipoVehiculos()Inicio");
        listadoTipoVehiculos = new ArrayList<>();
        listadoTipoVehiculos.add("Turbo");
        listadoTipoVehiculos.add("Patineta");
        listadoTipoVehiculos.add("Tractomula");
    }
    
    private void llenarListaTipoPropíetarios(){
        System.out.println("beans.nominatl.VehiculoBean.llenarListaTipoPropíetarios()");
        listadoTipoPropietarios = new ArrayList<>();
        listadoTipoPropietarios.add("PROPIO");
        listadoTipoPropietarios.add("EXTERNO");
    }
    
    private void limpiarCampos(){
        vehiculo = new Vehiculo();
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public List<Vehiculo> getListadoVehiculos() {
        return listadoVehiculos;
    }

    public void setListadoVehiculos(List<Vehiculo> listadoVehiculos) {
        this.listadoVehiculos = listadoVehiculos;
    }

    public List<String> getListadoTipoVehiculos() {
        return listadoTipoVehiculos;
    }

    public void setListadoTipoVehiculos(List<String> listadoTipoVehiculos) {
        this.listadoTipoVehiculos = listadoTipoVehiculos;
    }

    public List<String> getListadoTipoPropietarios() {
        return listadoTipoPropietarios;
    }

    public void setListadoTipoPropietarios(List<String> listadoTipoPropietarios) {
        this.listadoTipoPropietarios = listadoTipoPropietarios;
    }
    
    
}
