/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.nominatl;

import com.tl.entidades.Conductor;
import com.tl.manager.ConductorManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "conductorBean")
public class ConductorBean {
    
    private ConductorManager conductorControlador;
    private Conductor conductor;
    
    private List<Conductor>listadoConductores;
    private List<Conductor>listarC;
    private List<String> listaTipoDocumentos;
    private List<String> listaTipoConductores;
    private String nroDocumento;
    private boolean visualizarPanel;
    
    @PostConstruct
    public void init() {
        System.out.println("beans.ConductorBean.init()Inicio");
        
        conductorControlador = new ConductorManager();
        conductor = new Conductor();
        
        llenarListaTipoDocumentos();
        llenarListaTipoConductores();
        listarConductores();
    }
    
    public void registrarConductor(){
        System.out.println("beans.ConductorBean.registrarConductor()Inicio");
        
        conductor.setFechalog(new Date());
        conductor.setUsuariolog("1");
        conductor.setIdrol(1);
        boolean respuesta = conductorControlador.crearConductor(conductor);
        if(respuesta)
            addMessage(FacesMessage.SEVERITY_INFO, "Èxito", "Se Registró el conductor satisfactoriamente");
        else
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El conductor no pudo ser registrado");
        
        limpiarCampos();
        listarConductores();
    }
    
    public void buscarConductor(){
        System.out.println("beans.ConductorBean.buscarConductor()Inicio");
        conductor = conductorControlador.consultarConductorDocto(nroDocumento);
        listarC = new ArrayList<>();
        
        if(conductor != null){
            visualizarPanel = true;
            listarC.add(conductor);
        } else {
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El conductor no existe");
        }
    }
    
    public void actualizarConductor(){
    
    }
    
    public void listarConductores(){
        System.out.println("beans.ConductorBean.listarConductores()");
        listadoConductores = conductorControlador.listarConductores();
        if(listadoConductores == null || listadoConductores.isEmpty() || listadoConductores.size() == 0){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "No hay roles registrados");
        }
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }
    
    private void llenarListaTipoDocumentos(){
        listaTipoDocumentos = new ArrayList<>();
        listaTipoDocumentos.add("CÉDULA DE CIUDADANÍA");
        listaTipoDocumentos.add("CÉDULA DE EXTRANJERÍA");
    }
    
    private void llenarListaTipoConductores(){
        listaTipoConductores = new ArrayList<>();
        listaTipoConductores.add("PROPIO");
        listaTipoConductores.add("EXTERNO");
    }
    
    public void limpiarCampos(){
        System.out.println("beans.ConductorBean.limpiarCampos()inicio");
        this.conductor = new Conductor();
        visualizarPanel = false;
        nroDocumento = "";
        
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    public List<Conductor> getListadoConductores() {
        return listadoConductores;
    }

    public void setListadoConductores(List<Conductor> listadoConductores) {
        this.listadoConductores = listadoConductores;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public ConductorManager getConductorControlador() {
        return conductorControlador;
    }

    public void setConductorControlador(ConductorManager conductorControlador) {
        this.conductorControlador = conductorControlador;
    }

    public List<String> getListaTipoDocumentos() {
        return listaTipoDocumentos;
    }

    public void setListaTipoDocumentos(List<String> listaTipoDocumentos) {
        this.listaTipoDocumentos = listaTipoDocumentos;
    }

    public List<String> getListaTipoConductores() {
        return listaTipoConductores;
    }

    public void setListaTipoConductores(List<String> listaTipoConductores) {
        this.listaTipoConductores = listaTipoConductores;
    }

    public List<Conductor> getListarC() {
        return listarC;
    }

    public void setListarC(List<Conductor> listarC) {
        this.listarC = listarC;
    }

    public boolean isVisualizarPanel() {
        return visualizarPanel;
    }

    public void setVisualizarPanel(boolean visualizarPanel) {
        this.visualizarPanel = visualizarPanel;
    }
    
    
}
