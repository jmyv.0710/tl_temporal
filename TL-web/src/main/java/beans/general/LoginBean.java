/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.general;

import com.tl.entidades.Nominatraficolibre;
import com.tl.manager.NominaTraficoLibreManager;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@SessionScoped
@ManagedBean(name = "loginBean")
public class LoginBean {
    
    private String usuario;
    private String password;
    
    private NominaTraficoLibreManager nominaControlador;
//    private EmpleadosOficina empleado;
    
    @PostConstruct
    public void init() {
        System.out.println("beans.LoginBean.init()");
        nominaControlador = new NominaTraficoLibreManager();
    }
    
    public void hacerLogin(){
        try {
            System.out.println("beans.LoginBean.hacerLogin()Inicio");
            if(usuario != null && !usuario.equals("") && password != null && !password.equals("")){
                Nominatraficolibre empleado = nominaControlador.consultarNominaPorDoc(Integer.valueOf(usuario));
                if(empleado != null){
                    if(empleado.getContrasenia().equals(password)){
                        FacesContext.getCurrentInstance().getExternalContext().redirect("pages/menu/MenuInicial.xhtml");
                        addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Bienvenido " + empleado.getNombre());
                    } else {
                        addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Usuario o Contraseña Incorrecta");
                    }

                } else {
                    addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El Usuario no Esta Registrado");
                }
            } else {
                addMessage(FacesMessage.SEVERITY_ERROR, "Advertencia", "Debe Diligencias todos los campos");
            }
            
        } catch (IOException ex) {
            System.out.println("beans.LoginBean.hacerLogin()error: " + ex);
        }
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public EmpleadosOficina getEmpleado() {
//        return empleado;
//    }
//
//    public void setEmpleado(EmpleadosOficina empleado) {
//        this.empleado = empleado;
//    }
    
}
