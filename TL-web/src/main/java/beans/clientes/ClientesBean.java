/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.clientes;

import com.tl.entidades.Cliente;
import com.tl.manager.ClienteManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "clienteBean")
public class ClientesBean {
    
    private ClienteManager clienteControlador;
    private Cliente clienteNuevo;
    
    private List<Cliente>listadoClientes;
    private String nroDocumento;
    
    private List<String> listadoTipoPersonas;
    
    @PostConstruct
    public void init() {
        System.out.println("beans.ClienteBEan.init()Inicio");
        
        clienteControlador = new ClienteManager();
        clienteNuevo = new Cliente();
        
        llenarListadoTipoPersonas();
        listarClientes();
    }
    
    public void registrarCliente(){
        System.out.println("beans.ConductorBean.registrarConductor()Inicio");
        clienteNuevo.setEstado(0);
        clienteNuevo.setIdClienteTercero(0);
        clienteNuevo.setTipocliente("");
        
        clienteNuevo.setFechalog(new Date());
        clienteNuevo.setUsuariolog("ADMIN");
        boolean respuesta = clienteControlador.crearCliente(clienteNuevo);
        if(respuesta)
            addMessage(FacesMessage.SEVERITY_INFO, "Èxito", "Se Registró el cliente satisfactoriamente");
        else
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El cliente no pudo ser registrado");
        
        limpiarCampos();
        listarClientes();
    }
    
    public void buscarConductor(){
        System.out.println("beans.ConductorBean.buscarConductor()Inicio");
//        conductor = conductorControlador.consultarConductorDocto(nroDocumento);
//        if(conductor == null)
//            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "El conductor no existe");
    }
    
    public void actualizarConductor(){
    
    }
    
    public void listarClientes(){
        System.out.println("beans.ClientesBean.listarClientes()");
        listadoClientes = clienteControlador.listarClientes();
        if(listadoClientes == null || listadoClientes.isEmpty()){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "No hay clientes registrados");
        }
    }
    
    private void llenarListadoTipoPersonas(){
        System.out.println("beans.clientes.ClientesBean.llenarListadoTipoPersonas()");
        listadoTipoPersonas = new ArrayList<>();
        listadoTipoPersonas.add("PERSONA NATURAL");
        listadoTipoPersonas.add("PERSONA JURIDICA");
    }
    
    private void limpiarCampos(){
        clienteNuevo = new Cliente();
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    public Cliente getClienteNuevo() {
        return clienteNuevo;
    }

    public void setClienteNuevo(Cliente clienteNuevo) {
        this.clienteNuevo = clienteNuevo;
    }

    public List<Cliente> getListadoClientes() {
        return listadoClientes;
    }

    public void setListadoClientes(List<Cliente> listadoClientes) {
        this.listadoClientes = listadoClientes;
    }

    public List<String> getListadoTipoPersonas() {
        return listadoTipoPersonas;
    }

    public void setListadoTipoPersonas(List<String> listadoTipoPersonas) {
        this.listadoTipoPersonas = listadoTipoPersonas;
    }
    
}
