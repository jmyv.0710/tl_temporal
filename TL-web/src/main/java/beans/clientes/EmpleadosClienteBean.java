/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.clientes;

import com.tl.entidades.Cliente;
import com.tl.entidades.Nominacliente;
import com.tl.manager.ClienteManager;
import com.tl.manager.NominaClienteManager;
import com.tl.utilidades.UtilFechas;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "empleadosClienteBean")
public class EmpleadosClienteBean {
    
    private ClienteManager clienteControlador;
    private NominaClienteManager empleadosControlador;
    
    private List<Nominacliente>listadoEmpleados;
    private Cliente clienteConsultado;
    private Nominacliente nuevoEmpleado;
    
    private String nroDocumentoCliente;
    private List<String> listadoTipoDocumentos;
    
    @PostConstruct
    public void init() {
        System.out.println("beans.clientes.EmpleadosClienteBean.init()");
        inicializarVariables();
        listarTipoDocumento();
    }
    
    private void inicializarVariables(){
        System.out.println("beans.clientes.EmpleadosClienteBean.inicializarVariables()Inicio");
        
        clienteControlador = new ClienteManager();
        empleadosControlador = new NominaClienteManager();
        nuevoEmpleado = new Nominacliente();
    }
    
    public void buscarCliente(){
        System.out.println("beans.clientes.EmpleadosClienteBean.buscarCliente()");
        clienteConsultado = clienteControlador.consultarClienteDocto(nroDocumentoCliente);
        if(clienteConsultado != null){
            addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Cliente Identificado");
            listarEmpleadosCliente();
            if(!listadoEmpleados.isEmpty() && listadoEmpleados.size()>0){
                addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Se encontraron " +
                        listadoEmpleados.size() + " empelados");
            } else {
                addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "sin empleados registrados");
            }
        } else {
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Cliente no identificado");
        }
    }
    
    public void registrarNuevoEmpelado(){
        System.out.println("beans.clientes.EmpleadosClienteBean.registrarNuevoEmpelado()");
        try {
            nuevoEmpleado.setEstado(0);
            nuevoEmpleado.setFechalog(UtilFechas.obtenerFechaSistemaDate());
            nuevoEmpleado.setIdRol(1);
            nuevoEmpleado.setIdcliente(clienteConsultado.getIdcliente());
            nuevoEmpleado.setUsuariolog("ADMIN");
            boolean respuesta = empleadosControlador.crearNominaCliente(nuevoEmpleado);
            if(respuesta){
                addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Empleado Registrado");
                listarEmpleadosCliente();
            } else {
                addMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pudo almacenar el empleado");
            }
            nuevoEmpleado = new Nominacliente();
        } catch(ParseException e){
            System.out.println("beans.clientes.EmpleadosClienteBean.registrarNuevoEmpelado()Error: " + e);
        }
    }
    
    private void listarEmpleadosCliente(){
        System.out.println("beans.clientes.DireccionesClienteBean.listarDireccionesCliente()");
        listadoEmpleados = empleadosControlador.listarNominaClientes(clienteConsultado.getIdcliente());
    }
    
    private void listarTipoDocumento(){
        System.out.println("beans.clientes.EmpleadosClienteBean.listarTipoDocumento()Inicio");
        listadoTipoDocumentos = new ArrayList<>();
        listadoTipoDocumentos.add("CEDULA CIUDADANÍA");
        listadoTipoDocumentos.add("CEDULA EXTRANJERÍA");
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    public List<Nominacliente> getListadoEmpleados() {
        return listadoEmpleados;
    }

    public void setListadoEmpleados(List<Nominacliente> listadoEmpleados) {
        this.listadoEmpleados = listadoEmpleados;
    }

    public Cliente getClienteConsultado() {
        return clienteConsultado;
    }

    public void setClienteConsultado(Cliente clienteConsultado) {
        this.clienteConsultado = clienteConsultado;
    }

    public Nominacliente getNuevoEmpleado() {
        return nuevoEmpleado;
    }

    public void setNuevoEmpleado(Nominacliente nuevoEmpleado) {
        this.nuevoEmpleado = nuevoEmpleado;
    }

    public String getNroDocumentoCliente() {
        return nroDocumentoCliente;
    }

    public void setNroDocumentoCliente(String nroDocumentoCliente) {
        this.nroDocumentoCliente = nroDocumentoCliente;
    }

    public List<String> getListadoTipoDocumentos() {
        return listadoTipoDocumentos;
    }

    public void setListadoTipoDocumentos(List<String> listadoTipoDocumentos) {
        this.listadoTipoDocumentos = listadoTipoDocumentos;
    }
    
}
