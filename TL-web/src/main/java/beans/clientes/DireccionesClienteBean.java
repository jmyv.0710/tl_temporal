/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.clientes;

import com.tl.entidades.Cliente;
import com.tl.entidades.DireccionesCliente;
import com.tl.manager.ClienteManager;
import com.tl.manager.DireccionClienteManager;
import com.tl.utilidades.UtilFechas;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "direccionesBean")
public class DireccionesClienteBean {
    
    private DireccionClienteManager direccionesControlador;
    private ClienteManager clienteControlador;
    
    private List<DireccionesCliente>listadoDirecciones;
    private Cliente clienteConsultado;
    private DireccionesCliente nuevaDireccion;

    private List<String> listadoTipoDirecciones;
    
    private String nroDocumentoCliente;
    
    @PostConstruct
    public void init() {
        System.out.println("beans.clientes.DireccionesClienteBean.init()Inicio");
        direccionesControlador = new DireccionClienteManager();
        clienteControlador = new ClienteManager();
        inicializarVariables();
    }
    
    private void inicializarVariables(){
        System.out.println("beans.clientes.DireccionesClienteBean.inicializarVariables()Inicio");
        listadoDirecciones = new ArrayList<>();
        nuevaDireccion = new DireccionesCliente();
        listarTipoDirecciones();
    }
    
    public void consultarCliente(){
        System.out.println("beans.clientes.DireccionesClienteBean.consultarCliente()Inicio");
        clienteConsultado = clienteControlador.consultarClienteDocto(nroDocumentoCliente);
        if(clienteConsultado != null){
            addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Cliente Identificado");
            listarDireccionesCliente();
            if(!listadoDirecciones.isEmpty() && listadoDirecciones.size()>0){
                addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Se encontraron " +
                        listadoDirecciones.size() + " direcciones");
            } else {
                addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "sin direcciones registradas");
            }
        } else {
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Cliente no identificado");
        }
    }
    
    public void registrarNuevaDireccion(){
        System.out.println("beans.clientes.DireccionesClienteBean.registrarNuevaDireccion()Inicio");
        try {
            nuevaDireccion.setIdCliente(clienteConsultado.getIdcliente());
            nuevaDireccion.setFechalog(UtilFechas.obtenerFechaSistemaDate());
            nuevaDireccion.setUsuariolog("ADMIN");
            boolean respuesta = direccionesControlador.crearDireccionCliente(nuevaDireccion);
            if(respuesta){
                addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Dirección Registrada");
                listarDireccionesCliente();
            } else {
                addMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pudo almacenar la dirección");
            }
            nuevaDireccion = new DireccionesCliente();
        } catch (ParseException e ) {
            System.out.println("beans.clientes.DireccionesClienteBean.registrarNuevaDireccion()error: " + e);
        }
    }
    
    private void listarDireccionesCliente(){
        System.out.println("beans.clientes.DireccionesClienteBean.listarDireccionesCliente()");
        listadoDirecciones = direccionesControlador.listarDireccionesC(clienteConsultado.getIdcliente());
    }
    
    private void listarTipoDirecciones(){
        System.out.println("beans.clientes.DireccionesClienteBean.listarTipoDirecciones()Inicio");
        listadoTipoDirecciones = new ArrayList<>();
        listadoTipoDirecciones.add("RURAL");
        listadoTipoDirecciones.add("URBANO");
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    public List<DireccionesCliente> getListadoDirecciones() {
        return listadoDirecciones;
    }

    public void setListadoDirecciones(List<DireccionesCliente> listadoDirecciones) {
        this.listadoDirecciones = listadoDirecciones;
    }

    public Cliente getClienteConsultado() {
        return clienteConsultado;
    }

    public void setClienteConsultado(Cliente clienteConsultado) {
        this.clienteConsultado = clienteConsultado;
    }

    public DireccionesCliente getNuevaDireccion() {
        return nuevaDireccion;
    }

    public void setNuevaDireccion(DireccionesCliente nuevaDireccion) {
        this.nuevaDireccion = nuevaDireccion;
    }

    public String getNroDocumentoCliente() {
        return nroDocumentoCliente;
    }

    public void setNroDocumentoCliente(String nroDocumentoCliente) {
        this.nroDocumentoCliente = nroDocumentoCliente;
    }

    public List<String> getListadoTipoDirecciones() {
        return listadoTipoDirecciones;
    }

    public void setListadoTipoDirecciones(List<String> listadoTipoDirecciones) {
        this.listadoTipoDirecciones = listadoTipoDirecciones;
    }
    
}
