/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.solicitudes;

import com.tl.entidades.Carga;
import com.tl.entidades.Cliente;
import com.tl.entidades.DireccionesCliente;
import com.tl.entidades.Esquemaseguridad;
import com.tl.entidades.Nominacliente;
import com.tl.entidades.ServiciosAdicionales;
import com.tl.entidades.Solicitud;
import com.tl.manager.ClienteManager;
import com.tl.manager.DireccionClienteManager;
import com.tl.manager.NominaClienteManager;
import com.tl.manager.SolicitudManager;
import com.tl.utilidades.UtilFechas;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Manuel
 */
@ViewScoped
@ManagedBean(name = "solicitudesBean")
public class SolicitudesBean {
    
    private SolicitudManager solicitudControlador;
    private ClienteManager clientesControlador;
    private NominaClienteManager nominaClienteControlador;
    private DireccionClienteManager direccionesClientesControlador;
    
    private Solicitud nuevaSolicitud;
    private List<Solicitud> listadoSolicitudes;
    private List<Cliente> listadoClientes;
    private List<Nominacliente> listadoEmpleadosCliente;
    private List<DireccionesCliente> listadoDirecciones;
    private List<String> listadoTipoCarga;
    private List<String> listadoResponsablePoliza;
    
    private Carga nuevaCagra;
    private ServiciosAdicionales nuevoServicioAdicional;
    private Esquemaseguridad nuevoEsquemaSeguridad;
    
    private List<Carga> listadoCargas;
    private List<ServiciosAdicionales> listadoServiciosAdicionales;
    private List<Esquemaseguridad> listadoEsquemaSeguridad;
    
    private String solicitudSeleccionada;
    private String clienteSeleccionado;
    private String empleadoSeleccionado;
    private String contactoOrigenSeleccionado;
    private String contactoDestinoSeleccionado;
    private String origenSeleccionado;
    private String destinoSeleccionado;
    private String tipoCargaSeleccionada;
    
    private Cliente clienteEntidad;
    private int idOrigenSeleccionado;
    private int idDestinoSeleccionado;
    private int idSolicitanteSeleccionado;
    private int idContactoOrigenSeleccionado;
    private int idContactoDestinoSeleccionado;
    
    private Date fechaActualSistema;
    
    @PostConstruct
    public void init(){
        System.out.println("beans.solicitudes.SolicitudesBean.init()Inicio");
        inicializarVariables();
        listarSolicitudes();
        listarEmpresas();
        llenarListaTipoCarga();
        llenarListaResponsablesPoliza();
    }
    
    private void inicializarVariables(){
        System.out.println("beans.solicitudes.SolicitudesBean.inicializarVariables()Inicio");
        solicitudControlador = new SolicitudManager();
        clientesControlador = new ClienteManager();
        nominaClienteControlador = new NominaClienteManager();
        direccionesClientesControlador = new DireccionClienteManager();
        
        inicializarClasesBase();
        
        try {
            fechaActualSistema = UtilFechas.obtenerFechaSistemaDate();
        } catch(ParseException e ){
            System.out.println("beans.solicitudes.SolicitudesBean.inicializarVariables()Error: " + e);
        }
    }
    
    private void inicializarClasesBase(){
        System.out.println("beans.solicitudes.SolicitudesBean.inicializarClasesBase()Inicio");
        nuevaCagra = new Carga();
        nuevaSolicitud = new Solicitud();
        nuevoEsquemaSeguridad = new Esquemaseguridad();
        nuevoServicioAdicional = new ServiciosAdicionales();
        
        listadoCargas = new ArrayList<>();
        listadoEsquemaSeguridad = new ArrayList<>();
        listadoServiciosAdicionales = new ArrayList<>();
    }
    
    public void generarNuevaSolicitud(){
        System.out.println("beans.solicitudes.SolicitudesBean.generarNuevaSolicitud()Inicio");
        try {
            consultarIdEntidades();
            nuevaSolicitud.setSolicitadopor(clienteEntidad.getIdcliente());
            nuevaSolicitud.setOrigenCarga(idOrigenSeleccionado);
            nuevaSolicitud.setDestinoCarga(idDestinoSeleccionado);
            nuevaSolicitud.setSolicitadopor(idSolicitanteSeleccionado);
            nuevaSolicitud.setContactoorigen(idContactoOrigenSeleccionado);
            nuevaSolicitud.setContactodestino(idContactoDestinoSeleccionado);
            nuevaSolicitud.setFechasolicitud(fechaActualSistema);
            nuevaSolicitud.setObservaciones(clienteEntidad.getObservacionesespeciales());

            nuevaSolicitud.setFpu("");
            nuevaSolicitud.setTarifa(0);
            //nuevaSolicitud.setFechacargue(fechaActualSistema);
            //nuevaSolicitud.setNroreserva(idContactoDestinoSeleccionado);

            nuevaSolicitud.setUsuariolog("ADMIN");
            nuevaSolicitud.setFechalog(UtilFechas.obtenerFechaSistemaDate());
        
            boolean respuesta = solicitudControlador.adicionarSolicitud(nuevaSolicitud);
            if(respuesta){
                addMessage(FacesMessage.SEVERITY_INFO, "Éxito", "Solicitud Registrada");
                limpiarFormulario();
            } else {
                addMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pudo almacenar la solicitud");
            }
            
        } catch (ParseException e) {
            System.out.println("beans.solicitudes.SolicitudesBean.generarNuevaSolicitud()Error: " + e);
        }
    }
    
    public void agregarCarga(){
        System.out.println("beans.solicitudes.SolicitudesBean.agregarCarga()");
        nuevaCagra.setTipocarga(tipoCargaSeleccionada);
        listadoCargas.add(nuevaCagra);
        nuevaCagra = new Carga();
    }
    
    public void agregarServicioAdicional(){
        System.out.println("beans.solicitudes.SolicitudesBean.agregarServicioAdicional()Inicio");
        listadoServiciosAdicionales.add(nuevoServicioAdicional);
        nuevoServicioAdicional = new ServiciosAdicionales();
    }
    
    public void agregarEsquemaDeSeguridad(){
        System.out.println("beans.solicitudes.SolicitudesBean.agregarEsquemaDeSeguridad()");
        listadoEsquemaSeguridad.add(nuevoEsquemaSeguridad);
        nuevoEsquemaSeguridad = new Esquemaseguridad();
    }
    
    private void listarSolicitudes(){
        System.out.println("beans.solicitudes.SolicitudesBean.listarSolicitudes()Inicio");
        listadoSolicitudes = solicitudControlador.listarSolicitudes();
        if(listadoSolicitudes == null || listadoSolicitudes.isEmpty()){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "No hay solicitudes registradas");
        }
    }
    
    private void listarEmpresas(){
        System.out.println("beans.solicitudes.SolicitudesBean.listarEmpresas()");
        listadoClientes = clientesControlador.listarClientes();
        if(listadoClientes == null || listadoClientes.isEmpty()){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "No hay empresas registradas");
        }
    }
    
    private void listarClientesPorEmpresa(int idEmpresa){
        System.out.println("beans.solicitudes.SolicitudesBean.listarClientesPorEmpresa()Inicio");
        listadoEmpleadosCliente = nominaClienteControlador.listarNominaClientes(idEmpresa);
        if(listadoEmpleadosCliente == null || listadoEmpleadosCliente.isEmpty()){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "No hay empleados registrados para esta empresa");
        }
    }
    
    private void listarOrigenCarga(int idEmpresa){
        System.out.println("beans.solicitudes.SolicitudesBean.listarOrigenCarga()Inicio");
        listadoDirecciones = direccionesClientesControlador.listarDireccionesC(idEmpresa);
        if(listadoDirecciones == null || listadoDirecciones.isEmpty()){
            addMessage(FacesMessage.SEVERITY_WARN, "Advertencia!", "No hay direcciones de origen registradas para esta empresa");
        }
    }
    
    private void llenarListaTipoCarga(){
        System.out.println("beans.solicitudes.SolicitudesBean.llenarListaTipoCarga()Inicio");
        listadoTipoCarga = new ArrayList<>();
        listadoTipoCarga.add("TIPO 1");
        listadoTipoCarga.add("TIPO 2");
        listadoTipoCarga.add("TIPO 3");
    }
    
    public void buscarIdEmpresa(){
        System.out.println("beans.solicitudes.SolicitudesBean.buscaridEmpresa()Inicio");
        for (Cliente m : listadoClientes) {
            if (m.getNombrecliente().equals(this.clienteSeleccionado)){
                listarClientesPorEmpresa(m.getIdcliente());
                listarOrigenCarga(m.getIdcliente());
                this.clienteEntidad = m;
            }
        }
    }
    
    private void consultarIdEntidades(){
        System.out.println("beans.solicitudes.SolicitudesBean.consultarIdEntidades()");
        consultarIdOrigenCarga();
        consultarIdDestinoCarga();
        consultarIdEmpleados();
    }
    
    private void consultarIdOrigenCarga(){
        System.out.println("beans.solicitudes.SolicitudesBean.consultarIdOrigenCarga()");
        for (DireccionesCliente dir : listadoDirecciones) {
            if (dir.getDireccion().equals(this.origenSeleccionado)){
                this.idOrigenSeleccionado = dir.getIddireccionescliente();
            } else if (dir.getDireccion().equals(this.destinoSeleccionado)){
                this.idDestinoSeleccionado = dir.getIddireccionescliente();
            }
        }
    }
    
    private void consultarIdDestinoCarga(){
        System.out.println("beans.solicitudes.SolicitudesBean.consultarIdDestinoCarga()");
        
    }
    
    private void consultarIdEmpleados(){
        System.out.println("beans.solicitudes.SolicitudesBean.consultarIdEmpleados()");
        for (Nominacliente emp: listadoEmpleadosCliente) {
            if (emp.getNombresolicitante().equals(this.contactoOrigenSeleccionado)){
                this.idContactoOrigenSeleccionado = emp.getIdcliente();
            } else if (emp.getNombresolicitante().equals(this.contactoDestinoSeleccionado)){
                this.idContactoDestinoSeleccionado = emp.getIdcliente();
            } else if (emp.getNombresolicitante().equals(this.empleadoSeleccionado)){
                this.idSolicitanteSeleccionado = emp.getIdcliente();
            }
        }
    }
    
    private void llenarListaResponsablesPoliza(){
        System.out.println("beans.solicitudes.SolicitudesBean.llenarListaResponsablesPoliza()Inicio");
        listadoResponsablePoliza = new ArrayList<>();
        listadoResponsablePoliza.add("Cliente");
        listadoResponsablePoliza.add("Empresa");
    }
    
    private void addMessage(FacesMessage.Severity severidad, String cabecera, String mensaje) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severidad, cabecera, mensaje));
    }

    private void limpiarFormulario(){
        System.out.println("beans.solicitudes.SolicitudesBean.limpiarFormulario()");
        System.out.println("beans.solicitudes.SolicitudesBean.limpiarFormulario()TO - DO");
    }
    
    public Solicitud getNuevaSolicitud() {
        return nuevaSolicitud;
    }

    public void setNuevaSolicitud(Solicitud nuevaSolicitud) {
        this.nuevaSolicitud = nuevaSolicitud;
    }

    public List<Solicitud> getListadoSolicitudes() {
        return listadoSolicitudes;
    }

    public void setListadoSolicitudes(List<Solicitud> listadoSolicitudes) {
        this.listadoSolicitudes = listadoSolicitudes;
    }

    public Date getFechaActualSistema() {
        return fechaActualSistema;
    }

    public void setFechaActualSistema(Date fechaActualSistema) {
        this.fechaActualSistema = fechaActualSistema;
    }

    public List<Cliente> getListadoClientes() {
        return listadoClientes;
    }

    public void setListadoClientes(List<Cliente> listadoClientes) {
        this.listadoClientes = listadoClientes;
    }

    public List<Nominacliente> getListadoEmpleadosCliente() {
        return listadoEmpleadosCliente;
    }

    public void setListadoEmpleadosCliente(List<Nominacliente> listadoEmpleadosCliente) {
        this.listadoEmpleadosCliente = listadoEmpleadosCliente;
    }

    public List<DireccionesCliente> getListadoDirecciones() {
        return listadoDirecciones;
    }

    public void setListadoDirecciones(List<DireccionesCliente> listadoDirecciones) {
        this.listadoDirecciones = listadoDirecciones;
    }

    public List<String> getListadoTipoCarga() {
        return listadoTipoCarga;
    }

    public void setListadoTipoCarga(List<String> listadoTipoCarga) {
        this.listadoTipoCarga = listadoTipoCarga;
    }

    public String getSolicitudSeleccionada() {
        return solicitudSeleccionada;
    }

    public void setSolicitudSeleccionada(String solicitudSeleccionada) {
        this.solicitudSeleccionada = solicitudSeleccionada;
    }

    public String getClienteSeleccionado() {
        return clienteSeleccionado;
    }

    public void setClienteSeleccionado(String clienteSeleccionado) {
        this.clienteSeleccionado = clienteSeleccionado;
    }

    public String getEmpleadoSeleccionado() {
        return empleadoSeleccionado;
    }

    public void setEmpleadoSeleccionado(String empleadoSeleccionado) {
        this.empleadoSeleccionado = empleadoSeleccionado;
    }

    public String getOrigenSeleccionado() {
        return origenSeleccionado;
    }

    public void setOrigenSeleccionado(String origenSeleccionado) {
        this.origenSeleccionado = origenSeleccionado;
    }

    public String getDestinoSeleccionado() {
        return destinoSeleccionado;
    }

    public void setDestinoSeleccionado(String destinoSeleccionado) {
        this.destinoSeleccionado = destinoSeleccionado;
    }

    public String getTipoCargaSeleccionada() {
        return tipoCargaSeleccionada;
    }

    public void setTipoCargaSeleccionada(String tipoCargaSeleccionada) {
        this.tipoCargaSeleccionada = tipoCargaSeleccionada;
    }

    public Carga getNuevaCagra() {
        return nuevaCagra;
    }

    public void setNuevaCagra(Carga nuevaCagra) {
        this.nuevaCagra = nuevaCagra;
    }

    public ServiciosAdicionales getNuevoServicioAdicional() {
        return nuevoServicioAdicional;
    }

    public void setNuevoServicioAdicional(ServiciosAdicionales nuevoServicioAdicional) {
        this.nuevoServicioAdicional = nuevoServicioAdicional;
    }

    public Esquemaseguridad getNuevoEsquemaSeguridad() {
        return nuevoEsquemaSeguridad;
    }

    public void setNuevoEsquemaSeguridad(Esquemaseguridad nuevoEsquemaSeguridad) {
        this.nuevoEsquemaSeguridad = nuevoEsquemaSeguridad;
    }

    public List<Carga> getListadoCargas() {
        return listadoCargas;
    }

    public void setListadoCargas(List<Carga> listadoCargas) {
        this.listadoCargas = listadoCargas;
    }

    public List<ServiciosAdicionales> getListadoServiciosAdicionales() {
        return listadoServiciosAdicionales;
    }

    public void setListadoServiciosAdicionales(List<ServiciosAdicionales> listadoServiciosAdicionales) {
        this.listadoServiciosAdicionales = listadoServiciosAdicionales;
    }

    public List<Esquemaseguridad> getListadoEsquemaSeguridad() {
        return listadoEsquemaSeguridad;
    }

    public void setListadoEsquemaSeguridad(List<Esquemaseguridad> listadoEsquemaSeguridad) {
        this.listadoEsquemaSeguridad = listadoEsquemaSeguridad;
    }

    public List<String> getListadoResponsablePoliza() {
        return listadoResponsablePoliza;
    }

    public void setListadoResponsablePoliza(List<String> listadoResponsablePoliza) {
        this.listadoResponsablePoliza = listadoResponsablePoliza;
    }

    public String getContactoOrigenSeleccionado() {
        return contactoOrigenSeleccionado;
    }

    public void setContactoOrigenSeleccionado(String contactoOrigenSeleccionado) {
        this.contactoOrigenSeleccionado = contactoOrigenSeleccionado;
    }

    public String getContactoDestinoSeleccionado() {
        return contactoDestinoSeleccionado;
    }

    public void setContactoDestinoSeleccionado(String contactoDestinoSeleccionado) {
        this.contactoDestinoSeleccionado = contactoDestinoSeleccionado;
    }
    
    
    
}
