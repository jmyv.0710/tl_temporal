PrimeFaces.locales['es'] = {
    closeText: 'Cerrar',
    prevText: 'Ayer',
    nextText: 'Mañana',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['Dm', 'Ln', 'Mr', 'Mx', 'Jv', 'Vr', 'Sb'],
    weekHeader: 'LOL',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    timeText: 'Hora Seleccionada',
    hourText: 'Hora',
    minuteText: 'Minutos',
    allDayText: 'Todo el Día'
};
